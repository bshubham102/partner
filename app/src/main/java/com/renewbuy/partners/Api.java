package com.renewbuy.partners;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.AsyncTask;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.text.InputFilter;
import android.text.InputType;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.renewbuy.mercury.Authentication;
import com.renewbuy.mercury.Finance;
import com.renewbuy.mercury.Intializer;
import com.renewbuy.mercury.Transactions;
import com.renewbuy.mercury.Utility;
import com.renewbuy.mercury.callBacks.CallBack;
import com.renewbuy.mercury.models.ErrorModel;
import com.renewbuy.mercury.models.FinanceModel;
import com.renewbuy.mercury.models.ProfileModel;
import com.renewbuy.mercury.models.StatsModel;
import com.renewbuy.mercury.models.SuccessTransaction;
import com.renewbuy.mercury.models.TransactionExecutiveModel;
import com.renewbuy.mercury.models.UserModel;
import com.renewbuy.partners.Sales.MissedTransaction;
import com.renewbuy.partners.Sales.PurchasedTransaction;
import com.renewbuy.partners.Sales.RejectedTransaction;
import com.renewbuy.partners.Sales.SubmitTransaction;
import com.renewbuy.partners.db.AppDb;
import com.renewbuy.partners.db.CustomerBean;
import com.renewbuy.partners.db.CustomerTable;
import com.renewbuy.partners.db.NotificationTable;
import com.renewbuy.partners.db.PaymentBean;
import com.renewbuy.partners.db.PaymentTable;
import com.renewbuy.partners.db.StatsBean;
import com.renewbuy.partners.db.StatsTable;
import com.renewbuy.partners.gcm.RegistrationIntentService;
import com.renewbuy.partners.modelClass.NotificationModel;
import com.renewbuy.partners.receivers.SMSReceiver;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.Vector;
import java.util.concurrent.TimeUnit;

/**
 * Created by shubham on 18/7/16.
 */
public class Api {

    public static final int TIMEOUT = 30000;

    public static Dialog otp;
    public static SMSReceiver receiver;
    public static String otp_code = null;

    public static String getUrl(Activity activity, Integer stringId) {
        return Config.DOMAIN + activity.getString(stringId);
    }

    public static String getUrl(Activity activity, Integer stringId, Object... args) {
        return Config.DOMAIN + String.format(activity.getString(stringId), args);
    }

    static void verify_mobile_number(final IntroActivity activity, final String email_id, final String pwd, final String phn_no) {
        TelephonyManager tm = (TelephonyManager) activity.getSystemService(Context.TELEPHONY_SERVICE);

        if ((tm==null) || (tm != null && tm.getSimState() == TelephonyManager.SIM_STATE_ABSENT)) {

            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            final View view =  LayoutInflater.from(activity).inflate(R.layout.fragment_otp,null);
            final EditText et_otp = (EditText) view.findViewById(R.id.otp);
            builder.setTitle("Verify Mobile Number");
            builder.setMessage("You have recevied OTP on your registerd mobile number in order to complete your registration");
            builder.setView(view);
            builder.setCancelable(false);
            builder.setPositiveButton(activity.getResources().getString(R.string.submit), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Utils.hideKeyboard(activity);
                    final String otp_code = et_otp.getText().toString();
                    verify_OTP(activity, email_id, pwd, otp_code, phn_no);
                }
            });
            builder.setNegativeButton(activity.getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            Dialog dialog = builder.create();
            dialog.show();

        } else {
            new CheckOwnMobileNumber(activity, email_id, pwd, phn_no).execute();
        }
    }

    public static void feedback(final Activity activity, int rating, final EditText message){
        final String feedBackUrl = Utils.DOMAIN + activity.getString(R.string.feedback_url);

        final ProgressDialog progressDialog = ProgressDialog.show(activity, "", "Please wait...");

        final JSONObject params = new JSONObject();
        try {
            params.put("rating", rating);
            params.put("feedback", message.getText().toString());
            params.put("source", 2);
        } catch (JSONException e) {
        }

        String token = Utils.getAuthToken(activity);

        Log.d("Request",feedBackUrl+params);


        AuthorizedJsonObjectRequest req = new AuthorizedJsonObjectRequest(Request.Method.POST, feedBackUrl, params, token, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                try {
                    Log.d("Response",response.toString());

                    String result = response.getString("detail");
                    progressDialog.dismiss();
                    message.setText(null);
                    Utils.ShowSuccessDialog(activity,result, Utils.SUCCESS_DIALOG);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(activity.getApplicationContext(),
                            "Unable to connect to server",
                            Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                handleNetworkError(activity, error.networkResponse);
            }
        });
        // add the request object to the queue to be executed.
        ApplicationController.getInstance().addToRequestQueue(req);
    }

    public static void verify_OTP(final IntroActivity activity, final String email, final String password, String user_otp, String phn_no) {
        //Toast.makeText(activity, "inside verify_otp", Toast.LENGTH_SHORT).show();
        final String otpUrl = Config.DOMAIN + activity.getString(R.string.verify_otp_url);



        final ProgressDialog progressDialog = ProgressDialog.show(activity, "", "Verifying mobile number...");

        final JSONObject params = new JSONObject();
        try {
            params.put("otp_token", user_otp);
            params.put("email", email);
            params.put("mobile", phn_no);
        } catch (JSONException e) {
            String errorMessage = e.getMessage();
            String title = "JSON Parse Error Message";
            // Display error dialog.
            Utils.displayAlert(activity,title, errorMessage);
            e.printStackTrace();
        }
        Log.d("Request",otpUrl+params);
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, otpUrl, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.d("Response",response.toString());

                //String result = response.getString("detail");

                if(otp!=null) {
                    otp.dismiss();
                    otp_code = null;
                }

                String src=Utils.getStringFromPreferences(activity,Utils.utmSourceKey);
                String medium=Utils.getStringFromPreferences(activity,Utils.utmMediumKey);

                if (!src.isEmpty() && !medium.isEmpty()) {
                    Utils.saveDataToPreferences(activity, Utils.isRegisterKey, true);
                }

                progressDialog.dismiss();

                UserModel userModel = new UserModel(email,password);
                Api.loginUser(IntroActivity.activity, userModel);

                // Api.loginUser(activity, login_param);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                otp = null;
                progressDialog.dismiss();
                NetworkResponse response = error.networkResponse;
                if (response == null) {
                    Utils.displayAlert(activity,"Error", "Unable to connect to server.\n\nPlease check your internet connection");
                    return;
                }

                JSONObject jsonResponse;
                String title = "Error!";
                try {
                    String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                    jsonResponse = new JSONObject(jsonString);

                    String errorMessage = jsonResponse.optString("detail");
                    if (!errorMessage.isEmpty()) {
                        //VolleyLog.e("Response:%n %s", errorMessage);
                        // Display error dialog.
                        Utils.displayAlert(activity,title, errorMessage);
                    } else {
                        Iterator iterator = jsonResponse.keys();
                        while (iterator.hasNext()) {
                            String key = (String) iterator.next();
                            Utils.displayAlert(activity,"Error", jsonResponse.get(key).toString());
                            break;
                        }
                    }
                } catch (UnsupportedEncodingException | JSONException e) {
                    String errorMessage = "Error from server";
                    // Display error dialog.
                    Utils.displayAlert(activity,title, errorMessage);
                }
            }
        });

        req.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.MINUTES.toMillis(1), DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // add the request object to the queue to be executed.
        ApplicationController.getInstance().addToRequestQueue(req);
    }

    public static void setPartnerForExecutive(final Context context) {
        final String executiveId = Utils.getStringFromPreferences(context, Utils.executiveIdKey);
        final String partnerCode = Utils.getStringFromPreferences(context, Utils.partnerCodeKey);
        final String executiveUrl = String.format(Config.URL_EXECUTIVE_SET_PARTNER, executiveId);

        String token = Utils.getAuthToken(context);
        if (token == null || token.isEmpty()) {
            Toast.makeText(context.getApplicationContext(),
                    "User is not logged-in", Toast.LENGTH_SHORT).show();
            return;
        }

        final JSONObject jsonParams = new JSONObject();
        try {
            jsonParams.put("code", partnerCode);
        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }
        Log.d("Request",executiveUrl+jsonParams);
        AuthorizedJsonObjectRequest req = new AuthorizedJsonObjectRequest(Request.Method.POST, executiveUrl, jsonParams, token,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("Response",response.toString());




                        // Mark that we have sent the partner code to server
                        Utils.removeDataFromPreferences(context, Utils.partnerCodeKey);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                handleNetworkError(context, error.networkResponse);
            }
        });

        // add the request object to the queue to be executed.
        ApplicationController.getInstance().addToRequestQueue(req);
    }

    public static void setReferralCode(final Context context, String src, String medium, String term, String campaign, String content)
    {
        final String user_id = Utils.getStringFromPreferences(context, Utils.userIdKey);
        //final String partnerCode = Utils.getStringFromPreferences(context, Utils.partnerCodeKey);
        final String executiveUrl = String.format(Config.URL_SET_SOURCE, user_id);

        String token = Utils.getAuthToken(context);
        if (token == null || token.isEmpty()) {
            Toast.makeText(context.getApplicationContext(),
                    "User is not logged-in", Toast.LENGTH_SHORT).show();
            return;
        }

        final JSONObject jsonParams = new JSONObject();
        try {
            jsonParams.put("utm_source", src);
            jsonParams.put("utm_term", term);
            jsonParams.put("utm_medium", medium);
            jsonParams.put("utm_campaign", campaign);
            jsonParams.put("utm_content", content);
        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }
        Log.d("Request",executiveUrl+jsonParams);
        AuthorizedJsonObjectRequest req = new AuthorizedJsonObjectRequest(Request.Method.POST, executiveUrl, jsonParams, token,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("Response",response.toString());
                        // Mark that we have sent the partner code to server
                        Utils.removeDataFromPreferences(context, Utils.utmSourceKey);
                        Utils.removeDataFromPreferences(context, Utils.utmTermKey);
                        Utils.removeDataFromPreferences(context, Utils.utmMediumKey);
                        Utils.removeDataFromPreferences(context, Utils.utmCampaignKey);
                        Utils.removeDataFromPreferences(context, Utils.utmContentKey);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //handleNetworkError(context, error.networkResponse);
            }
        });

        // add the request object to the queue to be executed.
        ApplicationController.getInstance().addToRequestQueue(req);
    }

    /* REST based server-call methods */
    @Deprecated
    public static void loginUser(final IntroActivity activity, final HashMap<?, ?> params) {

        if (!Utils.isInternetAvailable(activity)) {
            Utils.displayAlert(activity,"Error", "Internet connection is not available");
            return;
        }

        final ProgressDialog progressDialog = ProgressDialog.show(activity, "", "Logging In...");
        final String loginUrl = getUrl(activity, R.string.login_url);
        Log.d("Request",loginUrl+params);


        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, loginUrl, new JSONObject(params), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    progressDialog.dismiss();
                    Log.d("Response",response.toString());

                    String mobile = "";
                    String authToken = "";
                    String executiveId = "";
                    String executiveName = "";
                    String user_id = "";
                    String email = "";

                    if (!response.isNull("mobile"))
                        mobile = response.getString("mobile");

                    if (!response.isNull("token"))
                        authToken = response.getString("token");

                    if (!response.isNull("executive_id"))
                        executiveId = response.getString("executive_id");

                    if (!response.isNull("user_id"))
                        user_id = response.getString("user_id");

                    if (!response.isNull("first_name"))
                        if (!response.isNull("last_name")) {
                            executiveName = response.getString("first_name") + " " + response.getString("last_name");
                        }
                        else {
                            executiveName = response.getString("first_name");
                        }

                    if (!response.isNull("email"))
                        email = response.getString("email");


                    // Save executive info in shared preferences
                    Utils.saveDataToPreferences(activity, Utils.emailKey, email);
                    Utils.saveDataToPreferences(activity, Utils.executiveMobileKey, mobile);
                    Utils.saveDataToPreferences(activity, Utils.authTokenKey, authToken);
                    Utils.saveDataToPreferences(activity, Utils.executiveIdKey, executiveId);
                    Utils.saveDataToPreferences(activity, Utils.executiveNameKey, executiveName);
                    Utils.saveDataToPreferences(activity, Utils.userIdKey, user_id);

                    if (response.isNull("executive_id")) {
                        //Utils.displayAlert("Error", "You are not a registered RB Partner.\nPlease contact customer care.");
                        AlertDialog.Builder dlgAlert = new AlertDialog.Builder(activity);

                        TextView tv_not_exective = new TextView(activity);
                        tv_not_exective.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                        tv_not_exective.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                        tv_not_exective.setGravity(Gravity.CENTER_HORIZONTAL);
                        tv_not_exective.setText("You are not a Renewbuy Partner yet.\n\nPlease enter your mobile number to register as a Partner.\n");
                        tv_not_exective.setTextColor(Color.BLACK);

                        final EditText myMsg = new EditText(activity);
                        myMsg.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                        myMsg.setHint("Enter 10 digits mobile number");
                        myMsg.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                        myMsg.setGravity(Gravity.CENTER_HORIZONTAL);
                        myMsg.setText(mobile);
                        myMsg.setSelection(myMsg.getText().length());
                        myMsg.setBackgroundResource(R.drawable.border_box);
                        myMsg.setFilters(new InputFilter[]{new InputFilter.LengthFilter(10)});
                        myMsg.setInputType(InputType.TYPE_CLASS_NUMBER);

                        LinearLayout ll = new LinearLayout(activity);
                        ll.setPadding(10, 10, 10, 10);
                        ll.setGravity(Gravity.CENTER);
                        ll.setOrientation(LinearLayout.VERTICAL);
                        ll.addView(tv_not_exective);
                        ll.addView(myMsg);

                        dlgAlert.setView(ll);

                        dlgAlert.setTitle("Error");
                        dlgAlert.setPositiveButton("Submit", null);
                        dlgAlert.setNegativeButton("Cancel", null);

                        final AlertDialog dialog = dlgAlert.create();
                        dialog.show();
                        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                String phoneno = myMsg.getText().toString();
                                if (phoneno.isEmpty() || !Utils.isValidMobileNo(phoneno)) {
                                    myMsg.setError("Please enter a valid Mobile No.");
                                } else {
                                    dialog.dismiss();
                                    make_executive(activity, params, phoneno);
                                }
                            }
                        });
                        return;
                    }


                    // If partner code has not been updated, update it now
                    Utils.updatePartnerForExecutive(activity);
                    Utils.updateReferralCode(activity);

                    // Register GCM token
                    if (activity.checkPlayServices()) {
                        // Start IntentService to register this application with GCM.
                        Intent intent = new Intent(activity, RegistrationIntentService.class);
                        activity.startService(intent);
                        Log.i("LOG","GCM Send");
                    }

                    // Refresh My Customer and My Earnings
                    refreshExecutiveData(activity);

                    getPartnerDetails(activity);

                    // Load main fragment.
                    activity.goToHomePage();

                } catch (JSONException e) {
                    String errorMessage = e.getMessage();
                    String title = "JSON Parse Error Message";
                    // Display error dialog.
                    Utils.displayAlert(activity ,title, errorMessage);
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                NetworkResponse response = error.networkResponse;
                if (response == null) {
                    Utils.displayAlert(activity,"Error", "Incorrect Email or Password");
                    return;
                }
                JSONObject jsonResponse;
                String title = "Error!";
                try {
                    String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                    jsonResponse = new JSONObject(jsonString);
                    String errorMessage = jsonResponse.optString("detail");
                    if (!errorMessage.isEmpty()) {
                        // Display error dialog.
                        Utils.displayAlert(activity,title, errorMessage);
                    } else {
                        Iterator iterator = jsonResponse.keys();
                        while (iterator.hasNext()) {
                            String key = (String) iterator.next();
                            Utils.displayAlert(activity,"Error", jsonResponse.get(key).toString());
                            break;
                        }
                    }
                } catch (UnsupportedEncodingException | JSONException e) {
                    String errorMessage = "Error from server";
                    // Display error dialog.
                    Utils.displayAlert(activity,title, errorMessage);
                }


            }
        });

        req.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.MINUTES.toMillis(30), DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // add the request object to the queue to be executed.
        ApplicationController.getInstance().addToRequestQueue(req);
    }

    // this login is based on v2 endpoint from SDK
    public static void loginUser(final IntroActivity activity, UserModel model) {
        final ProgressDialog progressDialog = ProgressDialog.show(activity, "", "Logging In...");

        final HashMap<String,String> params = new HashMap<>();
        params.put("email",model.getEmail());
        params.put("password",model.getPassword());

        Authentication auth = new Authentication(model);
        auth.loginUser(new CallBack<UserModel>() {
            @Override
            public void onResponse(UserModel model) {

                final String mobile = model.getNumber();
                String authToken = model.getToken();
                int executiveId = model.getExecutive_id();
                String executiveName = model.getFirstName() ;
                if(model.getLastName() != null )
                    executiveName+=" "+model.getLastName() ;

                String user_id = String.valueOf(model.getUser_id());
                String email = model.getEmail();

                String executive_code = model.getExecutive_code();

                if(model.getProfileModel() != null) {
                    ProfileModel profileModel = model.getProfileModel() ;
                    Utils.saveDataToPreferences(activity, Utils.profileKey, profileModel.getProfileImageUrl());
                    //We can get all details in profile model if Required
                }

                // Save executive info in shared preferences
                Utils.saveDataToPreferences(activity, Utils.emailKey, email);
                Utils.saveDataToPreferences(activity, Utils.executiveMobileKey, mobile);
                Utils.saveDataToPreferences(activity, Utils.authTokenKey, authToken);
                Utils.saveDataToPreferences(activity, Utils.executiveIdKey, String.valueOf(executiveId));
                Utils.saveDataToPreferences(activity, Utils.executiveCodeKey, executive_code);
                Utils.saveDataToPreferences(activity, Utils.executiveNameKey, executiveName);
                Utils.saveDataToPreferences(activity, Utils.userIdKey, user_id);

                if(executiveId == -1) {
                    Utils.ShowSuccessDialog(activity, "You are not a Renewbuy Partner yet.\n\nPlease enter your mobile number to register as a Partner.\n",
                            Utils.INFO_DIALOG, new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    final  AlertDialog.Builder dlgAlert = new AlertDialog.Builder(activity);

                                    final EditText myMsg = new EditText(activity);
                                    myMsg.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                                    myMsg.setHint("Enter 10 digits mobile number");
                                    myMsg.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                                    myMsg.setGravity(Gravity.CENTER_HORIZONTAL);
                                    myMsg.setSelection(myMsg.getText().length());
                                    myMsg.setBackgroundResource(R.drawable.border_box);
                                    myMsg.setFilters(new InputFilter[]{new InputFilter.LengthFilter(10)});
                                    myMsg.setInputType(InputType.TYPE_CLASS_NUMBER);

                                    LinearLayout ll = new LinearLayout(activity);
                                    ll.setPadding(10, 10, 10, 10);
                                    ll.setGravity(Gravity.CENTER);
                                    ll.setOrientation(LinearLayout.VERTICAL);
                                    ll.addView(myMsg);

                                    dlgAlert.setView(ll);

                                    dlgAlert.setTitle("Verify Mobile Number");
                                    dlgAlert.setPositiveButton("Submit", null);
                                    dlgAlert.setNegativeButton("Cancel", null);

                                    final AlertDialog dialog = dlgAlert.create();
                                    dialog.show();
                                    dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            String phoneno = myMsg.getText().toString();
                                            if (phoneno.isEmpty() || !Utils.isValidMobileNo(phoneno)) {
                                                myMsg.setError("Please enter a valid Mobile No.");
                                            } else {

                                                Utils.dialog.dismiss();
                                                make_executive(activity, params, phoneno);
                                                dialog.dismiss();
                                            }
                                        }
                                    });
                                }
                            });
                    return ;
                }

                // If partner code has not been updated, update it now
                Utils.updatePartnerForExecutive(activity);
                Utils.updateReferralCode(activity);

                // Register GCM token
                if (activity.checkPlayServices()) {
                    // Start IntentService to register this application with GCM.
                    Intent intent = new Intent(activity, RegistrationIntentService.class);
                    activity.startService(intent);
                    Log.i("LOG","GCM Send");
                }

                // Refresh My Customer and My Earnings
                refreshExecutiveData(activity);

                getPartnerDetails(activity);

                // Load main activity.
                progressDialog.dismiss();
                activity.goToHomePage();
            }

            @Override
            public void onError(ErrorModel errorModel) {
                progressDialog.dismiss();
                if(errorModel.getError().toLowerCase().contains("user is disabled")) {
                    Utils.ShowSuccessDialog(activity, errorModel.getError(), Utils.ERROR_DIALOG, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            IntroActivity.activity.goToSignUp();
                        }
                    });
                } else{
                    Utils.ShowSuccessDialog(activity, errorModel.getError(), Utils.ERROR_DIALOG);
                }
            }
        });
    }

    /**
     * Forgot Password Method
     * @param activity : for display Dialog and Toast
     * @param email : which email you want to reset password
     */
    public static void forgotPassword(final Activity activity, String email) {

        final String changePasswordUrl = getUrl(activity, R.string.forgot_password_url);
        final ProgressDialog progressDialog = ProgressDialog.show(activity, "", "Please wait...", true);

        JSONObject params = new JSONObject();
        try {
            params.put("email", email);
        } catch (JSONException ex) {
            ex.printStackTrace();
            return;
        }
        Log.d("Request",email);

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, changePasswordUrl, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                progressDialog.dismiss();
                Log.d("Response",response.toString());

                Utils.ShowSuccessDialog(activity ,"You will receive a link from support@renewbuy.com with a link to change your password. In case you do not find the mail in your inbox, please check your Spam folder",Utils.SUCCESS_DIALOG);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                handleNetworkError(activity, error.networkResponse);
            }
        });
        // add the request object to the queue to be executed.
        ApplicationController.getInstance().addToRequestQueue(req);
    }

    public static void make_executive(final Activity activity, final HashMap<?, ?> p, final String phoneno){
        final String makeExecutiveUrl = getUrl(activity, R.string.make_executive_url);

        String token = Utils.getAuthToken(activity);
        if (token == null || token.isEmpty()) {
            Toast.makeText(activity.getApplicationContext(),"User is not logged-in", Toast.LENGTH_SHORT).show();
        }

        JSONObject params = new JSONObject();
        try {
            params.put("mobile", phoneno);
        } catch (JSONException ex) {
            ex.printStackTrace();
            return;
        }

        final ProgressDialog progressDialog = ProgressDialog.show(activity, "", "Please wait...", true);

        Api.receiver = new SMSReceiver();
        IntentFilter intentFilter = new IntentFilter("android.provider.Telephony.SMS_RECEIVED");
        activity.registerReceiver(Api.receiver, intentFilter);
        Api.otp_code = null;
        Log.d("Request",makeExecutiveUrl+params);

        AuthorizedJsonObjectRequest req = new AuthorizedJsonObjectRequest(Request.Method.POST, makeExecutiveUrl, params, token,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progressDialog.dismiss();
                        Log.d("Response",response.toString());

                        verify_mobile_number((IntroActivity) activity, p.get("email").toString(), p.get("password").toString(), phoneno);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                handleNetworkError(activity, error.networkResponse);
            }
        });

        // add the request object to the queue to be executed.
        ApplicationController.getInstance().addToRequestQueue(req);
    }

    public static void refreshExecutiveData (Activity activity) {
        if( Utils.isInternetAvailable(activity) ) {
            //SDK Operations v2 endpoint
            String ex_code = Utils.getStringFromPreferences(activity,Utils.executiveCodeKey);
            String URL = Config.DOMAIN+"/api/v2/leads/?executive="+ex_code+"&embed=vehicle";//"&embed=user";

            fetchCustomers(activity, URL);
            getPaymentDetails(activity);
            // index 0 is for get stats for year
            fetchStatsV2(0,activity);
            getExecutive(activity);
        } else {
            Toast.makeText(activity, "Internet not found", Toast.LENGTH_SHORT).show();
        }
    }
    @Deprecated
    public static JSONArray fetchDataForQuotes(final Activity activity,String URL) {
        final JSONArray[] jsonArray = {null};
        final String token = Utils.getAuthToken(activity);

        AuthorizedJsonArrayRequest req = new AuthorizedJsonArrayRequest(URL, token,
                new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {
                        jsonArray[0] = response ;
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("fetchDataForQuotes Error: %s", error);
                jsonArray[0] = null ;
            }
        });
        RetryPolicy policy = new DefaultRetryPolicy(TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        req.setRetryPolicy(policy);

        // add the request object to the queue to be executed.
        ApplicationController.getInstance().addToRequestQueue(req);

        return jsonArray[0];
    }

    @Deprecated
    private static void getPaymentDetails(final Activity activity, String token) {
        //Remmove after use
        getPaymentDetails(activity);

        final String executiveId = Utils.getStringFromPreferences(activity, Utils.executiveIdKey);
        final String URL = getUrl(activity, R.string.payment_url, executiveId);

        AuthorizedJsonArrayRequest req = new AuthorizedJsonArrayRequest(URL, token,
                new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            AppDb db = AppDb.getInstance(activity);
                            PaymentTable paymentTable = (PaymentTable) db.getPaymentTableObject(PaymentTable.TABLE_NAME);
                            Vector<PaymentBean> paymentBeans = new Vector<PaymentBean>();

                            // Parsing json array response
                            // loop through each json object
                            for (int i = 0; i < response.length(); i++)
                            {
                                JSONObject vehicle = (JSONObject) response.get(i);
                                JSONObject premium = vehicle.getJSONObject("premium");
                                JSONObject customer = premium.getJSONObject("customer");
                                String vehicle_model = premium.getJSONObject("vehicle").optString("name");

                                String email = customer.getString("email");
                                String name = "";
                                if (customer.isNull("first_name")) {
                                    name = email;
                                } else {
                                    if (customer.isNull("last_name"))
                                        name = customer.optString("first_name", null);
                                    else
                                        name = customer.optString("first_name", null) + " " + customer.optString("last_name", null);
                                }

                                String phone = null;
                                if (!customer.isNull("mobile"))
                                    phone = customer.optString("mobile");

                                Integer documentStatus = vehicle.optInt("payment_status");
                                Integer vehicleId = vehicle.optInt("id");

                                double totalPremium = 0;
                                if(!vehicle.isNull("payment_amount")){
                                    totalPremium = vehicle.optDouble("payment_amount", 0.0);
                                }

                                double odpremium = 0;
                                if (!premium.isNull("odpremium")) {
                                    odpremium = premium.getDouble("odpremium");
                                }

                                double earned = 0;
                                if (!premium.isNull("actual_commission")) {
                                    earned = premium.getDouble("actual_commission");
                                }

                                String prevPolicyExpiry = null;
                                if (!vehicle.isNull("payment_date")) {
                                    prevPolicyExpiry = vehicle.optString("payment_date", null);
                                }

                                String policyNo = null;
                                if (!vehicle.isNull("policy_number")) {
                                    policyNo = vehicle.optString("policy_number", null);
                                }

                                String insurer = null;
                                if (!vehicle.isNull("insurer")) {
                                    insurer = vehicle.optString("insurer", null);
                                }

                                String policy_document = null;
                                if (!vehicle.isNull("policy_document")) {
                                    policy_document = vehicle.optString("policy_document", null);
                                }
                                //  Log.d("police",policy_document);



                                PaymentBean bean = new PaymentBean();
                                bean.setCustomer_name(name);
                                bean.setCustomer_email(email);
                                bean.setCustomer_mobile(phone);
                                bean.setCustomer_vehical_name(vehicle_model);
                                bean.setCustomer_document_status(documentStatus);
                                bean.setVehicle_id(vehicleId);
                                bean.setPolicyNo(policyNo);
                                bean.setInsurer(insurer);
                                bean.setCustomer_total_premium(totalPremium);
                                bean.setOdpremium(odpremium);
                                bean.setCustomer_earned(earned);
                                bean.setDocUrl(policy_document);

                                if (prevPolicyExpiry != null) {
                                    try {
                                        SimpleDateFormat DATE_TIME_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
                                        DATE_TIME_FORMAT.setTimeZone(TimeZone.getTimeZone("UTC"));
                                        Date date_in_date = DATE_TIME_FORMAT.parse(prevPolicyExpiry);
                                        bean.setCustomer_prev_policy_expiry(date_in_date);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    bean.setCustomer_prev_policy_expiry(null);
                                }

                                paymentBeans.add(bean);
                            }

                            paymentTable.deleteAllData(activity);
                            paymentTable.insertData(paymentBeans, activity);

                            if (PurchasedTransaction.instance != null)
                                PurchasedTransaction.instance.refreshList();

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(activity.getApplicationContext(),
                                    "Unable to connect to server",
                                    Toast.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(activity.getApplicationContext(),
                        "Unable to connect to server", Toast.LENGTH_SHORT).show();
            }
        });
        RetryPolicy policy = new DefaultRetryPolicy(TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        req.setRetryPolicy(policy);

        // add the request object to the queue to be executed.
        ApplicationController.getInstance().addToRequestQueue(req);
    }

    /**
     * Get Payment from SDK Model
     * @param activity  Main Activity Reference
     */
    private static void getPaymentDetails(final Activity activity){

        Date endDate = new Date() ;
        int date  = endDate.getDate();
        int month = endDate.getMonth() ;
        int year  = endDate.getYear() ;

        Date startDate = new Date(year,0,1);

        AppDb db = AppDb.getInstance(activity);
        final PaymentTable paymentTable = (PaymentTable) db.getPaymentTableObject(PaymentTable.TABLE_NAME);
        final Vector<PaymentBean> paymentBeans = new Vector<PaymentBean>();

        final Transactions transactions = new Transactions();
        transactions.getTransactions(startDate, endDate, new CallBack<SuccessTransaction>() {
            @Override
            public void onResponse(SuccessTransaction model) {

                for (int i = 0; i <= model.getList().size()-1 ; i++) {
                    TransactionExecutiveModel transaction = model.getList().get(i) ;

                    PaymentBean bean = new PaymentBean();
                    bean.setPayment_id(transaction.getPayment_id());

                    UserModel userModel = transaction.getUserModel() ;

                    bean.setCustomer_name(userModel.getFirstName());
                    bean.setCustomer_email(userModel.getEmail());
                    bean.setCustomer_mobile(userModel.getNumber());

                    bean.setCustomer_vehical_name(transaction.getVehiclename());
                    bean.setCustomer_document_status(transaction.getPaymentStatus());
                    bean.setVehicle_id((int) transaction.getVehicleIDV());
                    bean.setPolicyNo(transaction.getPolicyNumber());
                    bean.setInsurer(transaction.getInsurer());
                    bean.setCustomer_total_premium(transaction.getPaymentAmount());
                    bean.setOdpremium(transaction.getOdpremium());
                    bean.setCustomer_earned(transaction.getActualCommission());
                    bean.setDocUrl(transaction.getPolicyDocument());

                    String prevPolicyExpiry = transaction.getPaymentDate() ;
                    if (prevPolicyExpiry != null) {
                        try {
                            SimpleDateFormat DATE_TIME_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
                            DATE_TIME_FORMAT.setTimeZone(TimeZone.getTimeZone("UTC"));
                            Date date_in_date = DATE_TIME_FORMAT.parse(prevPolicyExpiry);
                            bean.setCustomer_prev_policy_expiry(date_in_date);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        bean.setCustomer_prev_policy_expiry(null);
                    }

                    paymentBeans.add(bean);
                }

                paymentTable.deleteAllData(activity);
                paymentTable.insertData(paymentBeans, activity);

                if (PurchasedTransaction.instance != null)
                    PurchasedTransaction.instance.refreshList();

                if(transactions.hasNextPage()) {
                    transactions.requestNextPage();
                }
            }


            @Override
            public void onError(ErrorModel errorModel) {
                Toast.makeText(activity.getApplicationContext(), errorModel.getError(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private static void fetchCustomers(final Activity activity, String URL) {
        JSONObject params = new JSONObject();

        AppDb db = AppDb.getInstance(activity);
        final CustomerTable customerTable = (CustomerTable) db.getTableObject(CustomerTable.TABLE_NAME);
        final Vector<CustomerBean> categoryBeans = new Vector<CustomerBean>();
        Log.d("Request",URL+params);

        AuthorizedJsonObjectRequest req = new AuthorizedJsonObjectRequest(Request.Method.GET, URL, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("Response",response.toString());

                try {
                    String nextPageURL = !response.isNull("next") ? response.getString("next") : null;
                    JSONArray result = response.getJSONArray("results");

                    if(result.length() > 0){
                        for (int i = 0; i <= result.length()-1 ; i++) {
                            JSONObject resObject = result.getJSONObject(i);

                            int id, user_id, product_type, lead_status,
                                    rejection_reason_id, vehicleId=0, vehicleClass = -1;

                            String name, email, mobile, executive_name, vehicalName = null, prevPolicyExpiry, quote_url=null,
                                    rejection_reason = null;
                            double expectedCommission = 0, actualCommision = 0 ;

                            id = resObject.getInt("id") ;
                            name = email = resObject.getString("email") ;
                            mobile = resObject.getString("mobile") ;

                            user_id = resObject.has("user") ? resObject.getInt("user") : 0;
                            executive_name = resObject.getString("executive") ;

                            prevPolicyExpiry = resObject.has("prev_policy_expiry") ? !resObject.isNull("prev_policy_expiry") ? resObject.getString("prev_policy_expiry") : null : null ;

                            product_type = resObject.getInt("product_type") ;
                            lead_status = resObject.getInt("lead_status") ;

                            if(!resObject.isNull("rejection_reason"))
                                rejection_reason_id = Integer.parseInt(resObject.getString("rejection_reason"));
                            else
                                rejection_reason_id = 0 ;

                            //use this object to get documents
                            JSONArray document = resObject.optJSONArray("documents") ;

                            //use this object to get user details
                            JSONObject user = resObject.optJSONObject("user") ;

                            //use this object to get vehicles details
                            JSONObject vehicle   = resObject.optJSONObject("vehicle") ;

                            if(user != null){
                                if( user.getString("first_name") != null ) {
                                    name = user.getString("first_name");
                                    name += !user.isNull("last_name") ? user.getString("last_name") : "";
                                }
                            }

                            if(vehicle != null){
                                vehicleId = vehicle.getInt("id") ;
                                vehicalName = vehicle.getString("vehicle_name");
                                vehicleClass = vehicle.getInt("vehicle_class") ;
                                quote_url = vehicle.optString("quotes_url");
                                prevPolicyExpiry = vehicle.getString("prev_policy_expiry");
                            }

                            switch (rejection_reason_id){
                                case 1:
                                    rejection_reason = "Invalid Policy";
                                    break;

                                case 2:
                                    rejection_reason = "Incomplete policy details";
                                    break;

                                case 3:
                                    rejection_reason = "Policy picture not clear";
                                    break;

                                case 4:
                                    rejection_reason = "Vehicle inspection required";
                                    break;

                                case 5:
                                    rejection_reason = "Policy uploaded twice";
                                    break;

                                case 6:
                                    rejection_reason = "Policy uploaded by somebody";
                                    break;

                                case 7:
                                    rejection_reason = "Vehicle is declined";
                                    break;
                            }

                            CustomerBean bean = new CustomerBean();
                            bean.setLead_id(id);
                            bean.setCustomer_name(name);
                            bean.setCustomer_email(email);
                            bean.setCustomer_mobile(mobile);
                            bean.setCustomer_vehical_name(vehicalName);
                            bean.setCustomer_vehical_class(vehicleClass);
                            bean.setCustomer_document_status(lead_status);
                            bean.setVehicle_id(vehicleId);
                            bean.setRejection_reason(rejection_reason);
                            bean.setQuotesUrl(quote_url);

                            if (prevPolicyExpiry != null && !prevPolicyExpiry.equals("null")) {
                                try {
                                    bean.setCustomer_prev_policy_expiry(new SimpleDateFormat("yyyy-MM-dd", Locale.US).parse(prevPolicyExpiry));
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                bean.setCustomer_prev_policy_expiry(null);
                            }
                            bean.setCustomer_expected_commission(expectedCommission);
                            bean.setCustomer_actual_commission(actualCommision);
                            categoryBeans.add(bean);
                        }
                    }
                    customerTable.deleteAllData(activity);
                    customerTable.insertData(categoryBeans, activity);

                    if(SubmitTransaction.instance != null){
                        SubmitTransaction.instance.refreshList();
                    }

                    if(MissedTransaction.instance != null){
                        MissedTransaction.instance.refreshList();
                    }

                    if(RejectedTransaction.instance != null){
                        RejectedTransaction.instance.refreshList();
                    }

                    //get next records
                    if(nextPageURL != null)
                        fetchCustomers(activity,nextPageURL);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", "Token "+Utils.getStringFromPreferences(activity,Utils.authTokenKey));
                headers.put("Content-Type", "application/json");
                headers.put("App-ID", SplashScreen.APP_ID);
                headers.put("API-SECRET-KEY", SplashScreen.API_KEY);

                return headers;
            }
        };

        RetryPolicy policy = new DefaultRetryPolicy(TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        req.setRetryPolicy(policy);

        // add the request object to the queue to be executed.
        ApplicationController.getInstance().addToRequestQueue(req);
    }

    @Deprecated
    private static void fetchStats(final Activity activity, String token) {
        // Fetch the list of uploaded documents
        final String executiveId = Utils.getStringFromPreferences(activity, Utils.executiveIdKey);
        final String statsUrl = getUrl(activity, R.string.executive_stats_url, executiveId);

        //JsonArrayRequest req = new JsonArrayRequest(URL, new Response.Listener<JSONArray>() {
        AuthorizedJsonObjectRequest req = new AuthorizedJsonObjectRequest(Request.Method.GET, statsUrl, null, token, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                // Parsing json object

                int total_accepted_policy  =  response.optInt("num_policies_accepted_fw",0) + response.optInt("num_policies_accepted_tw",0);
                int total_purchased_policy =  response.optInt("num_policies_purchased_fw",0) + response.optInt("num_policies_purchased_tw",0);

                String total_expected_commission = response.optString("total_expected_commission", "0");
                String total_actual_commission = response.optString("total_actual_commission", "0");

                String total_uploads = Long.toString(response.optLong("num_policies_accepted") +
                        response.optLong("num_policies_missed") +
                        response.optLong("num_policies_purchased"));

                String  income_missed = response.optString("income_missed", "0");
                String  balance_potential = response.optString("total_balance_potential", "0");

                Utils.saveDataToPreferences(activity, Utils.totalUploadsKey, total_uploads);

                Utils.saveDataToPreferences(activity, Utils.totalAcceptedPolicyByMonth, total_accepted_policy);
                Utils.saveDataToPreferences(activity, Utils.totalPurchasedPolicyByMonth, total_purchased_policy);

                Utils.saveDataToPreferences(activity, Utils.totalYearExpectedCommissionKey, total_expected_commission);

                Utils.saveDataToPreferences(activity, Utils.totalCurrentYearActualCommissionKey, total_actual_commission);

                Utils.saveDataToPreferences(activity, Utils.totalYearIncomeMissedKey, income_missed);

                Utils.saveDataToPreferences(activity, Utils.totalYearBalancePotentialKey, balance_potential);

                if(Stats.instance!=null)
                    Stats.instance.refreshDataSet();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(activity.getApplicationContext(),
                        "Unable to connect to server", Toast.LENGTH_SHORT).show();
            }
        });

        RetryPolicy policy = new DefaultRetryPolicy(TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        req.setRetryPolicy(policy);
        // add the request object to the queue to be executed.
        ApplicationController.getInstance().addToRequestQueue(req);
    }

    /**
     * Method to get stats for a partcular executive
     * @param activity used to store the data in sqlite
     */
    public static void fetchStatsV2(int pos, final Activity activity) {

        final boolean[] isFirst = {true};

        //Current Date Obj
        final Date currentDate = new Date();

        int date = currentDate.getDate();
        int month = currentDate.getMonth();
        int year = currentDate.getYear();


        //one year before date object
        final Date startDateForYear = new Date(year, 0, 1);

        //one month before date object
        final Date startDateForMonth = new Date(year, month, 1);


        //sqlite database obj
        AppDb db = AppDb.getInstance(activity);
        final StatsTable statsTable = (StatsTable) db.getTableObject(StatsTable.TABLE_NAME);

        final StatsBean bean = new StatsBean();
        com.renewbuy.mercury.Stats statsObj = new com.renewbuy.mercury.Stats();

        //getting stats for today
        statsObj.getStats(currentDate, currentDate, new CallBack<StatsModel>() {
            @Override
            public void onResponse(StatsModel model) {
                if(isFirst[0]){
                    isFirst[0] = false ;
                    if(statsTable.getAllData(activity).size() > 0) {
                        statsTable.deleteAllData(activity);
                    }
                }
                // Log.i("onResponse: Stats year",model.toString());
                bean.setActual_commission(model.getTotalActualCommission());
                bean.setExpected_commission(model.getTotalExpectedCommission());
                bean.setBalance_commission(model.getTotalBalancePotential());
                bean.setMissed_commission(model.getTotalMissedCommission());
                bean.setCar_sold(model.getNumPoliciesPurchasedFw());
                bean.setBike_sold(model.getNumPoliciesPurchasedTw());
                bean.setCar_earning(model.getTotalActualCommissionFw());
                bean.setBike_earning(model.getTotalActualCommissionTw());
                bean.setBike_earning(model.getTotalActualCommissionTw());
                bean.setTotal_uploads(model.getTotalUploads());


                bean.setAccpet_policy(model.getNumPoliciesAccepted() != -1 ? model.getNumPoliciesAccepted() : 0) ;
                bean.setRejected_policy(model.getNumPoliciesRejected() != -1 ? model.getNumPoliciesRejected() : 0) ;
                bean.setMissed_policy(model.getNumPoliciesMissed() != -1 ? model.getNumPoliciesMissed() : 0) ;
                bean.setSubmit_policy(model.getNumPoliciesSubmitted() != -1 ? model.getNumPoliciesSubmitted() : 0) ;

                bean.setStats_flag(StatsTable.TODAY);


                bean.setStartDate(startDateForYear);
                bean.setEndDate(currentDate);

                statsTable.insertData(bean, activity);

                if(Stats.instance!=null)
                    Stats.instance.refreshDataSet();
            }

            @Override
            public void onError(ErrorModel errorModel) {
                Toast.makeText(activity.getApplicationContext(), errorModel.getError(), Toast.LENGTH_SHORT).show();
            }
        });

        //getting stats for one month
        statsObj.getStats(startDateForMonth, currentDate, new CallBack<StatsModel>() {
            @Override
            public void onResponse(StatsModel model) {
                if(isFirst[0]){
                    isFirst[0] = false ;
                    if(statsTable.getAllData(activity).size() > 0) {
                        statsTable.deleteAllData(activity);
                    }
                }
                // Log.i("onResponse: Stats year",model.toString());
                bean.setActual_commission(model.getTotalActualCommission());
                bean.setExpected_commission(model.getTotalExpectedCommission());
                bean.setBalance_commission(model.getTotalBalancePotential());
                bean.setMissed_commission(model.getTotalMissedCommission());
                bean.setCar_sold(model.getNumPoliciesPurchasedFw());
                bean.setBike_sold(model.getNumPoliciesPurchasedTw());
                bean.setCar_earning(model.getTotalActualCommissionFw());
                bean.setBike_earning(model.getTotalActualCommissionTw());
                bean.setBike_earning(model.getTotalActualCommissionTw());
                bean.setTotal_uploads(model.getTotalUploads());


                bean.setAccpet_policy(model.getNumPoliciesAccepted() != -1 ? model.getNumPoliciesAccepted() : 0) ;
                bean.setRejected_policy(model.getNumPoliciesRejected() != -1 ? model.getNumPoliciesRejected() : 0) ;
                bean.setMissed_policy(model.getNumPoliciesMissed() != -1 ? model.getNumPoliciesMissed() : 0) ;
                bean.setSubmit_policy(model.getNumPoliciesSubmitted() != -1 ? model.getNumPoliciesSubmitted() : 0) ;

                bean.setStats_flag(StatsTable.MONTH);


                bean.setStartDate(startDateForYear);
                bean.setEndDate(currentDate);

                statsTable.insertData(bean, activity);

                if(Stats.instance!=null)
                    Stats.instance.refreshDataSet();
            }

            @Override
            public void onError(ErrorModel errorModel) {
                Toast.makeText(activity.getApplicationContext(), errorModel.getError(), Toast.LENGTH_SHORT).show();
            }
        });

        //getting stats for one year
        statsObj.getStats(startDateForYear, currentDate, new CallBack<StatsModel>() {
            @Override
            public void onResponse(StatsModel model) {
                if(isFirst[0]){
                    isFirst[0] = false ;
                    if(statsTable.getAllData(activity).size() > 0) {
                        statsTable.deleteAllData(activity);
                    }
                }
                // Log.i("onResponse: Stats year",model.toString());
                bean.setActual_commission(model.getTotalActualCommission());
                bean.setExpected_commission(model.getTotalExpectedCommission());
                bean.setBalance_commission(model.getTotalBalancePotential());
                bean.setMissed_commission(model.getTotalMissedCommission());
                bean.setCar_sold(model.getNumPoliciesPurchasedFw());
                bean.setBike_sold(model.getNumPoliciesPurchasedTw());
                bean.setCar_earning(model.getTotalActualCommissionFw());
                bean.setBike_earning(model.getTotalActualCommissionTw());
                bean.setBike_earning(model.getTotalActualCommissionTw());
                bean.setTotal_uploads(model.getTotalUploads());


                bean.setAccpet_policy(model.getNumPoliciesAccepted() != -1 ? model.getNumPoliciesAccepted() : 0) ;
                bean.setRejected_policy(model.getNumPoliciesRejected() != -1 ? model.getNumPoliciesRejected() : 0) ;
                bean.setMissed_policy(model.getNumPoliciesMissed() != -1 ? model.getNumPoliciesMissed() : 0) ;
                bean.setSubmit_policy(model.getNumPoliciesSubmitted() != -1 ? model.getNumPoliciesSubmitted() : 0) ;

                bean.setStats_flag(StatsTable.YEAR);


                bean.setStartDate(startDateForYear);
                bean.setEndDate(currentDate);

                statsTable.insertData(bean, activity);

                if(Stats.instance!=null)
                    Stats.instance.refreshDataSet();
            }

            @Override
            public void onError(ErrorModel errorModel) {
                Toast.makeText(activity.getApplicationContext(), errorModel.getError(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    //Gt the Vahan Code at login
    public static void getPartnerDetails(final Activity activity){
        final String executiveId = Utils.getStringFromPreferences(activity, Utils.executiveIdKey);
        final String executiveUrl = getUrl(activity, R.string.executive_vahan_url, executiveId);
        String token = Utils.getAuthToken(activity);
        final JSONObject jsonParams = new JSONObject();

        if (token == null || token.isEmpty()) {
            return ;
        }

        /*final ProgressDialog progressDialog = ProgressDialog.show(activity, "", "Please wait...", true);*/
        Log.d("Request",executiveUrl+jsonParams);

        AuthorizedJsonObjectRequest req = new AuthorizedJsonObjectRequest(Request.Method.GET, executiveUrl, jsonParams, token,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                       /* progressDialog.dismiss();*/
                        Log.d("Response",response.toString());

                        try
                        {
                            if( !response.isNull("partner") ){
                                JSONObject jsonObject =  response.getJSONObject("partner");
                                if ( !jsonObject.isNull("code")) {
                                    String code = jsonObject.getString("code").toLowerCase();
                                    Utils.saveDataToPreferences(activity.getApplicationContext(), Utils.executivePartnerCode, code);
                                }

                                if(CapturePolicy.instance != null) {
                                    CapturePolicy.instance.checkVAHAN();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                handleNetworkError(activity, error.networkResponse);
            }
        });

        // add the request object to the queue to be executed.
        ApplicationController.getInstance().addToRequestQueue(req);

    }

    public static void getExecutive(final Activity activity) {
        // Fetch the list of uploaded documents
        final String executiveId = Utils.getStringFromPreferences(activity, Utils.executiveIdKey);
        final String executiveUrl = getUrl(activity, R.string.executive_detail_url, executiveId);
        String token = Utils.getAuthToken(activity);
        final JSONObject jsonParams = new JSONObject();

        if (token == null || token.isEmpty()) {
            Toast.makeText(activity.getApplicationContext(),
                    "User is not logged-in", Toast.LENGTH_SHORT).show();
        }

        //final ProgressDialog progressDialog = ProgressDialog.show(activity, "", "Please wait...", true);
        Log.d("Request",executiveUrl+jsonParams);

        AuthorizedJsonObjectRequest req = new AuthorizedJsonObjectRequest(Request.Method.GET, executiveUrl, jsonParams, token,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("Response",response.toString());
                        // progressDialog.dismiss();
                        if (!response.isNull("ac_name"))
                            Utils.saveDataToPreferences(activity,Utils.accountNameKey,response.optString("ac_name", null));

                        if (!response.isNull("ac_number"))
                            Utils.saveDataToPreferences(activity,Utils.accountNumberKey,response.optString("ac_number", null));

                        if (!response.isNull("ac_ifsc_code"))
                            Utils.saveDataToPreferences(activity,Utils.ifscCodeKey,response.optString("ac_ifsc_code", null));

                        if (!response.isNull("pan_number"))
                            Utils.saveDataToPreferences(activity,Utils.panNoKey,response.optString("pan_number", null));

                        String acc_name = Utils.getStringFromPreferences(activity,Utils.accountNameKey);

                        if(!acc_name.equals("") && acc_name.length() > 0)
                            Utils.saveDataToPreferences(activity,Utils.hasAllAccountDetailsFlag,true);

                        if(PurchasedTransaction.instance != null) {
                            PurchasedTransaction.instance.setButtonText();
                        }

                        if(BankDetail.instance != null)
                            BankDetail.instance.setData();

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Update Executive Error: %s", error.getMessage());
                //  progressDialog.dismiss();
                handleNetworkError(activity, error.networkResponse);
            }
        });

        // add the request object to the queue to be executed.
        ApplicationController.getInstance().addToRequestQueue(req);
    }

    @Deprecated
    public static void updateExecutive(final Activity activity, HashMap<?, ?> params) {
        // Fetch the list of uploaded documents
        final String executiveId = Utils.getStringFromPreferences(activity, Utils.executiveIdKey);
        final String executiveUrl = getUrl(activity, R.string.executive_detail_url, executiveId);
        String token = Utils.getAuthToken(activity);
        final JSONObject jsonParams = new JSONObject(params);

        if (token == null || token.isEmpty()) {
            Toast.makeText(activity.getApplicationContext(),
                    "User is not logged-in", Toast.LENGTH_SHORT).show();
            return;
        }

        final ProgressDialog progressDialog = ProgressDialog.show(activity, "", "Please wait, Updating...", true);

        AuthorizedJsonObjectRequest req = new AuthorizedJsonObjectRequest(Request.Method.PATCH, executiveUrl, jsonParams, token,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progressDialog.dismiss();
                        if( BankDetail.instance != null) {
                            Utils.ShowSuccessDialog(activity, "Successfully updated bank details", Utils.SUCCESS_DIALOG);
                            BankDetail.instance.setData();
                        }

                        if(Profile.instance != null) {
                            Profile.instance.setData();
                        }

                        Utils.saveDataToPreferences(activity,Utils.hasAllAccountDetailsFlag,true);
                        if(PurchasedTransaction.instance != null) {
                            PurchasedTransaction.instance.setButtonText();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                progressDialog.dismiss();
                handleNetworkError(activity, error.networkResponse);
            }
        });

        // add the request object to the queue to be executed.
        ApplicationController.getInstance().addToRequestQueue(req);
    }

    //Update Bank details from SDK
    public static void updateExecutive(final Activity activity, FinanceModel model) {

        final ProgressDialog progressDialog = ProgressDialog.show(activity, "", "Please wait, Updating...", true);

        Finance finance = new Finance();
        finance.updateAccountDetails(model, new CallBack<FinanceModel>() {
            @Override
            public void onResponse(FinanceModel model) {
                progressDialog.dismiss();
                if( BankDetail.instance != null) {
                    Utils.ShowSuccessDialog(activity, "Successfully updated bank details", Utils.SUCCESS_DIALOG);
                    BankDetail.instance.setData();
                }

                if(Profile.instance != null) {
                    Profile.instance.setData();
                }

                Utils.saveDataToPreferences(activity,Utils.hasAllAccountDetailsFlag,true);
                if(PurchasedTransaction.instance != null) {
                    PurchasedTransaction.instance.setButtonText();
                }
            }

            @Override
            public void onError(ErrorModel errorModel) {
                progressDialog.dismiss();
                Utils.displayAlert(activity,"Error",errorModel.getError());
            }
        });
    }

    public static void getApiNotification(final Activity activity){
        final String URL = getUrl(activity, R.string.api_notification);
        String token = Utils.getAuthToken(activity);

        if (token == null || token.isEmpty()) {
            return ;
        }

        Log.d("Request",URL);


        AuthorizedStringRequest req = new AuthorizedStringRequest(Request.Method.GET, URL, token,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("Response",response.toString());

                        if(!response.isEmpty()) {
                            try {
                                AppDb db = AppDb.getInstance(activity);
                                NotificationTable notificationTable = (NotificationTable) db.getTableObject(NotificationTable.TABLE_NAME);

                                JSONArray jsonArray = new JSONArray(response);
                                for (int i = 0; i <= jsonArray.length() - 1; i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                                    NotificationModel bean = new NotificationModel();
                                    String id = jsonObject.getString("id") , email = Utils.getStringFromPreferences(activity , Utils.emailKey) ;
                                    bean.setId(id);
                                    bean.setEmail(email);
                                    bean.setLevel(jsonObject.getString("level"));
                                    bean.setIcon(jsonObject.getString("icon"));
                                    String timeStamp = String.valueOf(Utils.convertDateToEpoch(getDate(jsonObject.getString("timestamp"))));
                                    bean.setTimestamp(timeStamp);
                                    bean.setOnclick_Action(jsonObject.getString("onclick_action"));
                                    bean.setDescription(jsonObject.getString("description"));

                                    //change Expiry TimeStamp in SQlite DATE Format
                                    String expiryTimeStamp = String.valueOf(Utils.convertDateToEpoch(getDate(jsonObject.getString("expiry_timestamp"))));
                                    bean.setExpiry_timestamp(expiryTimeStamp);

                                    if( notificationTable.getCount(NotificationTable.COL_NOTIFI_ID ,id ) == 0) {
                                        notificationTable.insertData(bean);
                                        if(MainActivity.activity != null)
                                            MainActivity.activity.incrementNotificationCount();
                                    }

                                }
                            } catch (JSONException | ParseException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                handleNetworkError(activity, error.networkResponse);
            }
        });

        // add the request object to the queue to be executed.
        RetryPolicy policy = new DefaultRetryPolicy(TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        req.setRetryPolicy(policy);
        ApplicationController.getInstance().addToRequestQueue(req);
    }

    private static Date getDate(String timestamp) throws ParseException {
        Date date = null ;
        if( timestamp != null && !timestamp.equalsIgnoreCase("null")) {
            String[] split = timestamp.split("T");
            java.text.DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            date = dateFormat.parse(split[0]);
        }else{
            date = Utils.getAfterDaysDate(30);
        }
        return date ;
    }

    public static void emailQuotes(final Activity activity, String vehicleId) {
        // Fetch the list of uploaded documents
        final String token = Utils.getStringFromPreferences(activity, Utils.authTokenKey);
        final String emailQuoteUrl = getUrl(activity, R.string.email_quote_url, vehicleId);
        final ProgressDialog progressDialog = ProgressDialog.show(activity, "", "Sending Email...", true);

        Log.d("Request",emailQuoteUrl);

        AuthorizedJsonObjectRequest req = new AuthorizedJsonObjectRequest(Request.Method.POST, emailQuoteUrl, null, token, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("Response",response.toString());

                Utils.ShowSuccessDialog(activity, "Successfully sent email to customer!!",Utils.SUCCESS_DIALOG);
                progressDialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                handleNetworkError(activity, error.networkResponse);
            }
        });

        // add the request object to the queue to be executed.
        ApplicationController.getInstance().addToRequestQueue(req);
    }

    public static void updateUser(final Context context, final HashMap<?, ?> params) {
        //  final String userUrl = Config.URL_CURRENT_USER;
        final String deviceUrl = Config.Device_URL ;
        String token = Utils.getAuthToken(context);
        final JSONObject jsonParams = new JSONObject(params);

        if (token == null || token.isEmpty()) {
            Toast.makeText(context.getApplicationContext(),
                    "User is not logged-in", Toast.LENGTH_SHORT).show();
            return;
        }


        Log.d("Request",deviceUrl+jsonParams);

        AuthorizedJsonObjectRequest req2 = new AuthorizedJsonObjectRequest(Request.Method.POST, deviceUrl, jsonParams, token,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("Response",response.toString());
                        if (params.containsKey("gcm_id")) {
                            Utils.saveDataToPreferences(context, Config.GCM_TOKEN_SENT_TO_SERVER, true);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                handleNetworkError(context, error.networkResponse);
            }
        });

        // add the request object to the queue to be executed.
        ApplicationController.getInstance().addToRequestQueue(req2);
    }

    public static void changeActiveStatus(final Context context, final HashMap<?, ?> params) {
        final String userUrl = Config.URL_CURRENT_USER;
        final String deviceUrl = Config.Device_URL ;
        String token = Utils.getAuthToken(context);
        final JSONObject jsonParams = new JSONObject(params);


        Log.d("Request",deviceUrl+jsonParams);

        if(token != null && !token.equals("")) {
            AuthorizedJsonObjectRequest req2 = new AuthorizedJsonObjectRequest(Request.Method.POST, deviceUrl, jsonParams, token,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("Response", response.toString());

                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    handleNetworkError(context, error.networkResponse);
                }
            });
            // add the request object to the queue to be executed.
            ApplicationController.getInstance().addToRequestQueue(req2);
        }
    }

    public static void handleNetworkError(Context context, NetworkResponse response) {
        String title = "Error!";
        String responseString = "";

        if (response == null) {
            Utils.ShowSuccessDialog(context, "Unable to connect. Please check your internet connection.",Utils.ERROR_DIALOG);
            return;
        } else {
            if (response.statusCode == HttpURLConnection.HTTP_UNAUTHORIZED) {
                Utils.ShowSuccessDialog(context, "Please re-login and try again.\n\nPlease contact customer care if the issue persists.",Utils.ERROR_DIALOG);
                return;
            } else if (response.statusCode == HttpURLConnection.HTTP_FORBIDDEN) {
                Utils.ShowSuccessDialog(context,"You are not authorized to perform this action.\n\nPlease contact customer care.",Utils.ERROR_DIALOG);
                return;
            }
        }

        try {
            responseString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
        } catch (UnsupportedEncodingException e) {
            String errorMessage = "Error from server";
            // Display error dialog.
            Utils.ShowSuccessDialog(context,errorMessage,Utils.ERROR_DIALOG);
            return;
        }

        // Try if the response is JSON
        JSONObject jsonResponse;
        try {
            jsonResponse = new JSONObject(responseString);
            String errorMessage = jsonResponse.optString("detail");
            if (!errorMessage.isEmpty()) {
                // Display error dialog.
                Utils.ShowSuccessDialog(context,errorMessage,Utils.ERROR_DIALOG);
            } else {
                Iterator iterator = jsonResponse.keys();
                errorMessage = "";
                while (iterator.hasNext()) {
                    String key = (String) iterator.next();
                    errorMessage += "\n" + jsonResponse.get(key).toString();
                }
                Utils.ShowSuccessDialog(context,errorMessage,Utils.ERROR_DIALOG);
            }
        } catch (JSONException je) {
            // If the response is not JSON, then check if the response body is a string
            // If it is, then display the same error
            if (!responseString.isEmpty()) {
                // Display error dialog.
                Utils.ShowSuccessDialog(context,responseString,Utils.ERROR_DIALOG);
            } else {
                // Display generic error dialog.
                Utils.ShowSuccessDialog(context,"Error form Server",Utils.ERROR_DIALOG);
            }
        }
    }

    public static class CheckOwnMobileNumber extends AsyncTask<String, Void, String> {
        final static long SMS_ROUNDTRIP_TIMOUT = TimeUnit.SECONDS.toMillis(10);
        ProgressDialog progressDialog;
        IntroActivity activity;
        String email_id;
        String pwd;
        String phn_no;

        CheckOwnMobileNumber(IntroActivity activity, String email_id, String pwd, String phn_no) {
            this.activity = activity;
            this.email_id = email_id;
            this.pwd = pwd;
            this.phn_no=phn_no;
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            Utils.hideKeyboard(activity);
            progressDialog = ProgressDialog.show(activity, "", "Verifying...");
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            try {
                SmsManager sms = SmsManager.getDefault();
                timeout();
            } catch (Exception ex) {
                Log.v("Exception :", "" + ex);
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }

            activity.unregisterReceiver(receiver);

            if (otp_code != null) {
                verify_OTP(activity, email_id, pwd, otp_code, phn_no);
            } else {

                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                final View view =  LayoutInflater.from(activity).inflate(R.layout.fragment_otp,null);
                final EditText et_otp = (EditText) view.findViewById(R.id.otp);
                builder.setTitle("Verify Mobile Number");
                builder.setMessage("You have recevied OTP on your registerd mobile number in order to complete your registration");
                builder.setView(view);
                builder.setCancelable(false);
                builder.setPositiveButton(activity.getResources().getString(R.string.submit), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Utils.hideKeyboard(activity);
                        final String otp_code = et_otp.getText().toString();
                        verify_OTP(activity, email_id, pwd, otp_code, phn_no);
                    }
                });
                builder.setNegativeButton(activity.getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                Dialog dialog = builder.create();
                dialog.show();
            }
            super.onPostExecute(result);
        }

        private void timeout() {
            int waited = 0;
            while (waited < SMS_ROUNDTRIP_TIMOUT) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                waited += 100;
                if (otp_code != null) {
                    // waited = SMS_ROUNDTRIP_TIMOUT;
                    break;
                }
            }

            return;
        }
    }

    static void startSDK(Context context){
        // Intialize SDK and start internal process
        Intializer intializer = new Intializer(context);
        if(Config.DOMAIN.contains("dev.renewbuy") || Config.DOMAIN.contains("192.168")) {
            //Dev server Keys
            SplashScreen.APP_ID  = "9abf809e-4449-41c1-bb2a-492b4666441e";
            SplashScreen.API_KEY = "0zSbX5OtyqS86UQItLwJEDFM84XKiWgo";

            // enable dev server as Main Domain
            intializer.enableTesting(true);
        } else{
            SplashScreen.APP_ID  = "71da9a30-8827-4c4f-b336-691ae2e0f6d2" ;
            SplashScreen.API_KEY = "x1hgLp1P5nCRnU37McASD6Dmdkm9MhvO" ;
        }

        intializer.setAppId(SplashScreen.APP_ID);
        intializer.setApiKey(SplashScreen.API_KEY);
        //intializer.setAppId("3d0b9c4f-1048-41d6-8c76-fbe3b4422d6a");
        //intializer.setApiKey("r8iFHINPabe7mpIZWXHLLF3Bx62RE5lk");
        intializer.start();
    }
}
