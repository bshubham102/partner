package com.renewbuy.partners.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.analytics.CampaignTrackingReceiver;
import com.renewbuy.partners.Utils;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;

import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.util.List;

/**
 * Created by sandeep on 13/5/15.
 */
public class InstallReferrerReceiver extends BroadcastReceiver {

    private static String partnerCode = "";

    public static String justcheck="";
    private static String utmSource = "";
    private static String utmMedium = "";
    private static String utmTerm = "";
    private static String utmCampaign = "";
    private static String utmContent = "";

    public static String khem = "";

    public static String STUB_HTTP_PREFIX= "http://www.renewbuy.com/cse/?";

    @Override
    public void onReceive(Context context, Intent intent) {
        partnerCode = "";
        Bundle extras = intent.getExtras();

        try
        {
            // Make sure this is the intent we expect - it always should be.
            Log.i("zzz",intent.getExtras().toString());
            if (intent.getAction().equals("com.android.vending.INSTALL_REFERRER"))
            {
                // This intent should have a referrer string attached to it.
                String rawReferrer = intent.getStringExtra("referrer");

                Log.i("zzz",rawReferrer);
                if (null != rawReferrer)
                {
                    // The string is usually URL Encoded, so we need to decode it.
                    String referrer = URLDecoder.decode(rawReferrer, "UTF-8");

                    String retVal;
                    String addr = STUB_HTTP_PREFIX + referrer;
                    khem=addr;
                    Log.d("ttt",rawReferrer);
                    List<NameValuePair> pairs = null;
                    try {
                        pairs = URLEncodedUtils.parse(new URI(addr), "UTF-8");
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                    }

                    for (NameValuePair pair : pairs) {
                        if (pair.getName().equalsIgnoreCase("partner")) {
                            partnerCode = pair.getValue();
                            justcheck=pair.getValue();

                            Utils.saveDataToPreferences(context, Utils.partnerCodeKey, partnerCode);

                            // If the user is already logged-in (very rare chance),
                            // then try to update the partner code
                            Utils.updatePartnerForExecutive(context);
                        }

                        if(pair.getName().equalsIgnoreCase("utm_source")){
                            utmSource= pair.getValue();
                        }

                        if(pair.getName().equalsIgnoreCase("utm_medium")){
                            utmMedium= pair.getValue();
                        }

                        if(pair.getName().equalsIgnoreCase("utm_term")){
                            utmTerm= pair.getValue();
                        }

                        if(pair.getName().equalsIgnoreCase("utm_content")){
                            utmContent= pair.getValue();
                        }
                        if(pair.getName().equalsIgnoreCase("utm_campaign")){
                            utmCampaign= pair.getValue();
                        }
                    }

                    if(!utmSource.isEmpty() && !utmMedium.isEmpty()) {
                        Utils.saveDataToPreferences(context, Utils.utmSourceKey, utmSource);
                        Utils.saveDataToPreferences(context, Utils.utmMediumKey, utmMedium);
                        Utils.saveDataToPreferences(context, Utils.utmTermKey, utmTerm);
                        Utils.saveDataToPreferences(context, Utils.utmCampaignKey, utmCampaign);
                        Utils.saveDataToPreferences(context, Utils.utmContentKey, utmContent);

                        Utils.updateReferralCode(context, utmSource, utmMedium, utmTerm, utmCampaign, utmContent);
                    }
                }
            }
        }
        catch (Exception e)
        {
            Log.e("REFERRER", e.toString());
        }

        // Call the Google Install tracker
        CampaignTrackingReceiver googleCampaignTrackingReceiver = new CampaignTrackingReceiver();
        googleCampaignTrackingReceiver.onReceive(context, intent);
    }
}
