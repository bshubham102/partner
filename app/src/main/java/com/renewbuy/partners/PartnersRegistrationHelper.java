package com.renewbuy.partners;

import android.app.ProgressDialog;
import android.content.IntentFilter;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.renewbuy.partners.receivers.SMSReceiver;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

/**
 * Created by d2c3 on 16/4/15.
 **/

//9810076486 harsh CA
public class PartnersRegistrationHelper {
    static IntentFilter intentFilter ;
    // Create a JSON REST Call using Volley library.
    public static void createJsonRestCall(final IntroActivity activity, final JSONObject params) {
        final String registerUrl = Api.getUrl(activity, R.string.register_url);
        final ProgressDialog progressDialog = ProgressDialog.show(activity, "", "Registering...");

        Api.receiver = new SMSReceiver();
        intentFilter = new IntentFilter("android.provider.Telephony.SMS_RECEIVED");
        activity.registerReceiver(Api.receiver, intentFilter);
        Api.otp_code = null;

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, registerUrl, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    progressDialog.dismiss();
                    String partnerCodeValue = response.getString("mobile");

                    /*Utils.displayAlert("Welcome", activity.getString(R.string.welcome_to_renewbuy) + "\n\nYour current earning is: " + activity.getString(R.string.Rs) + "0");*/

                    //Save partner code to preferences settings.
                    Utils.saveDataToPreferences(activity, Utils.executiveMobileKey, partnerCodeValue);
                    VolleyLog.v("Response Partner Code:%n %s", response.getString("mobile"));

					/* Log the user in */
                    final String email_id = params.getString("email");
                    final String pwd = params.getString("password");

                    // If this is a new registration from the same mobile, reset the GCM-sent flag
                    Utils.removeDataFromPreferences(activity, Config.GCM_TOKEN_SENT_TO_SERVER);
                    Api.verify_mobile_number(activity, email_id, pwd, "");

                } catch (JSONException e) {
                    VolleyLog.v("JSON Response:%n %s", e.getMessage());
                    String errorMessage = e.getMessage();
                    String title = "JSON Parse Error Message";
                    // Display error dialog.
                    Utils.ShowSuccessDialog(activity, errorMessage,Utils.ERROR_DIALOG);
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                NetworkResponse response = error.networkResponse;
                if (response == null) {
                    Utils.ShowSuccessDialog(activity,"Unable to connect to server.\n\nPlease check your internet connection",Utils.ERROR_DIALOG);
                    return;
                }

                JSONObject jsonResponse;
                String title = "Error!";
                try {
                    String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                    jsonResponse = new JSONObject(jsonString);

                    String errorMessage = jsonResponse.optString("detail");
                    if (!errorMessage.isEmpty()) {
                        VolleyLog.e("Response:%n %s", errorMessage);
                        // Display error dialog.
                        Utils.ShowSuccessDialog(activity, errorMessage,Utils.ERROR_DIALOG);
                    } else {
                        Iterator iterator = jsonResponse.keys();
                        while (iterator.hasNext()) {
                            String key = (String) iterator.next();
                            Utils.ShowSuccessDialog(activity, jsonResponse.get(key).toString(),Utils.ERROR_DIALOG);
                            break;
                        }
                    }
                } catch (UnsupportedEncodingException | JSONException e) {
                    VolleyLog.e("Response:%n %s", error.getMessage());
                    String errorMessage = "Error from server";
                    // Display error dialog.
                    Utils.ShowSuccessDialog(activity,errorMessage,Utils.ERROR_DIALOG);
                }
                VolleyLog.e("Error: ", error.getMessage());
            }
        });

        req.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.MINUTES.toMillis(1), DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // add the request object to the queue to be executed.
        ApplicationController.getInstance().addToRequestQueue(req);
    }
}
