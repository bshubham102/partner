package com.renewbuy.partners;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

public class IntroActivity extends AppCompatActivity {

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final int LOGIN_FRAGMENT_INDEX = 1;
    private static final int REGISTER_FRAGMENT_INDEX = 2;

    //Change the Last Digit for change combination don't remove zero
    private static final String CHEAT  = "0"+"2143";
    private static final int CHEATLENGHT = CHEAT.length()-1;

    private String mTouchString = "0";
    private int cheatTouch = 0;

    private ImageView touch1,touch2,touch3,touch4, logo;

    //Cheat Views and Values over

    private static final String TAG = "IntroActivity";
    public static IntroActivity activity ;

    private TextView sign_in , register ;

    //current selected Fragment ;
    //value : 1 for loginFragment & 2 for RegisterFragment
    private int mSelectedFragment ;

    FragmentManager fragmentManager;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_intro);

        int height = getWindowManager().getDefaultDisplay().getHeight();

        logo = (ImageView) findViewById(R.id.img_logo);
        RelativeLayout.LayoutParams params = new RelativeLayout
                                              .LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.CENTER_HORIZONTAL,RelativeLayout.TRUE);
        params.setMargins(0,height/13,0,height/20);
        if (logo != null) {
            logo.setLayoutParams(params);
        }

        initCheatsViews();

        //Intialize the textview for swapping the fragments
        sign_in  = (TextView) findViewById(R.id.txt_sign_in);
        register = (TextView) findViewById(R.id.txt_sign_up);

        sign_in.setOnClickListener(new MyClickListener());
        register.setOnClickListener(new MyClickListener());

        activity = this ;
        fragmentManager = getSupportFragmentManager();
        mSelectedFragment = LOGIN_FRAGMENT_INDEX ;
        goToSignIn();
    }

    private void initCheatsViews() {
        touch1 = (ImageView) findViewById(R.id.touch1);
        touch2 = (ImageView) findViewById(R.id.touch2);
        touch3 = (ImageView) findViewById(R.id.touch3);
        touch4 = (ImageView) findViewById(R.id.touch4);

        touch1.setOnClickListener(new CheatListener());
        touch2.setOnClickListener(new CheatListener());
        touch3.setOnClickListener(new CheatListener());
        touch4.setOnClickListener(new CheatListener());
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if(hasFocus) {
            float logoX = logo.getX();
            float logoY = logo.getY();
            float logoWidth = logo.getWidth() ;
            float logoHeight = logo.getHeight() ;

            touch1.setX(logoX - touch1.getWidth()/2);
            touch1.setY(logoY - touch1.getHeight()/2);

            touch2.setX(logoX  - touch2.getWidth()/2);
            touch2.setY(logoY+logoHeight - touch2.getHeight()/2);

            touch4.setX(logoX+logoWidth  - touch2.getWidth()/2);
            touch4.setY(logoY+logoHeight - touch2.getHeight()/2);

            touch3.setX(logoX+logoWidth - touch1.getWidth()/2);
            touch3.setY(logoY - touch1.getHeight()/2);
        }
    }

    public void goToSignIn() {
        Utils.ChangeFragment(fragmentManager,new LoginFragment(),null,Utils.REPLACE,R.id.frag_container,"Login Fragment");
    }
    public void goToSignUp() {
        Utils.ChangeFragment(fragmentManager,new RegisterFragment(),null,Utils.REPLACE,R.id.frag_container,"Register Fragment");
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    public boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,	PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    public void goToHomePage() {
        Intent intent = new Intent(this,MainActivity.class);
        //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        switch(mSelectedFragment) {
            case LOGIN_FRAGMENT_INDEX:
                finish();
            break;

            case REGISTER_FRAGMENT_INDEX:
                int count = fragmentManager.getBackStackEntryCount() ;
                String name = fragmentManager.getBackStackEntryAt(count -1).getName();
                if(!name.equals("Register Fragment")) {
                    if (count > 2)
                        fragmentManager.popBackStack();
                    else
                        finish();
                }else{
                   // fragmentManager.popBackStack(null,FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    finish();
                }
            break;

        }
    }



    private class MyClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.txt_sign_in:
                    if( mSelectedFragment != LOGIN_FRAGMENT_INDEX) {
                        goToSignIn();
                        changeTextColor(LOGIN_FRAGMENT_INDEX);
                        mSelectedFragment=LOGIN_FRAGMENT_INDEX ;
                    }
                break;
                case R.id.txt_sign_up:
                    if( mSelectedFragment != REGISTER_FRAGMENT_INDEX) {
                        goToSignUp();
                        changeTextColor(REGISTER_FRAGMENT_INDEX);
                        mSelectedFragment=REGISTER_FRAGMENT_INDEX;
                    }
                break;
            }
        }
    }

    private void changeTextColor(int pos) {
        if(pos == LOGIN_FRAGMENT_INDEX ) {
            sign_in.setTextColor(getResources().getColor(R.color.colorPrimary));
            register.setTextColor(getResources().getColor(R.color.textHint));
        } else {
            register.setTextColor(getResources().getColor(R.color.colorPrimary));
            sign_in.setTextColor(getResources().getColor(R.color.textHint));
        }
    }

    private class CheatListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            int id = v.getId();
            switch (id) {
                case R.id.touch1:
                    mTouchString+="1";
                break;

                case R.id.touch2:
                    mTouchString+="2";
                break;

                case R.id.touch3:
                    mTouchString+="3";
                break;

                case R.id.touch4:
                    mTouchString+="4";
                break;
            }
            cheatTouch = cheatTouch+1;

            if (cheatTouch == CHEATLENGHT) {
                if( mTouchString.equals(CHEAT) ){
                    final EditText myMsg = new EditText(activity);

                    final AlertDialog dialog = ShowDialog(activity,"Enter Url","http://dev.renewbuy.com",myMsg,"OK","Cancel",false).create();
                    dialog.show();
                    dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimary));
                    dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorPrimary));
                    dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String url=myMsg.getText().toString();
                            dialog.dismiss();
                            Utils.DOMAIN = url;
                        }
                    });
                    dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            cheatTouch = 0;
                            mTouchString = "0";
                        }
                    });
                    Toast.makeText(activity, "Combination Granted", Toast.LENGTH_SHORT).show();

                } else {
                    cheatTouch = 0;
                    mTouchString = "0";
                }
            }
        }
    }

    private AlertDialog.Builder ShowDialog(Activity activity, String fieldHint, String fieldValue, EditText editText,
                                           String positiveText, String negativeText,boolean cancleable){

        AlertDialog.Builder dlgAlert = new AlertDialog.Builder(activity);

        if(editText != null) {
            editText.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            editText.setHint(fieldHint);
            editText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
            editText.setGravity(Gravity.CENTER_HORIZONTAL);
            editText.setText(fieldValue);
            editText.setBackgroundResource(R.drawable.border_box);

            TextView textView = new TextView(this);
            textView.setText(getResources().getString(R.string.cheat_string));
            textView.setPadding(10,10,10,10);
            textView.setGravity(Gravity.CENTER);

            ImageView logo = new ImageView(this);
            logo.setImageDrawable(getResources().getDrawable(R.drawable.cheat_logo));

            LinearLayout ll = new LinearLayout(activity);
            ll.setOrientation(LinearLayout.VERTICAL);
            ll.setPadding(10, 10, 10, 10);
            ll.setGravity(Gravity.CENTER);

            ll.addView(logo);
            ll.addView(textView);
            ll.addView(editText);

            dlgAlert.setView(ll);

            dlgAlert.setCancelable(cancleable);
            dlgAlert.setPositiveButton(positiveText, null);
            dlgAlert.setNegativeButton(negativeText, null);
        }
        return dlgAlert ;
    }
}
