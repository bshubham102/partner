package com.renewbuy.partners;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Created by sandeep on 9/5/15.
 */
public class AuthorizedJsonObjectRequest extends JsonObjectRequest {

    private String ACCESS_TOKEN = "";
    private int originalMethod;

    public AuthorizedJsonObjectRequest(int method, String url, JSONObject params, Response.Listener<JSONObject> listener,
                                       Response.ErrorListener errorListener) {
        super(method, url, params, listener, errorListener);
    }

    public AuthorizedJsonObjectRequest(int method, String url, JSONObject params, String token, Response.Listener<JSONObject> listener,
                                       Response.ErrorListener errorListener) {
        super((method == Method.PATCH ? Method.POST: method), url, params, listener, errorListener);
        this.ACCESS_TOKEN = token;
        this.originalMethod = method;
        super.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.MINUTES.toMillis(1), DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> headers = new HashMap<String, String>();
        String auth = "Token " + this.ACCESS_TOKEN;
        headers.put("Authorization", auth);
        headers.put("Content-Type", "application/json");
        headers.put("Accept", "application/json");

        if (this.originalMethod == Method.PATCH) {
            headers.put("X-HTTP-Method-Override", "PATCH");
        }

        return headers;
    }

    @Override
    protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
        try {
            String contentType = response.headers.get("Content-Type");
            if (contentType != null && contentType.equalsIgnoreCase("application/json")) {
                String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                return Response.success(new JSONObject(jsonString),
                        HttpHeaderParser.parseCacheHeaders(response));
            } else {
                return Response.success(new JSONObject(), HttpHeaderParser.parseCacheHeaders(response));
            }
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JSONException je) {
            return Response.error(new ParseError(je));
        }
    }
}
