package com.renewbuy.partners;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.DownloadListener;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 */
public class WebViewFragment extends Fragment implements MultiUseActivity.doWebviewBack,MainActivity.onBackListener{

    public static final String SELECT_URL = "SELECT_URL";
    public static final String SHOW_MENU = "SHOW_MENU";

    private boolean onSecondPage = false, isPaymentComplete = false, onPaymentPage = false;

    private View view ;
    private Context context ;
    private WebView webView ;
    private ProgressBar progressBar ;

    private String URL ;

    private boolean showMenu = false;

    public WebViewFragment() {
        // Required empty public constructor
    }

    public static WebViewFragment newInstance(boolean isHealthTab) {
        Bundle args = new Bundle();
        args.putBoolean(SHOW_MENU,isHealthTab);

        WebViewFragment fragment = new WebViewFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {

        if(view == null) {
            // Inflate the layout for this fragment
            view = inflater.inflate(R.layout.fragment_web_view, container, false);

            //Get the reference of parent activity
            context = getActivity();

            //Intialize the views
            init(view);

            //get bundle arguments ;
            savedInstanceState = getArguments();
            if (savedInstanceState != null) {
                URL = savedInstanceState.getString(SELECT_URL);
                showMenu = savedInstanceState.getBoolean(SHOW_MENU);
            }

            //has OptionMenu
            if(!showMenu) {
                setHasOptionsMenu(true);
            }else{
                URL = Api.getUrl((Activity) context, R.string.health_url,
                                                    Utils.getStringFromPreferences(context,Utils.executiveIdKey)) ;
            }

            //Start Operation in WebView
            startWebView();
        } else {
            container.removeView(view);
        }

        return view ;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.webview_menu,menu);
    }

    private void init(View view) {
        webView = (WebView) view.findViewById(R.id.m_webView);
        progressBar = (ProgressBar) view.findViewById(R.id.webview_progressbar);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            webView.setWebContentsDebuggingEnabled(true);
        }
        webView.setWebViewClient(new RbWebClient(getActivity()) {
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                if (errorCode != WebViewClient.ERROR_FILE_NOT_FOUND) {
                    webView.loadData("<br> Error!!! Unable to connect to server. <br> " +
                                    "Please check your internet connection or try again after sometime. <br>" +
                                    "<br> If the problem persists, please call customer care " + getString(R.string.rb_toll_free),
                            "text/html; charset=utf-8", "UTF-8");
                }
            }
        });

        final ProgressBar Pbar = (ProgressBar) view.findViewById(R.id.webview_progressbar);
        webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                if (progress < 100 && Pbar.getVisibility() == ProgressBar.GONE) {
                    Pbar.setVisibility(ProgressBar.VISIBLE);
                }
                Pbar.setProgress(progress);
                if (progress == 100) {
                    Pbar.setVisibility(ProgressBar.GONE);
                }
            }

           /* public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
                if (consoleMessage.messageLevel() == ConsoleMessage.MessageLevel.ERROR) {
                    Toast.makeText(context, consoleMessage.lineNumber() + ": " + consoleMessage.message().toString(), Toast.LENGTH_LONG).show();
                }
                return true;
            }*/
        });
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void startWebView() {
        String versionName="";
        int versionCode=0;

        try {
            versionName = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0).versionName;
            versionCode = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0).versionCode;

        } catch (Exception ignored) {
        }
        webView.getSettings().setJavaScriptEnabled(true);
        String ua=webView.getSettings().getUserAgentString();
        //Utils.displayAlert("ok",ua+"  RB Partners/"+versionName+"." +String.valueOf(versionCode));
        webView.getSettings().setUserAgentString(ua+" RB Partners/"+versionName+"." +String.valueOf(versionCode));

        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setAppCacheEnabled(true);
        webView.getSettings().setAllowFileAccess(true);
        webView.setDownloadListener(new DownloadListener() {
            @Override
            public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimetype, long contentLength) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
            }
        });

        webView.loadUrl(URL + "&source=app");

    }

    @Override
    public void onBackPress(Activity activity) {
        if(!isPaymentComplete) {
            if (webView.canGoBack()) {
                if (onSecondPage) {
                    webView.loadUrl("javascript:androidBack()");
                } else {
                    webView.goBack();
                }
            } else {
                webView.destroy();
                activity.finish();
            }
        }else{
            webView.destroy();
            activity.finish();
        }
    }

    @Override
    public void donBackPress(ViewPager viewPager) {
        if(!onPaymentPage) {
            if (webView.canGoBack()) {
                webView.goBack();
            } else {
                webView.loadUrl(URL);
                viewPager.setCurrentItem(0);
            }
        }else{
            webView.loadUrl(URL);
            viewPager.setCurrentItem(0);
        }
    }

    // RenewBuy Client...
    public class RbWebClient extends WebViewClient {
        private final Activity activity;

        public RbWebClient(Activity activity) {
            this.activity = activity;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            Log.i("onPageStarted: ",url);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            Log.i("onPagestarted: ",url);

            onSecondPage  = url.toLowerCase().contains("motor/quote/3");
            onPaymentPage = !url.toLowerCase().contains("renewbuy.com");

            if(url.toLowerCase().contains("/motor/policy/confirm/")){
                isPaymentComplete = true ;
                webView.clearCache(true);
                webView.clearFormData();
            }
        }

        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            Toast.makeText(this.activity, "Oh no! " + description, Toast.LENGTH_SHORT).show();
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {

            if (url.toLowerCase().contains("tel")) {
                try {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse(url));
                    MultiUseActivity.instance.startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return true;

            } else if (url.toLowerCase().contains("mailto:")) {
                try {
                    String[] email = url.split(":");
                    Intent intent = new Intent(Intent.ACTION_SENDTO,Uri.fromParts("mailto",email[1], null));
                    MultiUseActivity.instance.startActivity(intent);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                return true;

            } else if (url.endsWith(".pdf")) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                // if want to download pdf manually create AsyncTask here
                // and download file
                return true;

            }else if(url.toLowerCase().contains("renewbuy.com")){
                String newurl = url+"&source=app";
                view.loadUrl(newurl);
                return true ;

            }else {
                view.loadUrl(url);
            }

            return false;// true won't let you load the url
        }
    }

}
