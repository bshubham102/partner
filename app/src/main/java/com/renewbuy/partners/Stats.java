package com.renewbuy.partners;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.renewbuy.partners.db.AppDb;
import com.renewbuy.partners.db.CustomerTable;
import com.renewbuy.partners.db.PaymentTable;
import com.renewbuy.partners.db.StatsBean;
import com.renewbuy.partners.db.StatsTable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Vector;

public class Stats extends Fragment {

    private OnStatUploadClickListener mListener ;

    //Pie chart Arc's Background Colors
    private static final String PURCHASED_COLOR  = "#85C442";
    private static final String ACCEPTED_COLOR   = "#159f20";
    private static final String SUBMITTED_COLOR   = "#007bff";
    private static final String MISSED_COLOR      = "#b7abab";
    private static final String REJECTED_COLOR   = "#ff0000";

    private boolean byFilter = false;

    private int Selected = 0;
    private String[] optionArray;
    private String[] filterHeading ;

    private Context context;
    private TextView txt_sold_car, txt_sold_bike, txt_earned_car, txt_earned_bike,
                     txt_submitted, txt_accepted, txt_missed, txt_rejected, txt_purchased,txt_income_filter,
                     txt_income, txt_last_income_by_month_or_year,
                     txt_potential_income,txt_missed_income,
                     txt_balance_potential ,empty_pie_txt;
    private ProgressBar progress ;

    private LinearLayout fill_stats_layout, upload_layout, upload_layout_2, pie_layout;
    private RelativeLayout empty_stats_layout  ;

    private PieChart mChart ;
    public static Stats instance;

    private AppDb db ;
    private StatsTable statsTable ;
    private CustomerTable customerTable ;
    private PaymentTable paymentTable ;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof MainActivity) {
            mListener = (OnStatUploadClickListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_stats, container, false);

        //get context of the main parent activity
        context = getActivity();

        //Intialize all textview and chart
        init(view);

        //set Total Last Year or Month Income , potential balance ,
        // missed balance , exepected potential in list
        // at '0'index is yearly income &
        // at '1'index is monthly income
        initializeValue(0);

        //intialize the static instance of current fragment
        instance = this ;

        return view ;
    }

    private void init(View view){

        db = AppDb.getInstance(context);
        statsTable = (StatsTable) db.getTableObject(StatsTable.TABLE_NAME);
        customerTable = (CustomerTable) db.getTableObject(CustomerTable.TABLE_NAME);
        paymentTable = (PaymentTable) db.getTableObject(PaymentTable.TABLE_NAME);

        //Intialize the layout
        fill_stats_layout = (LinearLayout) view.findViewById(R.id.fill_stat_layout);
        upload_layout = (LinearLayout) view.findViewById(R.id.stat_upload_layout);
        upload_layout_2 = (LinearLayout) view.findViewById(R.id.stat_upload_layout_2);
        pie_layout = (LinearLayout) view.findViewById(R.id.pie_layout);
        empty_stats_layout = (RelativeLayout) view.findViewById(R.id.empty_stat_layout);


        progress = (ProgressBar) view.findViewById(R.id.stats_progress);

        // Intialize the Views
        empty_pie_txt = (TextView) view.findViewById(R.id.pie_empty_txt);

        txt_sold_car = (TextView) view.findViewById(R.id.txt_total_sold_car);
        txt_sold_bike = (TextView) view.findViewById(R.id.txt_total_sold_bike);

        txt_earned_car = (TextView) view.findViewById(R.id.txt_total_earned_car);
        txt_earned_bike = (TextView) view.findViewById(R.id.txt_total_earned_bike);

        txt_submitted = (TextView) view.findViewById(R.id.txt_total_submit);
        txt_accepted = (TextView) view.findViewById(R.id.txt_total_accept);
        txt_missed = (TextView) view.findViewById(R.id.txt_total_missed);
        txt_rejected = (TextView) view.findViewById(R.id.txt_total_reject);

        //txt_purchased = (TextView) view.findViewById(R.id.txt_total_submit);
        txt_income_filter = (TextView) view.findViewById(R.id.txt_income_filter);
        txt_income = (TextView) view.findViewById(R.id.txt_income_recevied);
        txt_potential_income = (TextView) view.findViewById(R.id.txt_potential_income);
        txt_missed_income = (TextView) view.findViewById(R.id.txt_missed_income);
        txt_balance_potential = (TextView) view.findViewById(R.id.txt_balance_potential);
        txt_last_income_by_month_or_year = (TextView) view.findViewById(R.id.last_year_month);


        //Intialize Chart
        mChart = (PieChart) view.findViewById(R.id.pie_chart);
        mChart.setUsePercentValues(false);
        mChart.setDescription("");

        mChart.setDrawCenterText(false);
        mChart.setDrawHoleEnabled(false);
        mChart.setRotationEnabled(false);

        //Intialize Array
        optionArray   = getResources().getStringArray(R.array.year_monrh_filter);
        filterHeading = getResources().getStringArray(R.array.purchased_filter);

        setListener();
    }

    private void setListener() {
        upload_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mListener != null)
                    mListener.onUploadClickListener(true,1);
            }
        });

        upload_layout_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mListener != null)
                    mListener.onUploadClickListener(true,1);
            }
        });
    }

    private void initializeValue(int pos) {

        //Intialize the filter Text
        initTextView(filterHeading[pos]);

        final int statsBy ;
        if(pos == 0){
            statsBy = StatsTable.YEAR;
        } else if(pos == 1){
            statsBy = StatsTable.MONTH ;
        } else{
            statsBy = StatsTable.TODAY ;
        }

        Vector<StatsBean> list = statsTable.getstatsByDate(context,statsBy);
        StatsBean bean = null;
        if(list.size() > 0 ) {
            bean = list.firstElement();

            //set Total purchased number and Earning acc to 2W and 4W
            txt_sold_car.setText(""+bean.getCar_sold());
            txt_sold_bike.setText(""+bean.getBike_sold());

            txt_earned_car.setText(context.getResources().getString(R.string.ruppee)+bean.getCar_earning());
            txt_earned_bike.setText(context.getResources().getString(R.string.ruppee)+bean.getBike_earning());

            // Set income to textView to set Current Year Income
            txt_income.setText(context.getResources().getString(R.string.ruppee)+bean.getActual_commission());

            //Set total Potential Income
            txt_potential_income.setText(context.getResources().getString(R.string.ruppee)+bean.getExpected_commission());

            //Set total Missed Income
            txt_missed_income.setText(context.getResources().getString(R.string.ruppee)+bean.getMissed_commission());

            //Set total Balance Potential Income
            txt_balance_potential.setText(context.getResources().getString(R.string.ruppee)+bean.getBalance_commission());
        }

        //TODO: Set all Values
        setValues(bean);
    }

    private void setValues(StatsBean bean){

        //get Lead count and transaction count
        int count = customerTable.getAllData(context).size();
        int transaction_count = paymentTable.getAllData(context).size();

        //if any one has value then show stats layout
        if(count > 0 || transaction_count > 0) {
            progress.setVisibility(View.GONE);
            fill_stats_layout.setVisibility(View.VISIBLE);
            empty_stats_layout.setVisibility(View.GONE);

            //total policy by executive
            int accepted  = bean!=null ? bean.getAccpet_policy() : 0;
            int submit = bean!=null ? bean.getSubmit_policy() : 0;
            int reject = bean!=null ? bean.getRejected_policy() : 0;
            int missed = bean!=null ? bean.getMissed_policy() : 0;

            if(accepted == 0 && submit==0 && reject==0 && missed==0){
                empty_pie_txt.setVisibility(View.VISIBLE);
                pie_layout.setVisibility(View.GONE);
            }else  {
                empty_pie_txt.setVisibility(View.GONE);
                pie_layout.setVisibility(View.VISIBLE);

                mChart.invalidate();

                txt_submitted.setText(String.valueOf(submit));
                txt_accepted.setText(String.valueOf(accepted));
                txt_missed.setText(String.valueOf(missed));
                txt_rejected.setText(String.valueOf(reject));

                ArrayList<PieEntry> data = new ArrayList<>();
                data.add(new PieEntry(submit));
                data.add(new PieEntry(accepted));
                data.add(new PieEntry(missed));
                data.add(new PieEntry(reject));

                ArrayList<Integer> colors = new ArrayList<>();
                colors.add(Color.parseColor(SUBMITTED_COLOR));
                colors.add(Color.parseColor(ACCEPTED_COLOR));
                colors.add(Color.parseColor(MISSED_COLOR));
                colors.add(Color.parseColor(REJECTED_COLOR));

                PieDataSet dataSet = new PieDataSet(data,"");
                dataSet.setSliceSpace(1.5f);
                dataSet.setSelectionShift(0f);
                dataSet.setColors(colors);

                PieData dataValue = new PieData(dataSet);
                dataValue.setDrawValues(false);

                mChart.setData(dataValue);
                mChart.getLegend().setEnabled(false);
            }
        }else{
            progress.setVisibility(View.GONE);
            empty_stats_layout.setVisibility(View.VISIBLE);
            fill_stats_layout.setVisibility(View.GONE);
        }
    }

    private SpannableString getSpanableString(String line , int startIndex , int lastIndex) {
        SpannableString spanstring = new SpannableString(line);
        ClickableSpan span = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                Utils.showFilterDialog(context, getOptionList(), Selected, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(Selected != which) {
                            byFilter = true ;
                            //TODO: Redraw Income Again
                            initializeValue(which);

                            //TODO: Set selected item id
                            Selected = which;
                        }
                        dialog.dismiss();
                    }
                });
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                ds.setColor(getResources().getColor(R.color.colorAccent));
                ds.setUnderlineText(false);
                //
            }
        };

        spanstring.setSpan(span,startIndex,lastIndex, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return spanstring ;
    }

    private void initTextView(String titlestring){
        SpannableString text = getSpanableString(titlestring,titlestring.indexOf("t"),titlestring.length());
        txt_income_filter.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.arrow_drop_down,0);
        txt_income_filter.setText(text);
        txt_income_filter.setTypeface(Typeface.DEFAULT_BOLD);
        txt_income_filter.setMovementMethod(LinkMovementMethod.getInstance());
    }

    private List<String> getOptionList() {
        List<String> option = new ArrayList<>();
        Collections.addAll(option, optionArray);
        return  option ;
    }

    public void refreshDataSet(){
        Selected = 0 ;
        mChart.invalidate();
        initializeValue(0);
    }

    public interface OnStatUploadClickListener{
        void onUploadClickListener(boolean isClick,int pos);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null ;
    }
}
