package com.renewbuy.partners;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.renewbuy.mercury.Vehicles;
import com.renewbuy.mercury.callBacks.CallBack;
import com.renewbuy.mercury.models.CityList;
import com.renewbuy.mercury.models.ErrorModel;
import com.renewbuy.mercury.models.VehicleList;
import com.renewbuy.partners.Adapter.DynamicListAdapter;
import com.renewbuy.partners.modelClass.DynamicListModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class DynamicList extends Fragment {
//555
    private int PURPOSE ;
    private String URL ;

    private View view ;
    private Context context;
    private Activity activity ;
    private EditText search;
    private ListView listView ;
    private LinearLayout noDatalinearLayout ;
    private ProgressBar progressBar ;

    private List<DynamicListModel> list = new ArrayList<>();
    private DynamicListAdapter adapter ;

    private Vehicles vehicles ;

    public DynamicList() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_item_list, container, false);

        //get parentActivity reference
        context = activity = getActivity();

        if( savedInstanceState != null) {
            PURPOSE = savedInstanceState.getInt(MultiUseActivity.SELECT_OPREATION);
        } else {
            savedInstanceState = getArguments();
            PURPOSE = savedInstanceState.getInt(MultiUseActivity.SELECT_OPREATION);
        }

        //Intializing the Views
        init(view);

        enableSearch(false);

        // Get Url for a various operation
        getURL(PURPOSE) ;

        return view;
    }

    private void init(View view) {
        search = (EditText) view.findViewById(R.id.et_search);
        listView = (ListView) view.findViewById(R.id.dynamicList);
        progressBar = (ProgressBar) view.findViewById(R.id.list_progressBar);
        noDatalinearLayout = (LinearLayout) view.findViewById(R.id.no_data_layout);

        vehicles = new Vehicles();

        //setListeners
        setListeners();
    }

    private void setListeners() {
        search.addTextChangedListener(new MyTextChangeListener());
    }

    public void getURL(int PURPOSE) {

        switch (PURPOSE) {

            case Utils.MANUFACTURER_REQUEST:
                vehicles.getManufactuer(new CallBack<VehicleList>() {
                    @Override
                    public void onResponse(VehicleList model) {

                        progressBar.setVisibility(View.GONE);

                        int length = model.getList().size() ;
                        if(length > 0) {

                            enableSearch(true);

                            for (int i = 0; i <= length - 1; i++) {
                                int id = Integer.parseInt(model.getList().get(i).getId());
                                String name = model.getList().get(i).getName();

                                DynamicListModel listmodel = new DynamicListModel();
                                listmodel.setId(id);
                                listmodel.setName(name);

                                list.add(listmodel);
                            }

                            adapter = new DynamicListAdapter(context, R.layout.list_textview, list, Utils.MANUFACTURER_REQUEST);
                            listView.setAdapter(adapter);
                        } else {
                            noDatalinearLayout.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void onError(ErrorModel errorModel) {
                        progressBar.setVisibility(View.GONE);
                        noDatalinearLayout.setVisibility(View.VISIBLE);
                    }
                });
            break;

            case Utils.MODEL_REQUEST:
                vehicles.getModel(String.valueOf(PolicyQuotesWebView.intance.ManufacturerID), new CallBack<VehicleList>() {
                    @Override
                    public void onResponse(VehicleList model) {

                        progressBar.setVisibility(View.GONE);

                        int length = model.getList().size() ;
                        if(length > 0) {

                            enableSearch(true);

                            for (int i = 0; i <= length - 1; i++) {
                                int id = Integer.parseInt(model.getList().get(i).getId());
                                String name = model.getList().get(i).getName();

                                DynamicListModel listmodel = new DynamicListModel();
                                listmodel.setId(id);
                                listmodel.setName(name);

                                list.add(listmodel);
                            }

                            adapter = new DynamicListAdapter(context, R.layout.list_textview, list, Utils.MODEL_REQUEST);
                            listView.setAdapter(adapter);
                        } else {
                            noDatalinearLayout.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void onError(ErrorModel errorModel) {
                        progressBar.setVisibility(View.GONE);
                        noDatalinearLayout.setVisibility(View.VISIBLE);
                    }
                });
            break ;

            case Utils.VARIANT_REQUEST:
                vehicles.getVariant(String.valueOf(PolicyQuotesWebView.intance.ModelID), new CallBack<VehicleList>() {
                    @Override
                    public void onResponse(VehicleList model) {

                        progressBar.setVisibility(View.GONE);

                        int length = model.getList().size() ;
                        if(length > 0) {

                            enableSearch(true);

                            for (int i = 0; i <= length - 1; i++) {
                                int id = Integer.parseInt(model.getList().get(i).getId());
                                String name = model.getList().get(i).getName();

                                DynamicListModel listmodel = new DynamicListModel();
                                listmodel.setId(id);
                                listmodel.setName(name);

                                list.add(listmodel);
                            }

                            adapter = new DynamicListAdapter(context, R.layout.list_textview, list, Utils.VARIANT_REQUEST);
                            listView.setAdapter(adapter);
                        }else {
                            noDatalinearLayout.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void onError(ErrorModel errorModel) {
                        progressBar.setVisibility(View.GONE);
                        noDatalinearLayout.setVisibility(View.VISIBLE);
                    }
                });
            break ;

            case Utils.CITY_REQUEST:
                vehicles.getCity(new CallBack<CityList>() {
                    @Override
                    public void onResponse(CityList model) {

                        progressBar.setVisibility(View.GONE);

                        int length = model.getList().size() ;
                        if(length > 0) {

                            enableSearch(true);

                            for (int i = 0; i <= length - 1; i++) {
                                int id = Integer.parseInt(model.getList().get(i).getId());
                                String name = model.getList().get(i).getName();

                                DynamicListModel listmodel = new DynamicListModel();
                                listmodel.setId(id);
                                listmodel.setName(name);

                                list.add(listmodel);
                            }
                            adapter = new DynamicListAdapter(context, R.layout.list_textview, list, Utils.CITY_REQUEST);
                            listView.setAdapter(adapter);
                        } else {
                            noDatalinearLayout.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void onError(ErrorModel errorModel) {
                        progressBar.setVisibility(View.GONE);
                        noDatalinearLayout.setVisibility(View.VISIBLE);
                    }
                });
            break;
        }
    }

    private class MyTextChangeListener implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
         adapter.getFilter().filter(s.toString());
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }

    private void enableSearch(boolean b) {
        search.setEnabled(b);
    }

}
