package com.renewbuy.partners;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by sandeep on 9/5/15.
 */
public class AuthorizedJsonArrayRequest extends JsonArrayRequest {

    private String ACCESS_TOKEN = "";

    public AuthorizedJsonArrayRequest(String url, Response.Listener<JSONArray> listener,
                                      Response.ErrorListener errorListener) {
        super(url, listener, errorListener);
    }

    public AuthorizedJsonArrayRequest(String url, String token, Response.Listener<JSONArray> listener,
                                      Response.ErrorListener errorListener) {
        super(url, listener, errorListener);
        this.ACCESS_TOKEN = token;
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> headers = new HashMap<String, String>();
        String auth = "Token " + this.ACCESS_TOKEN;
        headers.put("Authorization", auth);
        headers.put("Content-Type", "application/json");
        headers.put("Accept", "application/json");
        return headers;
    }

}
