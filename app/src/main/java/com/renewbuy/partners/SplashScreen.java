package com.renewbuy.partners;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

public class SplashScreen extends AppCompatActivity {

    private static final int SPLASH_DISPLAY_LENGTH = 1000;

    private String notifcationAction = null ;
    static String APP_ID, API_KEY ;
    Intent mainIntent ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        Api.startSDK(this);

        // check user already login or not
        String token = Utils.getAuthToken(this);

        if(token != null) {
            mainIntent = new Intent(SplashScreen.this, MainActivity.class);
            Bundle bundle = getIntent().getExtras();
            if(bundle != null) {
                notifcationAction = bundle.getString(Utils.notificationTitle);
                if (notifcationAction != null)
                    mainIntent.putExtra(Utils.notificationTitle, notifcationAction);
            }
        } else {
            mainIntent = new Intent(SplashScreen.this, IntroActivity.class);
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(mainIntent);
                finish();
            }
        }, SPLASH_DISPLAY_LENGTH);
    }
}
