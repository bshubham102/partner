package com.renewbuy.partners;

import android.app.Application;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.renewbuy.mercury.Authentication;
import com.renewbuy.mercury.Prefs;
import com.renewbuy.partners.modelClass.LruBitmapCache;

public class ApplicationController extends Application {
	public static GoogleAnalytics googleAnalytics;
	public static Tracker googleTracker;

	/**
	 * Log or request TAG
	 */
	public static final String TAG = "VolleyPatterns";

	/**
	 * Global request queue for Volley
	 */
	private RequestQueue mRequestQueue;

	private ImageLoader mImageLoader;

	/*
	 * A singleton instance of the application class for easy access in other
	 * places
	 */
	private static ApplicationController sInstance;

	@Override
	public void onCreate() {
		super.onCreate();

		// initialize the singleton
		sInstance = this;

		googleAnalytics = GoogleAnalytics.getInstance(this);
		googleAnalytics.setLocalDispatchPeriod(1800);

		googleTracker = googleAnalytics.newTracker(R.xml.global_tracker);
		googleTracker = googleAnalytics.newTracker("UA-57859131-5"); // Replace with actual tracker/property Id
		//googleTracker.enableExceptionReporting(true);
		googleTracker.enableAdvertisingIdCollection(true);
		googleTracker.enableAutoActivityTracking(true);

		boolean flag = !(Utils.getBooleanFromPreferences(this,  Utils.initFlag));
		//DataCollection collector = new DataCollection(flag);
		//collector.collect(this);

		Utils.saveDataToPreferences(this, Utils.initFlag, true);

		//Getting previous token if not exsist then logout user and remove
		// all perferences
		Prefs.getInstance().loadPrefs(this);
		Log.i(TAG, "Token "+Prefs.getInstance().token);

		String token = Prefs.getInstance().token;

		if(token != null && token.equals("")){
			Authentication authentication = new Authentication();
			authentication.logout(this);

			Utils.logoutUser(this);

			if (Utils.getStringFromPreferences(getApplicationContext(), Utils.executivePartnerCode) != null) {
				Utils.removeDataFromPreferences(getApplicationContext(), Utils.executivePartnerCode);
			}
		}
	}

	/**
	 * @return ApplicationController singleton instance
	 */
	public static synchronized ApplicationController getInstance() {
		return sInstance;
	}

	/**
	 * @return The Volley Request queue, the queue will be created if it is null
	 */
	public RequestQueue getRequestQueue() {
		// lazy initialize the request queue, the queue instance will be
		// created when it is accessed for the first time
		if (mRequestQueue == null) {
			mRequestQueue = Volley.newRequestQueue(getApplicationContext());
		}

		return mRequestQueue;
	}

	public ImageLoader getImageLoader() {
		getRequestQueue();
		if (mImageLoader == null) {
			mImageLoader = new ImageLoader(this.mRequestQueue,new LruBitmapCache());
		}
		return this.mImageLoader;
	}

	/**
	 * Adds the specified request to the global queue, if tag is specified then
	 * it is used else Default TAG is used.
	 *
	 * @param req
	 * @param tag
	 */
	public <T> void addToRequestQueue(Request<T> req, String tag) {
		// set the default tag if tag is empty
		req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);

		com.android.volley.VolleyLog.d("Adding request to queue: %s", req.getUrl());

		getRequestQueue().add(req);
	}

	/**
	 * Adds the specified request to the global queue using the Default TAG.
	 *
	 * @param req
	 * @param
	 */
	public <T> void addToRequestQueue(Request<T> req) {
		// set the default tag if tag is empty
		req.setTag(TAG);

		getRequestQueue().add(req);
	}

	/**
	 * Cancels all pending requests by the specified TAG, it is important to
	 * specify a TAG so that the pending/ongoing requests can be cancelled.
	 *
	 * @param tag
	 */
	public void cancelPendingRequests(Object tag) {
		if (mRequestQueue != null) {
			mRequestQueue.cancelAll(tag);
		}
	}
}