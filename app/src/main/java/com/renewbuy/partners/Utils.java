package com.renewbuy.partners;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.util.Patterns;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.webkit.MimeTypeMap;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.Holder;
import com.orhanobut.dialogplus.ListHolder;
import com.orhanobut.dialogplus.OnItemClickListener;
import com.renewbuy.partners.db.AppDb;
import com.renewbuy.partners.db.CustomerTable;
import com.renewbuy.partners.db.PaymentTable;
import com.renewbuy.partners.db.StatsTable;

import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.InetAddress;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by shubham on 8/7/16.
 */
public class Utils {
    static String DOMAIN = "http://dev.renewbuy.com";
    //static String DOMAIN = "https://www.renewbuy.com";
    //static String DOMAIN = "http://192.168.2.39";

    public final static String initFlag = "INIT_FLAG";

    public static final int SUCCESS_DIALOG = 1;
    public static final int ERROR_DIALOG = 2;
    public static final int INFO_DIALOG = 3;

    public static final String ADD= "add";
    public static final String REPLACE= "replace";

    public final static String notificationTab = "TAB";
    public final static String notificationTitle = "TITLE";
    public final static String isRegisterKey = "IS_REG";


    //TODO:ActivityResultCode
    //for capture Policy Fragment
    public static final int REQUEST_IMAGE_CAPTURE = 0;
    public static final int REQUEST_IMAGE_GALLERY = 1;
    public static final int REQUEST_IMAGE_DOCUMENT = 2;
    //for webViewPolicy
    public static final int MANUFACTURER_REQUEST = 3;
    public static final int MODEL_REQUEST = 4;
    public static final int VARIANT_REQUEST = 5;
    public static final int CITY_REQUEST = 6;

    //TYPE OF BUSSINESS CONSTANT
    public final static String NEW_VEHICLE = "New Vehicle";
    public final static String ROLLOVER = "Rollover";
    public final static String WITHIN_THREE_MONTH = "Policy Expired within last three month";
    public final static String BEFORE_THREE_MONTH = "Expired before last three month";
    public final static String DONT_HAVE_POLICY = "I don't have previous policy";


    //boolean : if partner has some New Notification about refferals
    public static boolean hasRefferalsNotfication ;

    //value:  indexTransactionNotfication is used to show red Dot on child of Refferals Tab
    public static int indexTransactionNotfication = 3;

    public static final int SUBMITTED = 1;
    public static final int ACCEPTED = 2;
    public static final int REJECTED = 3;
    public static final int MISSED = 4;
    public static final int PURCHASED = 5;
    public static final int ACCEPTED_OFFLINE = 6;

    public static final String accountNameKey = "ACCOUNT_NAME";
    public static final String accountNumberKey = "ACCOUNT_NUMBER";
    public static final String ifscCodeKey = "IFSC_CODE";
    public static final String panNoKey = "PAN_NO";

    public final static String executivePartnerCode = "EXECUTIVE_PARTNER_CODE";
    public final static String executiveMobileKey = "EXECUTIVE_MOBILE";
    public final static String emailKey = "EMAIL";
    public final static String executiveNameKey = "EXECUTIVE_NAME";
    public final static String authTokenKey = "AUTHENTICATION_TOKEN";
    public final static String executiveIdKey = "EXECUTIVE_ID";
    public final static String executiveCodeKey= "EXECUTIVE_CODE";
    public final static String userIdKey = "USER_ID";

    public static final String profileKey = "profile_path";
    public final static String seperator = "@*@*@*@";
    public final static String statusArray[] = {"Submitted", "Accepted", "Rejected", "Missed", "Purchased", "Accepted Offline"};

    //Constant For Stats Fragment
    public final static String totalAcceptedPolicyByYear = "TOTAL_ACCEPTED_POLICY_BY_YEAR";
    public final static String totalAcceptedPolicyByMonth = "TOTAL_ACCEPTED_POLICY_BY_MONTH";

    public final static String totalPurchasedPolicyByYear = "TOTAL_PURCHASED_POLICY_BY_YEAR";
    public final static String totalPurchasedPolicyByMonth = "TOTAL_PURCHASED_POLICY_BY_MONTH";

    public final static String totalCurrentYearActualCommissionKey = "TOTAL_CURRENT_YEAR_ACTUAL_COMMISSION";
    public final static String totalCurrentMonthActualCommissionKey = "TOTAL_CURRENT_MONTH_ACTUAL_COMMISSION";

    public final static String totalCurrentYearCarSoldKey = "TOTAL_CURRENT_YEAR_CAR_SOLD";
    public final static String totalCurrentMonthCarSoldKey = "TOTAL_CURRENT_MONTH_CAR_SOLD";

    public final static String totalCurrentYearCarEarningKey = "TOTAL_CURRENT_YEAR_CAR_EARNING";
    public final static String totalCurrentMonthCarEarningKey = "TOTAL_CURRENT_MONTH_CAR_EARNING";

    public final static String totalCurrentYearBikeEarningKey = "TOTAL_CURRENT_YEAR_BIKE_EARNING";
    public final static String totalCurrentMonthBikeEarningKey = "TOTAL_CURRENT_MONTH_BIKE_EARNING";

    public final static String totalCurrentYearBikeSoldKey = "TOTAL_CURRENT_YEAR_BIKE_SOLD";
    public final static String totalCurrentMonthBikeSoldKey = "TOTAL_CURRENT_MONTH_BIKE_SOLD";

    public final static String totalLastYearActualCommissionKey = "TOTAL_LAST_YEAR_ACTUAL_COMMISSION";
    public final static String totalLastMonthActualCommissionKey = "TOTAL_LAST_MONTH_ACTUAL_COMMISSION";

    public final static String totalYearIncomeMissedKey = "TOTAL_YEAR_INCOME_MISSED";
    public final static String totalMonthIncomeMissedKey = "TOTAL_MONTH_INCOME_MISSED";

    public final static String totalYearBalancePotentialKey = "TOTAL_YEAR_BALANCE_POTENTIAL";
    public final static String totalMonthBalancePotentialKey = "TOTAL_MONTH_BALANCE_POTENTIAL";

    public final static String totalYearExpectedCommissionKey = "TOTAL_YEAR_EXPECTED_COMMISSION";
    public final static String totalMonthExpectedCommissionKey = "TOTAL_MONTH_EXPECTED_COMMISSION";

    public final static String totalUploadsKey = "TOTAL_UPLOADS";
    public final static String totalExpectedCommissionKey = "TOTAL_EXPECTED_COMMISSION";
    public final static String totalIncomeMissedKey = "TOTAL_INCOME_MISSED";
    public final static String totalBalancePotentialKey = "TOTAL_BALANCE_POTENTIAL";
    //Constant over

    public final static String partnerCodeKey = "PARTNER_CODE";

    public final static String utmSourceKey = "UTM_SOURCE";
    public final static String utmMediumKey = "UTM_MEDIUM";
    public final static String utmTermKey = "UTM_TERM";
    public final static String utmContentKey = "UTM_CONTENT";
    public final static String utmCampaignKey = "UTM_CAMPAIGN";
    public final static String tutorialShownKey = "TUTORIAL_SHOWN";
    public final static Pattern emailPattern = Patterns.EMAIL_ADDRESS; // API level 8+
    public static final String hasAllAccountDetailsFlag = "hasAllAccountDetailsFlag";

    //Notification Onclick Action Constant
    private static final String OPEN_DIALER    = "OPEN_DIALER";
    private static final String OPEN_SCREEN    = "OPEN_SCREEN";
    private static final String OPEN_URL       = "OPEN_URL";
    private static final String OPEN_PLAYSTORE = "OPEN_PLAYSTORE";


    public static AlertDialog dialog;


    public static ArrayList<String> getDateList() {
        ArrayList<String> list = new ArrayList<>();
        for (int i = 1; i <= 31 ; i++) {
            String number ;
            if( i <= 9 ){
                number = "0"+i;
            } else {
                number = ""+i;
            }
            list.add(number);
        }
        return list;
    }

    public static ArrayList<String> getMonthList() {
        String[] month = MainActivity.activity.getResources().getStringArray(R.array.month_array);
        ArrayList<String> monthlist = new ArrayList<>();
        for (int i = 0; i <= month.length-1 ; i++) {
            monthlist.add(month[i]);
        }
        return monthlist;
    }

    public static ArrayList<String> getYearList() {
        Date date = new Date();
        int year = 1900 + date.getYear() ;
        ArrayList<String> Yearlist = new ArrayList<>();
        for (int i = year+3; i >= year -1 ; i--) {
            Yearlist.add(""+i);
        }
        return Yearlist;
    }
    public static ArrayList<String> getTenYearList() {
        Date date = new Date();
        int year = 1900 + date.getYear() ;
        ArrayList<String> Yearlist = new ArrayList<>();
        for (int i = year; i >= year-15 ; i--) {
            Yearlist.add(""+i);
        }
        return Yearlist;
    }

    public static ArrayList<String> getBusinessTypeList() {
        ArrayList<String> businessList = new ArrayList<>();

        businessList.add(NEW_VEHICLE);
        businessList.add(ROLLOVER);
        businessList.add(WITHIN_THREE_MONTH);
        businessList.add(BEFORE_THREE_MONTH);
        businessList.add(DONT_HAVE_POLICY);
        return businessList;
    }

    public static int getMonthNumber(String month) throws ParseException {
        Date date = new SimpleDateFormat("MMM").parse(month);
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.MONTH)+1;
    }

    public static boolean isValidFormat(String format, String value) {
        Date date = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            date = sdf.parse(value);
            if (!value.equals(sdf.format(date))) {
                date = null;
            }
        } catch (ParseException ex) {
            ex.printStackTrace();
        }
        return date != null;
    }

    public static boolean checkDateIsInRightFormat(String date, String month, String year) {
        Date date1 = null;
        try {
            String checkDate = date+"/"+getMonthNumber(month)+"/"+year;
            SimpleDateFormat sdf = new SimpleDateFormat("dd/M/yyyy");
            date1 = sdf.parse(checkDate);
            if (!checkDate.equals(sdf.format(date1))) {
                date1 = null;
            }
        } catch (ParseException ex) {
            ex.printStackTrace();
        }
        return date1 != null;
    }

    public static void logoutUser(Context context ) {

        // Mark User is not Active in Device Details EndPoint
        HashMap<String,String> map = new HashMap<String,String>();
        map.put("platform","2");
        map.put("app","1");
        map.put("device_id",Utils.getDeviceId(context));
        map.put("is_active","false");
        Api.changeActiveStatus(context,map);

        // Newly added on 06-07-2016
        Utils.removeDataFromPreferences(context, Config.GCM_TOKEN_SENT_TO_SERVER);

        //Remove profile reference
        MainActivity.bitmap = null ;

        Utils.saveDataToPreferences(context, Utils.executiveMobileKey, "");
        Utils.saveDataToPreferences(context, Utils.authTokenKey, "");
        Utils.saveDataToPreferences(context, Utils.executiveIdKey, "");
        Utils.saveDataToPreferences(context, Utils.emailKey, "");
        Utils.saveDataToPreferences(context, Utils.profileKey, "");

        removeAllIncome(context);

        AppDb db = AppDb.getInstance(context);
        PaymentTable paymentTable = (PaymentTable) db.getPaymentTableObject(PaymentTable.TABLE_NAME);
        CustomerTable customerTable = (CustomerTable) db.getTableObject(CustomerTable.TABLE_NAME);
        StatsTable statsTable = (StatsTable) db.getTableObject(StatsTable.TABLE_NAME);
        statsTable.deleteAllData(context);
        paymentTable.deleteAllData(context);
        customerTable.deleteAllData(context);
    }

    //Remove all incomes
    public static void removeAllIncome(Context context){

        // remove bank details
        removeDataFromPreferences(context,accountNameKey);
        removeDataFromPreferences(context,accountNumberKey);
        removeDataFromPreferences(context,ifscCodeKey);
        removeDataFromPreferences(context,panNoKey);
        removeDataFromPreferences(context,hasAllAccountDetailsFlag);
    }



    public static class ResolveHostTask extends AsyncTask<String, Integer, String> {
        @Override
        protected String doInBackground(String... params) {
            InetAddress addr = null;
            try {
                addr = InetAddress.getByName(params[0]);
                return addr.getHostAddress();
            } catch (Exception e) {
                e.printStackTrace();
                return "";
            }
        }
    }

    // Check Internet Connectivity is there or not
    public static boolean isInternetAvailable(Activity activity) {
        ConnectivityManager connectivityManager = (ConnectivityManager)activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            return true;
        }
        else
            return false;
    }

    // Validate Mobile Number.
    public static boolean isValidMobileNo(String phoneNo) {
        boolean check = false;
        if (phoneNo != null && phoneNo.trim().length() == 10) {
            try {
                Long.parseLong(phoneNo);
                check = true;
            } catch (Exception e) {
                e.printStackTrace();
                check = false;
            }
        }
        return check;
    }

    //Authentication Methods
    public static String getAuthToken(Activity activity) {
        return getAuthToken(activity.getBaseContext());
    }

    public static String getAuthToken(Context context) {
        String token = Utils.getStringFromPreferences(context, Utils.authTokenKey);
        if (token == null || token.isEmpty()) {
            return null;
        }
        return token;
    }

    public static String getExecutiveId(Context context) {
        String executive_id_key = Utils.getStringFromPreferences(context, Utils.executiveIdKey);
        if (executive_id_key == null || executive_id_key.isEmpty()) {
            return null;
        }
        return executive_id_key;
    }

    public static boolean isLoggedIn(Context context) {
        String token = getAuthToken(context);
        String executive_id = getExecutiveId(context);
        if (token == null || token.isEmpty() || executive_id == null || executive_id.isEmpty()) {
            return false;
        }

        return true;
    }

    public static void updatePartnerForExecutive(Context context) {
        if (isLoggedIn(context) && !Utils.getStringFromPreferences(context, Utils.partnerCodeKey).isEmpty())
        {
            Api.setPartnerForExecutive(context);
        }
    }

    public static void updateReferralCode(Context context, String src, String medium, String term, String campaign, String content) {
        if (Utils.getBooleanFromPreferences(context, isRegisterKey) && isLoggedIn(context)) {
            Api.setReferralCode(context, src, medium, term, campaign, content);

            Utils.saveDataToPreferences(context, Utils.isRegisterKey, false);
        }
    }

    public static void updateReferralCode(Context context) {
        String src = Utils.getStringFromPreferences(context, Utils.utmSourceKey);
        String medium = Utils.getStringFromPreferences(context, Utils.utmMediumKey);

        boolean g = Utils.getBooleanFromPreferences(context, isRegisterKey) ;
        boolean h = isLoggedIn(context) ;

        if (Utils.getBooleanFromPreferences(context, isRegisterKey) && isLoggedIn(context) && !src.isEmpty() && !medium.isEmpty()) {
            String term = Utils.getStringFromPreferences(context, Utils.utmTermKey);
            String campaign = Utils.getStringFromPreferences(context, Utils.utmCampaignKey);
            String content = Utils.getStringFromPreferences(context, Utils.utmContentKey);

            Api.setReferralCode(context, src, medium, term, campaign, content);

            Utils.saveDataToPreferences(context, Utils.isRegisterKey, false);
        }
    }

    // Validate Email Id
    public static boolean isValidEmailId(String emailId) {
        boolean check;
        Pattern p;
        Matcher m;
        String EMAIL_STRING = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        p = Pattern.compile(EMAIL_STRING);

        m = p.matcher(emailId);
        check = m.matches();
        return check;
    }

    public static String getPrimaryEmail(Context context) {
        Account[] accounts = AccountManager.get(context).getAccountsByType("com.google");

        for (Account account : accounts) {
            if (emailPattern.matcher(account.name).matches()) {
                return account.name;
            }
        }

        // displayAlert(context,"Error", "Unable to get email address from device");
        return "";
    }

    private static HashMap<String, Long> mLastClickTimes = new HashMap<>();

    public static boolean isRepeatedClicks(String tag) {
        if (!mLastClickTimes.containsKey(tag)) {
            mLastClickTimes.put(tag, SystemClock.elapsedRealtime());
            return false;
        }
        ;

        Long mLastClickTime = mLastClickTimes.get(tag);
        // If the click is within 2 seconds
        if (Math.abs(SystemClock.elapsedRealtime() - mLastClickTime) < 2000) {
            return true;
        } else {
            mLastClickTimes.put(tag, SystemClock.elapsedRealtime());
            return false;
        }
    }



    public static void displayAlert(Context context ,String title, String errorMessage) {
        //by shubham 20-apr-2016
        errorMessage = errorMessage.replaceAll("[|?*<\\\":>+\\\\[\\\\]/'\\[\\]]", " ");
        errorMessage = errorMessage.substring(0, 1).toUpperCase() + errorMessage.substring(1);

        ShowSuccessDialog(context,errorMessage,ERROR_DIALOG);
    }

    public static void displayAlert(String title, String errorMessage) {
        Activity activity = MainActivity.activity;
        //by shubham 20-apr-2016
        errorMessage = errorMessage.replaceAll("[|?*<\\\":>+\\\\[\\\\]/'\\[\\]]", " ");
        errorMessage = errorMessage.substring(0, 1).toUpperCase() + errorMessage.substring(1);
        ShowSuccessDialog(activity,errorMessage,ERROR_DIALOG);
        ;
    }


    public static void ChangeFragment(FragmentManager manager , Fragment fragment ,Bundle bundle , String method, int container, String backStack) {

        FragmentTransaction transaction = manager.beginTransaction();
        if(bundle != null) {
            fragment.setArguments(bundle);
        }
        if( method.equals(ADD))
            transaction.add(container,fragment);
        else
            transaction.replace(container,fragment);

        if(backStack != null)
            transaction.addToBackStack(backStack);
        transaction.commit();
    }


    public static Drawable buildCounterDrawable(Activity activity ,int count, int backgroundImageId) {
        LayoutInflater inflater = LayoutInflater.from(activity);
        View view = inflater.inflate(R.layout.counter_menuitem_layout, null);
        view.setBackgroundResource(backgroundImageId);


        TextView textView  = (TextView) view.findViewById(R.id.humburger_counter);

        if (count == 0) {
            textView.setVisibility(View.GONE);
        } else {
            if(count > 9)
                textView.setText("9+");
            else
                textView.setText(""+count);
        }

        view.measure(
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());

        view.setDrawingCacheEnabled(true);
        view.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        Bitmap bitmap = Bitmap.createBitmap(view.getDrawingCache());
        view.setDrawingCacheEnabled(false);

        return new BitmapDrawable(activity.getResources(), bitmap);
    }

    // Save key - value pair to preferences settings.
    public static void saveDataToPreferences(Activity activity, String key, String value) {
        SharedPreferences settings = activity.getSharedPreferences("sharedPreference", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static void saveBankDetailsInSettings(Activity mainActivity, String accName, String accNo, String ifsc, String pan_no) {
        Utils.saveDataToPreferences(mainActivity, accountNameKey, accName);
        Utils.saveDataToPreferences(mainActivity, accountNumberKey, accNo);
        Utils.saveDataToPreferences(mainActivity, ifscCodeKey, ifsc);
        Utils.saveDataToPreferences(mainActivity, panNoKey, pan_no);
    }

    // Save key - value pair to preferences settings.
    public static void saveDataToPreferences(Context context, String key, String value) {
        SharedPreferences settings = context.getSharedPreferences("sharedPreference", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(key, value);
        editor.commit();
    }

    // Save key - value pair to preferences settings.
    public static void saveDataToPreferences(Context context, String key, boolean value) {
        SharedPreferences settings = context.getSharedPreferences("sharedPreference", Context.MODE_PRIVATE);
        settings.edit().putBoolean(key, value).commit();
    }

    // Get saved value from preferences. Default value is "".
    public static boolean getBooleanFromPreferences(Context context, String key) {
        SharedPreferences settings = context.getSharedPreferences("sharedPreference", Context.MODE_PRIVATE);
        return settings.getBoolean(key, false);
    }

    // Get saved value from preferences. Default value is "".
    public static boolean getBooleanFromPreferences(Context context, String key, boolean def) {
        SharedPreferences settings = context.getSharedPreferences("sharedPreference", Context.MODE_PRIVATE);
        return settings.getBoolean(key, def);
    }

    // Save key - value pair to preferences settings.
    public static void saveDataToPreferences(Context context, String key, Integer value) {
        SharedPreferences settings = context.getSharedPreferences("sharedPreference", Context.MODE_PRIVATE);
        settings.edit().putInt(key, value).commit();
    }

    // Get saved value from preferences.
    public static int getDatafromPreferences(Context context, String key) {
        SharedPreferences settings = context.getSharedPreferences("sharedPreference", Context.MODE_PRIVATE);
        return settings.getInt(key, 0);
    }

    // Get saved value from preferences. Default value is "".
    public static String getStringFromPreferences(Activity activity, String key) {
        SharedPreferences settings = activity.getSharedPreferences("sharedPreference", Context.MODE_PRIVATE);
        return settings.getString(key, "");
    }

    // Get saved value from preferences. Default value is "".
    public static String getStringFromPreferences(Context context, String key) {
        SharedPreferences settings = context.getSharedPreferences("sharedPreference", Context.MODE_PRIVATE);
        return settings.getString(key, "");
    }

    // Get saved value from preferences. Default value is "".
    public static void removeDataFromPreferences(Activity activity, String key) {
        SharedPreferences settings = activity.getSharedPreferences("sharedPreference", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.remove(key);
        editor.commit();
    }

    public static void removeDataFromPreferences(Context context, String key) {
        SharedPreferences settings = context.getSharedPreferences("sharedPreference", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.remove(key);
        editor.commit();
    }

    //Date Operations Methods
    public static Integer convertDateToEpoch(Date date) {
        Long epochSecs = date.getTime() / 1000;
        return epochSecs.intValue();
    }

    public static Date convertEpochToDate(Integer epoch) {
        long epochMilli = (long) epoch * 1000;
        return new Date(epochMilli);
    }

    public static void ShowDialog(Context context , String title , String[] textArray , int[] drawableArray){
        WeakReference<Activity> reference = new WeakReference<Activity>((Activity) context);

        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        LinearLayout  vertical  = new LinearLayout(context);
        vertical.setOrientation(LinearLayout.VERTICAL);
        LinearLayout.LayoutParams verticalParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        vertical.setLayoutParams(verticalParams);
        vertical.setPadding(10,10,10,0);
        makeHorizontalLayout(vertical,context,textArray,drawableArray);

        builder.setNegativeButton("CLOSE",null);
        builder.setCancelable(true);
        builder.setView(vertical);
        builder.setTitle(title);

        AlertDialog dialog = builder.create();
        if(reference.get() != null && !reference.get().isFinishing()) {
            dialog.show();
        }

        Button button = dialog.getButton(DialogInterface.BUTTON_NEGATIVE);
        button.setTextColor(context.getResources().getColor(R.color.colorAccent));
    }

    public static void ShowSuccessDialog(Context context, String Message, int dialogType){
        int layoutId  ;

        WeakReference<Activity> reference = null;
        if(context instanceof  Activity){
            reference = new WeakReference<Activity>((Activity) context);
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View view = LayoutInflater.from(context).inflate(R.layout.success_dialog_layout,null);
        TextView message = (TextView) view.findViewById(R.id.dialog_message);
        TextView title = (TextView) view.findViewById(R.id.dialog_title);
        ImageView icon = (ImageView) view.findViewById(R.id.dialog_icon);
        Button button = (Button) view.findViewById(R.id.dialog_button);

        if(dialogType == SUCCESS_DIALOG) {
            layoutId = R.drawable.ic_check_circle;
            title.setText(context.getResources().getString(R.string.success));
        }
        else {
            layoutId = R.drawable.ic_cancel_black_24dp;
            title.setText(context.getResources().getString(R.string.error));
        }
        Log.i("ShowSuccessDialog: ",Message);
        Message = Message.replaceAll("[|?*<\\\":>+\\\\[\\\\]/'\\[\\]]", " ");
        Message = Message.substring(0, 1).toUpperCase() + Message.substring(1);

        icon.setImageDrawable(context.getResources().getDrawable(layoutId));
        message.setText(Message);

        builder.setView(view);
        builder.setCancelable(true);

        final AlertDialog dialog = builder.create();
        if(reference != null && reference.get() != null && !reference.get().isFinishing()) {
            dialog.show();
        }

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }
    public static void ShowSuccessDialog(Context context, String Message, int dialogType, View.OnClickListener listener){
        int layoutId = 0;

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View view = LayoutInflater.from(context).inflate(R.layout.success_dialog_layout,null);
        TextView message = (TextView) view.findViewById(R.id.dialog_message);
        TextView title = (TextView) view.findViewById(R.id.dialog_title);
        ImageView icon = (ImageView) view.findViewById(R.id.dialog_icon);
        Button button = (Button) view.findViewById(R.id.dialog_button);

        switch (dialogType){
            case SUCCESS_DIALOG:
                layoutId = R.drawable.ic_check_circle;
                title.setText(context.getResources().getString(R.string.success));
                break;

            case ERROR_DIALOG:
                layoutId = R.drawable.ic_cancel_black_24dp;
                title.setText(context.getResources().getString(R.string.error));
                break;

            case INFO_DIALOG:
                layoutId = R.drawable.ic_report_problem_black_24dp;
                title.setText(context.getResources().getString(R.string.info));
                break;
        }

        Message = Message.replaceAll("[|?*<\\\":>+\\\\[\\\\]/'\\[\\]]", " ");
        Message = Message.substring(0, 1).toUpperCase() + Message.substring(1);

        icon.setImageDrawable(context.getResources().getDrawable(layoutId));
        message.setText(Message);

        builder.setView(view);
        builder.setCancelable(true);

        dialog = builder.create();
        dialog.show();

        if(listener != null)
            button.setOnClickListener(listener);
        else
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
    }

    private static void makeHorizontalLayout(LinearLayout vertical, Context context, String[] textArray, int[] drawableArray) {
        int size = textArray.length;

        for (int i = 0; i <=size-1 ; i++) {
            View view = LayoutInflater.from(context).inflate(R.layout.dialog_layout,null);
            ImageView imageView = (ImageView) view.findViewById(R.id.dialog_icon);
            TextView  txtView = (TextView) view.findViewById(R.id.dialog_text);

            imageView.setImageDrawable(context.getResources().getDrawable(drawableArray[i]));
            txtView.setText(textArray[i]);

            vertical.addView(view);
        }
    }

    public static void showFilterDialog(Context context, List<String> datalist, int selected, DialogInterface.OnClickListener listener) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        final List<String> list = datalist ;
        final ArrayAdapter<String> adapter = new ArrayAdapter<>(context, android.R.layout.simple_list_item_single_choice, list);

        builder.setSingleChoiceItems(adapter,selected, listener);

        AlertDialog alert = builder.create();
        alert.show();
        //alert.getWindow().getAttributes().windowAnimations = R.style.dialog_animation;
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;

        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }

    public static void setPic(ImageView mImageView, String mCurrentPhotoPath) {
        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);

        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Get the dimensions of the View
        int targetW = mImageView.getWidth();
        int targetH = mImageView.getHeight();

        // Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath);
        // Determine how much to scale down the image
        int scaleFactor = 0;
        if (targetW != 0 && targetH != 0)
            scaleFactor = Math.min(photoW / targetW, photoH / targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;

        Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        mImageView.setImageBitmap(bitmap);
    }

    public static File createImageFile(Activity activity) throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = activity.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName, /* prefix */
                ".jpg", /* suffix */
                storageDir /* directory */
        );
        return image;
    }

    private static Uri getUri() {
        String state = Environment.getExternalStorageState();
        if (!state.equalsIgnoreCase(Environment.MEDIA_MOUNTED))
            return MediaStore.Images.Media.INTERNAL_CONTENT_URI;

        return MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context       The context.
     * @param uri           The Uri to query.
     * @param selection     (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }


    /**
     * Get a file path from a Uri. This will get the the path for Storage Access
     * Framework Documents, as well as the _data field for the MediaStore and
     * other file-based ContentProviders.
     *
     * @param context The context.
     * @param uri     The Uri to query.
     * @author paulburke
     */
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        if (isImageFile(uri) || isDocumentFile(uri)) {

            // MediaStore (and general)
            if ("content".equalsIgnoreCase(uri.getScheme())) {
                String path = getDataColumn(context, uri, null, null);
                if (path != null) {
                    return path;
                }
            }
            // File
            else if ("file".equalsIgnoreCase(uri.getScheme()) || "assets".equalsIgnoreCase(uri.getScheme())) {
                String path = uri.getPath();
                if (path != null) {
                    return path;
                }
            }

            // DocumentProvider
            if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {

                // ExternalStorageProvider
                if (isExternalStorageDocument(uri)) {
                    final String docId = DocumentsContract.getDocumentId(uri);
                    final String[] split = docId.split(":");
                    final String type = split[0];

                    if ("primary".equalsIgnoreCase(type)) {
                        return Environment.getExternalStorageDirectory() + "/" + split[1];
                    }

                    // TODO handle non-primary volumes
                }

                // DownloadsProvider
                else if (isDownloadsDocument(uri)) {

                    final String id = DocumentsContract.getDocumentId(uri);
                    final Uri contentUri = ContentUris.withAppendedId(
                            Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                    return getDataColumn(context, contentUri, null, null);
                }

                // MediaProvider
                else if (isMediaDocument(uri)) {
                    final String docId = DocumentsContract.getDocumentId(uri);
                    final String[] split = docId.split(":");
                    final String type = split[0];

                    Uri contentUri = null;
                    if ("image".equals(type)) {
                        contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                    } else if ("video".equals(type)) {
                        contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                    } else if ("audio".equals(type)) {
                        contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                    }

                    final String selection = "_id=?";
                    final String[] selectionArgs = new String[]{
                            split[1]
                    };

                    return getDataColumn(context, contentUri, selection, selectionArgs);
                }

            }
        }

        return null;
    }

    // TODO: Evaluate whether this method can be replaced by getImagePathFromUri
    public static String getRealPathFromURI(Activity activity, Uri imageUri) {
        Cursor cursor = activity.getContentResolver().query(imageUri, null, null, null, null);
        if (cursor == null) {
            return imageUri.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }
    }

    public static String getImagePathFromUri(Activity activity, Uri imageUri) {
        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        //Cursor cursor = activity.getContentResolver().query(imageUri, filePathColumn, null, null, null);
        Cursor imageCursor = null;
        String str[] = imageUri.getLastPathSegment().split(":");
        if (str.length >= 2) {
            imageCursor = activity.getContentResolver()
                    .query(getUri(), filePathColumn,MediaStore.Images.Media._ID + "=" + str[str.length - 1], null, null);
        } else {
            imageCursor = activity.getContentResolver().query(imageUri, filePathColumn, null, null, null);
        }
        //cursor.moveToFirst();
        String picturePath = null;

        if(imageCursor != null ) {
            if (imageCursor.moveToFirst()) {
                int columnIndex = imageCursor.getColumnIndex(filePathColumn[0]);
                picturePath = imageCursor.getString(columnIndex);
            }
            imageCursor.close();
        }
        return picturePath;
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }//
    public static boolean isImageFile(Uri selectedImageUri) {
        String type = getMimeType(selectedImageUri);

        if (type != null && !type.isEmpty())
            return type.split("/")[0].equalsIgnoreCase("image");

        return false;
    }

    public static boolean isDocumentFile(Uri selectedImageUri) {
        String type = getMimeType(selectedImageUri);

        String[] ACCEPT_MIME_TYPES = {
                "application/msword",
                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                "application/vnd.openxmlformats-officedocument.spreadsheetml.template",
                "application/vnd.openxmlformats-officedocument.presentationml.template",
                "application/vnd.openxmlformats-officedocument.presentationml.slideshow",
                "application/vnd.openxmlformats-officedocument.presentationml.presentation",
                "application/vnd.openxmlformats-officedocument.presentationml.slide",
                "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                "application/vnd.openxmlformats-officedocument.wordprocessingml.template",
                "application/vnd.ms-excel.addin.macroEnabled.12",
                "application/vnd.ms-excel.sheet.binary.macroEnabled.12",
                "application/pdf"
        };

        if (type != null && !type.isEmpty())
            return Arrays.asList(ACCEPT_MIME_TYPES).contains(type);
        //return type.split("/")[0].equalsIgnoreCase("application");

        return false;
    }

    public static String getMimeType(Uri selectedImageUri) {
        String type ;
        String extension = null;

        if (selectedImageUri != null)
            extension = MimeTypeMap.getFileExtensionFromUrl(selectedImageUri.toString());

        if (extension != null && !extension.equals(""))
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        else {
            ContentResolver resolver = MainActivity.activity.getContentResolver();
            type = resolver.getType(selectedImageUri);
        }

        return type;
    }



    public static int getVersionCode(Context context){
        PackageInfo pInfo = null;
        try {
            pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return pInfo.versionCode;
    }

    public static String getDeviceId(Context context){
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public static  Date getAfterDaysDate(int days){
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE,days);

        return c.getTime();
    }

    public static void selectDropDown(Context ctx, String title, final TextView txt, final ArrayList<String> list) {

        final ArrayAdapter arrayAdapter = new ArrayAdapter(ctx,android.R.layout.simple_list_item_1,list);

        ShowDialog(ctx,title,new ListHolder(), Gravity.BOTTOM, arrayAdapter, new OnItemClickListener() {
            @Override
            public void onItemClick(DialogPlus dialog, Object item, View view, int position) {
                if(item != null || position != -1) {
                    String strName  = list.get(position);
                    if (txt != null) {
                        txt.setText(strName);
                        dialog.dismiss();
                    }
                }
            }
        }, false);
    }

    private static void ShowDialog(Context context ,String titletext , Holder holder, int gravity, BaseAdapter adapter, OnItemClickListener itemClickListener, boolean expanded) {
        View header = LayoutInflater.from(context).inflate(R.layout.dialog_header,null);
        TextView title = (TextView) header.findViewById(R.id.bottom_dialog_title);
        title.setText(titletext) ;
        final DialogPlus dialog = DialogPlus.newDialog(context)
                .setContentHolder(holder)
                .setHeader(header)
                .setCancelable(true)
                .setGravity(gravity)
                .setAdapter(adapter)
                .setOnItemClickListener(itemClickListener)
                .setExpanded(expanded)
                .create();
        dialog.show();
    }

    public static void hideKeyboard(Context ctx) {
        InputMethodManager inputManager = (InputMethodManager) ctx
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        // check if no view has focus:
        View v = ((Activity) ctx).getCurrentFocus();
        if (v == null)
            return;

        inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    public static Bitmap getRoundedCornerBitmap(Bitmap bitmap, int pixels) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);
        final float roundPx = pixels;

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }

    public static Bitmap getCircularBitmapFromFile(ImageView mImageView, String filepath){

        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filepath, bmOptions);

        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Get the dimensions of the View
        int targetW = 175;
        int targetH = 175;

        // Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath);
        // Determine how much to scale down the image

        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);


        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;

        Bitmap bitmap = BitmapFactory.decodeFile(filepath, bmOptions);


        if(bitmap !=null ) {
            bitmap = Bitmap.createScaledBitmap(bitmap, 175, 175, true);
            return getRoundedCornerBitmap(bitmap, bitmap.getWidth());
        }
        return null ;
    }

    public static Intent getClickAction(String onClickAction) {
        String purpose ,action ;
        Intent intent = null;
        Uri uri ;
        String[] temp = onClickAction.split(":");
        purpose = temp[0];
        action  = temp[1];

        switch (purpose) {

            case OPEN_DIALER:
                uri = Uri.parse("tel:" + action);
                intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(uri);
                break;

            case OPEN_URL:
                String url = onClickAction.substring(onClickAction.indexOf(":")+1,onClickAction.length());
                uri = Uri.parse(url);
                intent = new Intent(Intent.ACTION_VIEW, uri);
                break;

            case OPEN_PLAYSTORE:
                uri = Uri.parse("https://play.google.com/store/apps/details?id=" + action);
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(uri);
                break;
        }
        return intent ;
    }

}
