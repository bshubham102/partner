package com.renewbuy.partners;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by sandeep on 9/5/15.
 */
public class AuthorizedStringRequest extends StringRequest {

    private String ACCESS_TOKEN = "";

    public AuthorizedStringRequest(int method, String url, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(method, url, listener, errorListener);
    }

    public AuthorizedStringRequest(int method, String url, String token, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(method, url, listener, errorListener);
        this.ACCESS_TOKEN = token;
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> headers = new HashMap<String, String>();
        String auth = "Token " + this.ACCESS_TOKEN;
        headers.put("Authorization", auth);
        return headers;
    }
}
