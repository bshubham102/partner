package com.renewbuy.partners;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.renewbuy.mercury.Utility;
import com.renewbuy.partners.Adapter.DialogAdapter;
import com.renewbuy.partners.modelClass.DialogModel;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;


/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileDetail extends Fragment {

    static final int REQUEST_CAMERA = 0;
    static final int REQUEST_GALLERY = 1;

    private View view ;
    private Context context ;
    private ImageView mProfleView ;
    private TextView mUsername ;
    private FloatingActionButton mUploadBtn ;
    private ProgressDialog dialog ;

    private boolean isFirst = true ;
    private Uri mCurrentPhotoUri, selectedImageUri;
    private String selectedImagePath, mCurrentPhotoPath ;

    public static ProfileDetail instance ;

    public static ProfileDetail newInstance() {
        return new ProfileDetail();
    }

    public ProfileDetail() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {

        //Get the  refernce of parent activity
        context = getActivity() ;

        instance = this ;

        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_profile_detail, container, false);

        //Intialize the views
        init(view);

        //SetListner
        setListener() ;

        //set Data
        setData();

        return view ;
    }

    private void setListener() {
        mUploadBtn.setOnClickListener(new MyClickListener());
    }

    private void init(View view) {

        mProfleView = (ImageView) view.findViewById(R.id.details_profile_img);
        mUploadBtn = (FloatingActionButton) view.findViewById(R.id.upload_btn);
        mUsername = (TextView) view.findViewById(R.id.txt_username_details);

        dialog = new ProgressDialog(context);
        dialog.setMessage("Updating Image....");
        dialog.setCancelable(false);
    }

    private void setData() {
        mUsername.setText(Utils.getStringFromPreferences(context,Utils.executiveNameKey));

        setProfileImage();
    }

    @SuppressLint("NewApi")
    @Override
    public void onResume() {
        super.onResume();

        if(isFirst) {
            isFirst = false ;

            PropertyValuesHolder scaleX = PropertyValuesHolder.ofFloat(View.SCALE_X , 1f);
            PropertyValuesHolder scaleY = PropertyValuesHolder.ofFloat(View.SCALE_Y , 1f);

            ObjectAnimator scale = ObjectAnimator.ofPropertyValuesHolder(mUploadBtn,scaleX,scaleY);
            scale.setDuration(400);
            scale.start();
        }
    }

    @SuppressLint("NewApi")
    @Override
    public void onDestroyView() {
        super.onDestroyView();

        PropertyValuesHolder scaleX = PropertyValuesHolder.ofFloat(View.SCALE_X , 0f);
        PropertyValuesHolder scaleY = PropertyValuesHolder.ofFloat(View.SCALE_Y , 0f);

        ObjectAnimator scale = ObjectAnimator.ofPropertyValuesHolder(mUploadBtn,scaleX,scaleY);
        scale.setDuration(400);
        scale.start();
    }

    private class MyClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.upload_btn:
                    intentForCapturingImage();
                break;
            }
        }
    }

    public void intentForCapturingImage() {

        Activity activity = MultiUseActivity.instance;
        final AlertDialog.Builder dlgAlert = new AlertDialog.Builder(activity);

        WeakReference<MultiUseActivity> reference = new WeakReference<MultiUseActivity>(MultiUseActivity.instance);

        ListView lv = new ListView(activity);
        lv.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        ArrayList<DialogModel> models = new ArrayList<>();
        models.add(new DialogModel("Camera",R.drawable.ic_click_overlay_camera));
        models.add(new DialogModel("Gallery",R.drawable.ic_click_overlay_gallery));

        DialogAdapter adapter = new DialogAdapter(activity, R.layout.dialog_row_layout, models);
        lv.setAdapter(adapter);

        dlgAlert.setView(lv);
        dlgAlert.setCancelable(true);
        final AlertDialog dialog = dlgAlert.create();
        if(reference.get() != null && !reference.get().isFinishing()) {
            dialog.show();
        }

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                dialog.dismiss();

                switch (position) {
                    case 0:
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (context.checkSelfPermission(android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ) {
                                requestPermissions(new String[]{android.Manifest.permission.CAMERA},REQUEST_CAMERA);
                            } else {
                                startCameraOperation();
                            }
                        } else {
                            startCameraOperation();
                        }
                        break;

                    case 1:
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (context.checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ) {
                                requestPermissions(new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE},REQUEST_GALLERY);
                            } else {
                                startGalleryOperation();
                            }
                        } else {
                            startGalleryOperation();
                        }
                        break;
                }

            }
        });
    }

    private void startCameraOperation(){
        try {
            File photoFile = Utils.createImageFile(getActivity());
            // Save a file: path for use with ACTION_VIEW intents
            mCurrentPhotoPath = photoFile.getAbsolutePath();
            mCurrentPhotoUri = Uri.fromFile(photoFile);

            Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            captureIntent.putExtra(MediaStore.EXTRA_OUTPUT, mCurrentPhotoUri);
            getActivity().startActivityForResult(captureIntent, Utils.REQUEST_IMAGE_CAPTURE);
        }catch (IOException ex) {
            // Error occurred while creating the File
            ex.printStackTrace();
            Log.e(Config.TAG, "Exception: " + ex.getMessage());
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void startGalleryOperation(){
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        photoPickerIntent.putExtra(Intent.EXTRA_LOCAL_ONLY,true);
        getActivity().startActivityForResult(photoPickerIntent, Utils.REQUEST_IMAGE_GALLERY);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case Utils.REQUEST_IMAGE_CAPTURE:
                    selectedImageUri = mCurrentPhotoUri;
                    selectedImagePath = Utils.getRealPathFromURI(getActivity(), selectedImageUri);
                    break;

                case Utils.REQUEST_IMAGE_GALLERY:
                    selectedImageUri = data == null ? null : data.getData();
                    selectedImagePath = Utils.getImagePathFromUri(getActivity(), selectedImageUri);
                    break;
            }

            try {
                if(selectedImagePath != null)
                    multipartRequest(selectedImagePath);
                else
                    Utils.ShowSuccessDialog(context,"Unable to get image.",Utils.ERROR_DIALOG);
            } catch (ParseException | IOException e) {
                e.printStackTrace();
            }

        }
    }

    public void multipartRequest(final String filepath) throws ParseException, IOException {
        dialog.show();
        String urlUpload = Config.DOMAIN+"/api/v2/user/"+Utils.getStringFromPreferences(context,Utils.userIdKey)+"/profile/?embed=userprofile";

        Map<String, File> mFilePartData = new HashMap<>();
        Map<String, String> mStringPart   = new HashMap<>();
        Map<String, String> headers       = new HashMap<>();

        String boundary = "*****" + Long.toString(System.currentTimeMillis()) + "*****";
        String auth = "Token " + Utils.getStringFromPreferences(context,Utils.authTokenKey);

        // Header Details
        headers.put("Content-Type", "multipart/form-data; boundary=" + boundary);
        headers.put("Authorization", auth);
        headers.put("API-SECRET-KEY",SplashScreen.API_KEY);
        headers.put("App-ID",SplashScreen.APP_ID);

        mFilePartData.put("image",new File(filepath));

        MultipartUpload upload = new MultipartUpload(Request.Method.PATCH, context, urlUpload, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.i("onResponse: ",response.toString());
                Utils.saveDataToPreferences(context,Utils.profileKey,filepath);
                dialog.dismiss();
                Utils.ShowSuccessDialog(context,"Image Uploaded Successfully.",Utils.SUCCESS_DIALOG);

                if(MainActivity.activity != null){
                    MainActivity.activity.setProfileImage();
                }

                if(Profile.instance != null){
                    Profile.instance.setProfileImage();
                }
                setProfileImage();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                Utils.ShowSuccessDialog(context,"Error in uploading images. Please try again later.",Utils.ERROR_DIALOG);
            }
        },mFilePartData,mStringPart,headers);

        upload.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.MINUTES.toMillis(Utility.REQUEST_TIMEOUT), DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ApplicationController.getInstance().addToRequestQueue(upload);
    }


    public void setProfileImage(){
        /*String profilePath = Utils.getStringFromPreferences(context,Utils.profileKey);

        File file = new File(profilePath);
        if (!profilePath.equals("") && ! profilePath.startsWith("http") && file.exists()) {
            Bitmap bitmap = Utils.getCircularBitmapFromFile(mProfleView,profilePath);
                assert bitmap != null ;
                mProfleView.setImageBitmap(bitmap);
            }*/

        if(MainActivity.bitmap != null){
            mProfleView.setImageBitmap(MainActivity.bitmap);
        }

    }
}
