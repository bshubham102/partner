package com.renewbuy.partners.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.renewbuy.partners.Utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Vector;

/**
 * Created by devlp on 12/10/15.
 */
public class PaymentTable {
    public static final String TABLE_NAME = "m_payment";
    public static final String COL_PAYMENT_ID = "payment_id";
    public static final String COL_CUSTOMER_NAME = "customer_name";
    public static final String COL_CUSTOMER_EMAIL = "customer_email";
    public static final String COL_CUSTOMER_MOBILE = "customer_mobile";
    public static final String COL_CUSTOMER_PAYMENT_STATUS = "customer_payment_status";
    public static final String COL_CUSTOMER_PAYMENT_DATE = "customer_payment_date";
    public static final String COL_CUSTOMER_ACTUAL_COMMISSION = "customer_actual_commission";
    public static final String COL_CUSTOMER_VEHICAL_NAME = "customer_vehical_name";
    public static final String COL_VEHICLE_ID = "vehicle_id";
    public static final String COL_POLICY_NO="policy_no";
    public static final String COL_INSURER="insurer";
    public static final String COL_ODPREMIUM="obpremium";
    public  static final String COL_TOTAL_PREMIUM = "total_premium";

    public static final String COL_DOC_URL="doc_url";

    Object lock = new Object();

    private AppDb appDb;

    public PaymentTable(AppDb appDb) {
        this.appDb = appDb;
    }

    public void createTable(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + " (" + COL_PAYMENT_ID + " INTEGER,"+ COL_CUSTOMER_NAME + " text," + COL_CUSTOMER_EMAIL + " text," + COL_CUSTOMER_MOBILE + " text," + COL_CUSTOMER_VEHICAL_NAME + " text," + COL_CUSTOMER_PAYMENT_STATUS + " INTEGER," + COL_CUSTOMER_PAYMENT_DATE + " INTEGER," + COL_CUSTOMER_ACTUAL_COMMISSION + " DOUBLE," + COL_ODPREMIUM + " DOUBLE," + COL_TOTAL_PREMIUM + " DOUBLE," + COL_VEHICLE_ID +" INTEGER," + COL_POLICY_NO + " text," + COL_INSURER + " text," + COL_DOC_URL + " text)";
        db.execSQL(CREATE_TABLE);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
    }

   public String insertData(Vector<PaymentBean> beanList, Context ctx)
    {
        SQLiteDatabase db = null;
        PaymentBean bean = null;
        long returnValue = 0;
        long dateTime_Millis = 0;
        synchronized (lock) {

            try {
                db = appDb.getWritableDatabase(TABLE_NAME);

                // ---------------- ADD Category
                for (int i = 0; i < beanList.size(); i++) {

                    ContentValues values = new ContentValues();
                    values.clear();
                    bean = beanList.get(i);
                    if (bean.getCustomer_name() != null && !bean.getCustomer_name().equalsIgnoreCase("null") && bean.getCustomer_name().trim().length() > 0) {
                        values.put(COL_CUSTOMER_NAME, bean.getCustomer_name());
                    }
                    if (bean.getCustomer_email() != null && !bean.getCustomer_email().equalsIgnoreCase("null") && bean.getCustomer_email().trim().length() > 0) {
                        values.put(COL_CUSTOMER_EMAIL, bean.getCustomer_email());
                    }
                    if (bean.getCustomer_mobile() != null && !bean.getCustomer_mobile().equalsIgnoreCase("null") && bean.getCustomer_mobile().trim().length() > 0) {
                        values.put(COL_CUSTOMER_MOBILE, bean.getCustomer_mobile());
                    }
                    if (bean.getCustomer_vehical_name() != null && !bean.getCustomer_vehical_name().equalsIgnoreCase("null") && bean.getCustomer_vehical_name().trim().length() > 0) {
                        values.put(COL_CUSTOMER_VEHICAL_NAME, bean.getCustomer_vehical_name());
                    }
                    if (bean.getDocUrl() != null && !bean.getDocUrl().equalsIgnoreCase("null") && bean.getDocUrl().trim().length() > 0) {
                        values.put(COL_DOC_URL, bean.getDocUrl());
                    }


                    values.put(COL_PAYMENT_ID,bean.getPayment_id());
                    values.put(COL_CUSTOMER_PAYMENT_STATUS, bean.getCustomer_document_status());
                    values.put(COL_CUSTOMER_PAYMENT_DATE, Utils.convertDateToEpoch(bean.getCustomer_prev_policy_expiry()));
                    values.put(COL_CUSTOMER_ACTUAL_COMMISSION, bean.getCustomer_earned());
                    values.put(COL_ODPREMIUM, bean.getOdpremium());
                    values.put(COL_TOTAL_PREMIUM, bean.getCustomer_total_premium());

                    if (bean.getVehicle_id() != null) {
                        values.put(COL_VEHICLE_ID, bean.getVehicle_id());
                    }

                    if (bean.getVehicle_id() != null) {
                        values.put(COL_POLICY_NO, bean.getPolicyNo());
                    }

                    values.put(COL_INSURER, bean.getInsurer());

                    returnValue = db.insert(TABLE_NAME, null, values);
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                appDb.closeDB();
            }
        }
        if (returnValue > 0l) {
            return String.valueOf(dateTime_Millis);
        } else {
            return "";
        }
    }

    public Vector<PaymentBean> getAllData(Context ctx) {
        Vector<PaymentBean> list = new Vector<PaymentBean>();
        synchronized (lock) {
            SQLiteDatabase db = null;
            Cursor cursor = null;
            try {
                db = appDb.getWritableDatabase(TABLE_NAME);
                String[] columns = new String[] { COL_PAYMENT_ID, COL_CUSTOMER_NAME, COL_CUSTOMER_EMAIL, COL_CUSTOMER_MOBILE, COL_CUSTOMER_VEHICAL_NAME, COL_CUSTOMER_PAYMENT_DATE, COL_ODPREMIUM,COL_CUSTOMER_PAYMENT_STATUS, COL_TOTAL_PREMIUM, COL_CUSTOMER_ACTUAL_COMMISSION, COL_VEHICLE_ID, COL_POLICY_NO, COL_INSURER,COL_DOC_URL };
                cursor = db.query(false, TABLE_NAME, columns, null, null, null, null, COL_CUSTOMER_PAYMENT_DATE + " DESC", null);
                if (cursor.moveToFirst()) {
                    do {
                        PaymentBean bean = new PaymentBean();
                        bean = getData(cursor);
                        list.add(bean);
                    } while (cursor.moveToNext());
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (cursor != null)
                    cursor.close();
                appDb.closeDB();
            }
        }
        return list;
    }

    private PaymentBean getData(Cursor cursor) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-mm-yyyy", Locale.US);
        Date custPrevPolicyExpiry = null;
        String custName = cursor.getString(cursor.getColumnIndex(COL_CUSTOMER_NAME));
        String custEmail = cursor.getString(cursor.getColumnIndex(COL_CUSTOMER_EMAIL));
        String custMobile = cursor.getString(cursor.getColumnIndex(COL_CUSTOMER_MOBILE));
        String custVehicleName = cursor.getString(cursor.getColumnIndex(COL_CUSTOMER_VEHICAL_NAME));
        int payment_id = cursor.getInt(cursor.getColumnIndex(COL_PAYMENT_ID));
        int custDocStatus = cursor.getInt(cursor.getColumnIndex(COL_CUSTOMER_PAYMENT_STATUS));
        try {
            Integer epoch = cursor.getInt(cursor.getColumnIndex(COL_CUSTOMER_PAYMENT_DATE));
            if (epoch != null && epoch != 0) {
                custPrevPolicyExpiry = Utils.convertEpochToDate(epoch);
            }
        } catch (Exception e) {
            e.printStackTrace();
            // custPrevPolicyExpiry = new
            // SimpleDateFormat("dd-mm-yyyy").format(new Date());
        }
        double custActualComm = cursor.getDouble(cursor.getColumnIndex(COL_CUSTOMER_ACTUAL_COMMISSION));
        double custOdPremium = cursor.getDouble(cursor.getColumnIndex(COL_ODPREMIUM));
        double custTotalPremium = cursor.getDouble(cursor.getColumnIndex(COL_TOTAL_PREMIUM));
        Integer vehicleId = cursor.getInt(cursor.getColumnIndex(COL_VEHICLE_ID));
        String policyNo = cursor.getString(cursor.getColumnIndex(COL_POLICY_NO));
        String insurer = cursor.getString(cursor.getColumnIndex(COL_INSURER));
        String docurl = cursor.getString(cursor.getColumnIndex(COL_DOC_URL));

        PaymentBean bean = new PaymentBean();
        bean.setPayment_id(payment_id);
        bean.setCustomer_name(custName);
        bean.setCustomer_email(custEmail);
        bean.setCustomer_mobile(custMobile);
        bean.setCustomer_vehical_name(custVehicleName);
        bean.setCustomer_document_status(custDocStatus);
        bean.setCustomer_prev_policy_expiry(custPrevPolicyExpiry);
        bean.setCustomer_earned(custActualComm);
        bean.setVehicle_id(vehicleId);
        bean.setPolicyNo(policyNo);
        bean.setInsurer(insurer);
        bean.setOdpremium(custOdPremium);
        bean.setCustomer_total_premium(custTotalPremium);
        bean.setDocUrl(docurl);

        return bean;
    }

    public int deleteAllData(Context ctx) {
        SQLiteDatabase db = null;
        int rowsAffected = -1;
        synchronized (lock) {
            try {
                db = appDb.getWritableDatabase(TABLE_NAME);
                String selection = "1";
                String[] selectionArgs = new String[] {};
                rowsAffected = db.delete(TABLE_NAME, selection, selectionArgs);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                appDb.closeDB();
            }
        }
        return rowsAffected;
    }
}
