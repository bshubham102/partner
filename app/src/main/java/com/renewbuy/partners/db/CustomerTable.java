package com.renewbuy.partners.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.renewbuy.partners.Utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Vector;

public class CustomerTable {
	public static final String TABLE_NAME = "m_customer";
	public static final String COL_LEAD_ID = "lead_id";
	public static final String COL_CUSTOMER_NAME = "customer_name";
	public static final String COL_CUSTOMER_EMAIL = "customer_email";
	public static final String COL_CUSTOMER_MOBILE = "customer_mobile";
	public static final String COL_CUSTOMER_VEHICAL_NAME = "customer_vehical_name";
	public static final String COL_CUSTOMER_VEHICAL_CLASS = "customer_vehical_class";
	public static final String COL_CUSTOMER_DOCUMENT_STATUS = "customer_document_status";
	public static final String COL_CUSTOMER_PREV_POLICY_EXPIRY = "customer_prev_policy_expiry";
	public static final String COL_CUSTOMER_EXPECTED_COMMISSION = "customer_expected_commission";
	public static final String COL_CUSTOMER_ACTUAL_COMMISSION = "customer_actual_commission";
	public static final String COL_VEHICLE_ID = "vehicle_id";
	public static final String COL_REJ_REASON="rejection_reason";
	public static final String COL_QUOTES_URL="quotes_url";

	Object lock = new Object();

	public static final String TOKEN_ID = "token_id";

	private AppDb appDb;

	public CustomerTable(AppDb appDb) {
		this.appDb = appDb;
	}

	public void createTable(SQLiteDatabase db) {
		String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + " ("
				                              + COL_LEAD_ID + " INTEGER,"
				                              + COL_CUSTOMER_NAME + " text,"
											  + COL_CUSTOMER_EMAIL + " text,"
				                              + COL_CUSTOMER_MOBILE + " text,"
				                              + COL_QUOTES_URL + " text,"
				                              + COL_CUSTOMER_VEHICAL_NAME + " text,"
				                              + COL_CUSTOMER_VEHICAL_CLASS + " INTEGER,"
				                              + COL_CUSTOMER_DOCUMENT_STATUS + " INTEGER,"
				                              + COL_CUSTOMER_PREV_POLICY_EXPIRY + " INTEGER,"
				                              + COL_CUSTOMER_EXPECTED_COMMISSION + " DOUBLE,"
				                              + COL_CUSTOMER_ACTUAL_COMMISSION + " DOUBLE,"
				                              + COL_VEHICLE_ID + " INTEGER,"
				                              + COL_REJ_REASON +" text)";
		db.execSQL(CREATE_TABLE);
	}

	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
	}

	public String insertData(Vector<CustomerBean> beanList, Context ctx) {
		SQLiteDatabase db = null;
		CustomerBean bean = null;
		long returnValue = 0;
		long dateTime_Millis = 0;
		synchronized (lock) {

			try {
				db = appDb.getWritableDatabase(TABLE_NAME);

				// ---------------- ADD Category
				for (int i = 0; i < beanList.size(); i++) {

					ContentValues values = new ContentValues();
					values.clear();
					bean = beanList.get(i);

					if (bean.getCustomer_name() != null && !bean.getCustomer_name().equalsIgnoreCase("null") && bean.getCustomer_name().trim().length() > 0) {
						values.put(COL_CUSTOMER_NAME, bean.getCustomer_name());
					}

					if (bean.getCustomer_email() != null && !bean.getCustomer_email().equalsIgnoreCase("null") && bean.getCustomer_email().trim().length() > 0) {
						values.put(COL_CUSTOMER_EMAIL, bean.getCustomer_email());
					}

					if (bean.getCustomer_mobile() != null && !bean.getCustomer_mobile().equalsIgnoreCase("null") && bean.getCustomer_mobile().trim().length() > 0) {
						values.put(COL_CUSTOMER_MOBILE, bean.getCustomer_mobile());
					}

					values.put(COL_QUOTES_URL, bean.getQuotesUrl());

					if (bean.getCustomer_vehical_name() != null && !bean.getCustomer_vehical_name().equalsIgnoreCase("null") && bean.getCustomer_vehical_name().trim().length() > 0) {
						values.put(COL_CUSTOMER_VEHICAL_NAME, bean.getCustomer_vehical_name());
					}

					values.put(COL_CUSTOMER_VEHICAL_CLASS , bean.getCustomer_vehical_class());
					values.put(COL_CUSTOMER_DOCUMENT_STATUS, bean.getCustomer_document_status());

					if (bean.getCustomer_prev_policy_expiry() != null) {
						values.put(COL_CUSTOMER_PREV_POLICY_EXPIRY, Utils.convertDateToEpoch(bean.getCustomer_prev_policy_expiry()));
					} else {
						values.putNull(COL_CUSTOMER_PREV_POLICY_EXPIRY);
					}

					values.put(COL_LEAD_ID, bean.getLead_id());
					values.put(COL_CUSTOMER_ACTUAL_COMMISSION, bean.getCustomer_actual_commission());
					values.put(COL_CUSTOMER_EXPECTED_COMMISSION, bean.getCustomer_expected_commission());

					if (bean.getVehicle_id() != null) {
						values.put(COL_VEHICLE_ID, bean.getVehicle_id());
					}

					if (bean.getRejection_reason() != null) {
						values.put(COL_REJ_REASON, bean.getRejection_reason());
					}

					returnValue = db.insert(TABLE_NAME, null, values);
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				appDb.closeDB();
			}
		}
		if (returnValue > 0l) {
			return String.valueOf(dateTime_Millis);
		} else {
			return "";
		}
	}

	public int updateData(CustomerBean obj, Context ctx) {
		int rowsAffected = -1;
		SQLiteDatabase db = null;
		synchronized (lock) {
			try {
				db = appDb.getWritableDatabase(TABLE_NAME);
				ContentValues values = new ContentValues();
				values.put(COL_LEAD_ID, obj.getLead_id());
				values.put(COL_CUSTOMER_NAME, obj.getCustomer_name());
				values.put(COL_CUSTOMER_EMAIL, obj.getCustomer_email());
				values.put(COL_CUSTOMER_MOBILE, obj.getCustomer_mobile());
				values.put(COL_CUSTOMER_VEHICAL_NAME, obj.getCustomer_vehical_name());
				values.put(COL_CUSTOMER_VEHICAL_CLASS, obj.getCustomer_vehical_class());
				values.put(COL_CUSTOMER_DOCUMENT_STATUS, obj.getCustomer_document_status());
				values.put(COL_CUSTOMER_PREV_POLICY_EXPIRY, obj.getCustomer_prev_policy_expiry().toString());
				values.put(COL_CUSTOMER_EXPECTED_COMMISSION, obj.getCustomer_expected_commission());
				values.put(COL_CUSTOMER_ACTUAL_COMMISSION, obj.getCustomer_actual_commission());
				values.put(COL_REJ_REASON, obj.getRejection_reason());
				values.put(COL_VEHICLE_ID, obj.getVehicle_id());
				String selection = COL_VEHICLE_ID + "=? ";
				String[] selectionArgs = new String[] { obj.getVehicle_id().toString() };
				try {
					rowsAffected = db.update(TABLE_NAME, values, selection, selectionArgs);
				} catch (Exception e) {
					e.printStackTrace();
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				appDb.closeDB();
			}
		}
		return rowsAffected;
	}

	public Vector<CustomerBean> getAllData(Context ctx) {
		Vector<CustomerBean> list = new Vector<CustomerBean>();
		synchronized (lock) {
			SQLiteDatabase db = null;
			Cursor cursor = null;
			try {
				db = appDb.getWritableDatabase(TABLE_NAME);
				String[] columns = new String[]{COL_LEAD_ID, COL_CUSTOMER_NAME, COL_CUSTOMER_EMAIL, COL_CUSTOMER_MOBILE, COL_QUOTES_URL, COL_CUSTOMER_VEHICAL_NAME, COL_CUSTOMER_VEHICAL_CLASS,  COL_CUSTOMER_DOCUMENT_STATUS, COL_CUSTOMER_PREV_POLICY_EXPIRY, COL_CUSTOMER_EXPECTED_COMMISSION, COL_CUSTOMER_ACTUAL_COMMISSION, COL_VEHICLE_ID, COL_REJ_REASON};
				cursor = db.query(false, TABLE_NAME, columns, null, null, null, null, null, null);
				// cursor = db.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE " + COL_CUSTOMER_DOCUMENT_STATUS + " !=3 UNION ALL SELECT * FROM " + TABLE_NAME + " WHERE " + COL_CUSTOMER_DOCUMENT_STATUS + " ==3", null);

				if (cursor.moveToFirst()) {
					do {
						CustomerBean bean = new CustomerBean();
						bean = getData(cursor);
						list.add(bean);
					} while (cursor.moveToNext());
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				if (cursor != null)
					cursor.close();
				appDb.closeDB();
			}
		}
		return list;
	}

	public Vector<CustomerBean> getDataMonthly(Context ctx, int month) {
		Vector<CustomerBean> list = new Vector<CustomerBean>();
		synchronized (lock) {
			SQLiteDatabase db = null;
			Cursor cursor = null;
			try {
				db = appDb.getWritableDatabase(TABLE_NAME);

                Integer src_date;
                Integer next_month_date;

                if(month >= Calendar.getInstance().get((Calendar.MONTH))+1) {
                    String date_in_string = new String("01/" + month + "/" + Calendar.getInstance().get(Calendar.YEAR));
                    Date date = new SimpleDateFormat("dd/MM/yyyy").parse(date_in_string);
                    src_date = Utils.convertDateToEpoch(date);

                    date_in_string = new String("01/" + (month+1) + "/" + Calendar.getInstance().get(Calendar.YEAR));
                    date = new SimpleDateFormat("dd/MM/yyyy").parse(date_in_string);
                    next_month_date = Utils.convertDateToEpoch(date);
                }else {
                    String date_in_string = new String("01/" + month + "/" + (Calendar.getInstance().get(Calendar.YEAR)+1));
                    Date date = new SimpleDateFormat("dd/MM/yyyy").parse(date_in_string);
                    src_date = Utils.convertDateToEpoch(date);

                    date_in_string = new String("01/" + (month+1) + "/" + (Calendar.getInstance().get(Calendar.YEAR)+1));
                    date = new SimpleDateFormat("dd/MM/yyyy").parse(date_in_string);
                    next_month_date = Utils.convertDateToEpoch(date);
                }

				String query = "select * from " + TABLE_NAME + " where " + COL_CUSTOMER_PREV_POLICY_EXPIRY + ">="+src_date+" AND "+COL_CUSTOMER_PREV_POLICY_EXPIRY + " <" + next_month_date + ";";

				cursor = db.rawQuery(query, null);
				Log.e("cursor", cursor.toString());
				if (cursor.moveToFirst()) {
					do {
						CustomerBean bean = new CustomerBean();
						bean = getData(cursor);
						list.add(bean);
					} while (cursor.moveToNext());
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				if (cursor != null)
					cursor.close();
				appDb.closeDB();
			}
		}
		return list;
	}

	public Vector<CustomerBean> getDataByStatus(Context ctx, int status) {
		Vector<CustomerBean> list = new Vector<CustomerBean>();
		synchronized (lock) {
			SQLiteDatabase db = null;
			Cursor cursor = null;
			try {
				db = appDb.getWritableDatabase(TABLE_NAME);
				String query = "select * from " + TABLE_NAME + " where " + COL_CUSTOMER_DOCUMENT_STATUS + "=? ;";
				String[] selectionArgs = new String[] { status + "" };
				cursor = db.rawQuery(query, selectionArgs);
				if (cursor.moveToFirst()) {
					do {
						CustomerBean bean = new CustomerBean();
						bean = getData(cursor);
						list.add(bean);
					} while (cursor.moveToNext());
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				if (cursor != null)
					cursor.close();
				appDb.closeDB();
			}
		}
		return list;
	}

	public int getCountByStatus(Context ctx, int status) {
		int count=0 ;
		synchronized (lock) {
			SQLiteDatabase db = null;
			Cursor cursor = null;
			try {
				db = appDb.getWritableDatabase(TABLE_NAME);
				String query = "select * from " + TABLE_NAME + " where " + COL_CUSTOMER_DOCUMENT_STATUS + "=? ;";
				String[] selectionArgs = new String[] { status + "" };
				cursor = db.rawQuery(query, selectionArgs);
				count = cursor.getCount();
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				if (cursor != null)
					cursor.close();
				appDb.closeDB();
			}
		}
		return count;
	}

	public Vector<CustomerBean> getDataByLessThan(Context ctx, int status) {
		Vector<CustomerBean> list = new Vector<CustomerBean>();
		synchronized (lock) {
			SQLiteDatabase db = null;
			Cursor cursor = null;
			try {
				db = appDb.getWritableDatabase(TABLE_NAME);
				String query = "select * from " + TABLE_NAME + " where " + COL_CUSTOMER_DOCUMENT_STATUS + "<=? OR "+COL_CUSTOMER_DOCUMENT_STATUS+"="+Utils.ACCEPTED_OFFLINE+" order by "+COL_LEAD_ID+" DESC ;";
				String[] selectionArgs = new String[] { status + "" };
				cursor = db.rawQuery(query, selectionArgs);
				if (cursor.moveToFirst()) {
					do {
						CustomerBean bean = new CustomerBean();
						bean = getData(cursor);
						list.add(bean);
					} while (cursor.moveToNext());
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				if (cursor != null)
					cursor.close();
				appDb.closeDB();
			}
		}
		return list;
	}

	private CustomerBean getData(Cursor cursor) {
		SimpleDateFormat formatter = new SimpleDateFormat("dd-mm-yyyy", Locale.US);
		Date custPrevPolicyExpiry = null;
		int lead_id = cursor.getInt(cursor.getColumnIndex(COL_LEAD_ID));
		String custName = cursor.getString(cursor.getColumnIndex(COL_CUSTOMER_NAME));
		String custEmail = cursor.getString(cursor.getColumnIndex(COL_CUSTOMER_EMAIL));
		String custMobile = cursor.getString(cursor.getColumnIndex(COL_CUSTOMER_MOBILE));
		String custVehicleName = cursor.getString(cursor.getColumnIndex(COL_CUSTOMER_VEHICAL_NAME));
		int custVehicleClass = cursor.getInt(cursor.getColumnIndex(COL_CUSTOMER_VEHICAL_CLASS));
		int custDocStatus = cursor.getInt(cursor.getColumnIndex(COL_CUSTOMER_DOCUMENT_STATUS));

		try {
			Integer epoch = cursor.getInt(cursor.getColumnIndex(COL_CUSTOMER_PREV_POLICY_EXPIRY));
			if (epoch != null && epoch != 0) {
				custPrevPolicyExpiry = Utils.convertEpochToDate(epoch);
			}
		} catch (Exception e) {
			e.printStackTrace();
			// custPrevPolicyExpiry = new
			// SimpleDateFormat("dd-mm-yyyy").format(new Date());
		}

		double custExpectedComm = cursor.getDouble(cursor.getColumnIndex(COL_CUSTOMER_EXPECTED_COMMISSION));
		double custActualComm = cursor.getDouble(cursor.getColumnIndex(COL_CUSTOMER_ACTUAL_COMMISSION));

		Integer vehicleId = cursor.getInt(cursor.getColumnIndex(COL_VEHICLE_ID));
		String rejReason = cursor.getString(cursor.getColumnIndex(COL_REJ_REASON));

		CustomerBean bean = new CustomerBean();

		bean.setLead_id(lead_id);
		bean.setCustomer_name(custName);
		bean.setCustomer_email(custEmail);
		bean.setCustomer_mobile(custMobile);
		bean.setCustomer_vehical_name(custVehicleName);
		bean.setCustomer_vehical_class(custVehicleClass);
		bean.setCustomer_document_status(custDocStatus);
		bean.setCustomer_prev_policy_expiry(custPrevPolicyExpiry);
		bean.setCustomer_expected_commission(custExpectedComm);
		bean.setCustomer_actual_commission(custActualComm);
		bean.setVehicle_id(vehicleId);
		bean.setRejection_reason(rejReason);
		bean.setQuotesUrl(cursor.getString(cursor.getColumnIndex(COL_QUOTES_URL)));

		return bean;
	}

	// public Vector<BudgetBean> getAllBudget(Activity baseActivity, String
	// tokenId, boolean applyChecks) {
	// AppDb db = AppDb.getInstance(baseActivity);
	// BudgetTable budgetTable = (BudgetTable)
	// db.getTableObject(BudgetTable.TABLE_NAME);
	// return budgetTable.getAllBudgets(tokenId, baseActivity, applyChecks);
	// }

	public int deleteCustomer(String vehicleId, Context ctx) {
		SQLiteDatabase db = null;
		int rowsAffected = -1;
		synchronized (lock) {
			try {
				db = appDb.getWritableDatabase(TABLE_NAME);
				String selection = COL_VEHICLE_ID + "=? ";
				String[] selectionArgs = new String[] { vehicleId };
				rowsAffected = db.delete(TABLE_NAME, selection, selectionArgs);
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				appDb.closeDB();
			}
		}
		return rowsAffected;
	}

	public int deleteAllData(Context ctx) {
		SQLiteDatabase db = null;
		int rowsAffected = -1;
		synchronized (lock) {
			try {
				db = appDb.getWritableDatabase(TABLE_NAME);
				String selection = "1";
				String[] selectionArgs = new String[] {};
				rowsAffected = db.delete(TABLE_NAME, selection, selectionArgs);
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				appDb.closeDB();
			}
		}
		return rowsAffected;
	}
}