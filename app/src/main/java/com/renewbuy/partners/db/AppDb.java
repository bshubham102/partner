package com.renewbuy.partners.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.concurrent.atomic.AtomicInteger;

public class AppDb {

    //adb shell am broadcast -a com.android.vending.INSTALL_REFERRER -n com.renewbuy.partners/com.renewbuy.partners.receivers –es “referrer” “utm_source=test_source&utm_medium=emma&utm_term=test_term&utm_content=test_content&utm_campaign=666″
    //market://details?id=com.renewbuy.partners&referrer=partner%3Dd2c


	private static final int DATABASE_VERSION = 21;
	public static final String DATABASE_NAME = "renewbuy_db";
	private SQLiteOpenHelper helper;
	private CustomerTable categoryTable;
	private NotificationTable notificationTable;
	private PaymentTable paymentTable;
	private StatsTable statsTable ;
	private static AppDb instance = null;
	private AtomicInteger mOpenCounter = new AtomicInteger();
	private SQLiteDatabase mDatabase;

	public synchronized SQLiteDatabase getWritableDatabase(String tableName)
    {
		if (mOpenCounter.incrementAndGet() == 1 && helper != null) {
			mDatabase = helper.getWritableDatabase();
		}
		return mDatabase;
	}

	public synchronized SQLiteDatabase getReadableDatabase(String tableName)
	{
		if (mOpenCounter.incrementAndGet() == 1 && helper != null) {
			mDatabase = helper.getReadableDatabase();
		}
		return mDatabase;
	}

	private AppDb(Context context) {
		helper = new SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {
			@Override
			public void onCreate(SQLiteDatabase db) {
				categoryTable.createTable(db);
				notificationTable.createTable(db);
				paymentTable.createTable(db);
				statsTable.createTable(db);
			}

			@Override
			public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
				categoryTable.onUpgrade(db, oldVersion, newVersion);
				notificationTable.onUpgrade(db, oldVersion, newVersion);
				paymentTable.onUpgrade(db, oldVersion, newVersion);
				statsTable.onUpgrade(db, oldVersion, newVersion);
				onCreate(db);
			}

		};
		categoryTable = new CustomerTable(this);
		notificationTable = new NotificationTable(this);
		paymentTable = new PaymentTable(this);
		statsTable = new StatsTable(this) ;
	}

	public static synchronized AppDb getInstance(Context context) {
		if (instance == null) {
			instance = new AppDb(context);
		}
		return instance;
	}

	public Object getTableObject(String tableName) {
		if (tableName.equalsIgnoreCase(CustomerTable.TABLE_NAME)) {
			return categoryTable;
		}else if (tableName.equalsIgnoreCase(NotificationTable.TABLE_NAME)) {
			return notificationTable;
		}else if (tableName.equalsIgnoreCase(PaymentTable.TABLE_NAME)) {
			return paymentTable;
		}else if (tableName.equalsIgnoreCase(StatsTable.TABLE_NAME)) {
			return statsTable;
		}
		return null;
	}

	public Object getPaymentTableObject(String tableName) {
		if (tableName.equalsIgnoreCase(PaymentTable.TABLE_NAME)) {
			return paymentTable;
		} /*else if (tableName.equalsIgnoreCase(SubCategoryTable.TABLE_NAME)) {
			return subCategoryTable;
		} */
		return null;
	}

	public Object getStatsTableObject(String tableName) {
		if (tableName.equalsIgnoreCase(StatsTable.TABLE_NAME)) {
			return statsTable;
		} /*else if (tableName.equalsIgnoreCase(SubCategoryTable.TABLE_NAME)) {
			return subCategoryTable;
		} */
		return null;
	}

	public Object getNotificationTableObject(String tableName){
		if (tableName.equalsIgnoreCase(NotificationTable.TABLE_NAME)) {
			return notificationTable;
		} /*else if (tableName.equalsIgnoreCase(SubCategoryTable.TABLE_NAME)) {
			return subCategoryTable;
		} */
		return null;
	}

	public void closeDB() {
		if (mOpenCounter.decrementAndGet() == 0 && helper != null && mDatabase != null) {
			mDatabase.close();
		}
	}
}