package com.renewbuy.partners.db;

import java.util.Date;

/**
 * Created by shubham on 17/10/16.
 */

public class StatsBean {

    int stats_flag, total_uploads, accpet_policy, submit_policy, missed_policy, rejected_policy,  actual_commission, expected_commission, missed_commission, balance_commission, car_sold,
        bike_sold, car_earning, bike_earning ;

    Date startDate, endDate ;


    public int getStats_flag() {
        return stats_flag;
    }

    public void setStats_flag(int stats_flag) {
        this.stats_flag = stats_flag;
    }


    public int getTotal_uploads() {
        return total_uploads;
    }

    public void setTotal_uploads(int total_uploads) {
        this.total_uploads = total_uploads;
    }

    public int getAccpet_policy() {
        return accpet_policy;
    }

    public void setAccpet_policy(int accpet_policy) {
        this.accpet_policy = accpet_policy;
    }

    public int getActual_commission() {
        return actual_commission;
    }

    public void setActual_commission(int actual_commission) {
        this.actual_commission = actual_commission;
    }

    public int getExpected_commission() {
        return expected_commission;
    }

    public void setExpected_commission(int expected_commission) {
        this.expected_commission = expected_commission;
    }

    public int getMissed_commission() {
        return missed_commission;
    }

    public void setMissed_commission(int missed_commission) {
        this.missed_commission = missed_commission;
    }

    public int getBalance_commission() {
        return balance_commission;
    }

    public void setBalance_commission(int balance_commission) {
        this.balance_commission = balance_commission;
    }

    public int getCar_sold() {
        return car_sold;
    }

    public void setCar_sold(int car_sold) {
        this.car_sold = car_sold;
    }

    public int getBike_sold() {
        return bike_sold;
    }

    public void setBike_sold(int bike_sold) {
        this.bike_sold = bike_sold;
    }

    public int getCar_earning() {
        return car_earning;
    }

    public void setCar_earning(int car_earning) {
        this.car_earning = car_earning;
    }

    public int getBike_earning() {
        return bike_earning;
    }

    public void setBike_earning(int bike_earning) {
        this.bike_earning = bike_earning;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public int getSubmit_policy() {
        return submit_policy;
    }

    public void setSubmit_policy(int submit_policy) {
        this.submit_policy = submit_policy;
    }

    public int getMissed_policy() {
        return missed_policy;
    }

    public void setMissed_policy(int missed_policy) {
        this.missed_policy = missed_policy;
    }

    public int getRejected_policy() {
        return rejected_policy;
    }

    public void setRejected_policy(int rejected_policy) {
        this.rejected_policy = rejected_policy;
    }
}
