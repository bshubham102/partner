package com.renewbuy.partners.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.renewbuy.partners.Utils;

import java.util.Vector;

/**
 * Created by shubham on 17/10/16.
 */

public class StatsTable {
    public static final String TABLE_NAME = "m_stats";
    public static final String COL_TOTAL_UPLOAD = "total_upload";
    public static final String COL_ACCPETED_POLICY = "accepted_policy";
    public static final String COL_SUBMITTED_POLICY = "submitted_policy";
    public static final String COL_MISSED_POLICY = "missed_policy";
    public static final String COL_REJECTED_POLICY = "rejected_policy";
    public static final String COL_ACTUAL_COMMISSION = "actual_commission";
    public static final String COL_EXPECTED_COMMISSION = "expected_commission";
    public static final String COL_MISSED_COMMISSION = "missed_commission";
    public static final String COL_BALANCE_COMMISSION = "balance_commission";
    public static final String COL_CAR_SOLD = "car_sold";
    public static final String COL_BIKE_SOLD = "bike_sold";
    public static final String COL_CAR_EARNING = "car_earning";
    public static final String COL_BIKE_EARNING = "bike_earning";
    public static final String COL_START_DATE = "start_date";
    public static final String COL_END_DATE = "end_date";
    public static final String COL_STATS_BY = "stats_by";

    public static final int  YEAR = 2;
    public static final int  MONTH = 1;
    public static final int  TODAY = 0;


    Object lock = new Object();

    public static final String TOKEN_ID = "token_id";

    private AppDb appDb;

    public StatsTable(AppDb appDb) {
        this.appDb = appDb;
    }

    public void createTable(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + " ("
                + COL_TOTAL_UPLOAD + " INTEGER,"
                + COL_ACTUAL_COMMISSION + " INTEGER,"
                + COL_EXPECTED_COMMISSION + " INTEGER,"
                + COL_MISSED_COMMISSION + " INTEGER,"
                + COL_BALANCE_COMMISSION + " INTEGER,"
                + COL_SUBMITTED_POLICY + " INTEGER,"
                + COL_ACCPETED_POLICY + " INTEGER,"
                + COL_MISSED_POLICY + " INTEGER,"
                + COL_REJECTED_POLICY + " INTEGER,"
                + COL_CAR_SOLD + " INTEGER,"
                + COL_BIKE_SOLD + " INTEGER,"
                + COL_CAR_EARNING + " INTEGER,"
                + COL_BIKE_EARNING + " INTEGER,"
                + COL_STATS_BY + " INTEGER,"
                + COL_START_DATE + " INTEGER,"
                + COL_END_DATE + " INTEGER)";
        db.execSQL(CREATE_TABLE);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
    }

    public String insertData(StatsBean bean, Context ctx) {
        SQLiteDatabase db = null;
        long returnValue = 0;
        long dateTime_Millis = 0;
        synchronized (lock) {

            try {
                db = appDb.getWritableDatabase(TABLE_NAME);

                    ContentValues values = new ContentValues();
                    values.clear();

                    values.put(COL_STATS_BY,bean.getStats_flag());
                    values.put(COL_TOTAL_UPLOAD,bean.getTotal_uploads());
                    values.put(COL_ACTUAL_COMMISSION, bean.getActual_commission());
                    values.put(COL_EXPECTED_COMMISSION, bean.getExpected_commission());
                    values.put(COL_MISSED_COMMISSION, bean.getMissed_commission());
                    values.put(COL_BALANCE_COMMISSION, bean.getBalance_commission());


                    values.put(COL_SUBMITTED_POLICY, bean.getSubmit_policy());
                    values.put(COL_ACCPETED_POLICY, bean.getAccpet_policy());
                    values.put(COL_MISSED_POLICY, bean.getMissed_policy());
                    values.put(COL_REJECTED_POLICY, bean.getRejected_policy());

                    values.put(COL_CAR_SOLD, bean.getCar_sold());
                    values.put(COL_BIKE_SOLD, bean.getBike_sold());
                    values.put(COL_CAR_EARNING, bean.getCar_earning());
                    values.put(COL_BIKE_EARNING, bean.getBike_earning());

                    if(bean.getStartDate() != null){
                        values.put(COL_START_DATE, Utils.convertDateToEpoch(bean.getStartDate()));
                    } else {
                        values.putNull(COL_START_DATE);
                    }

                    if(bean.getEndDate() != null){
                        values.put(COL_END_DATE, Utils.convertDateToEpoch(bean.getEndDate()));
                    } else {
                        values.putNull(COL_END_DATE);
                    }

                    returnValue = db.insert(TABLE_NAME, null, values);

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                appDb.closeDB();
            }
        }
        if (returnValue > 0l) {
            return String.valueOf(dateTime_Millis);
        } else {
            return "";
        }
    }

    public int updateData(StatsBean obj, Context ctx) {
        int rowsAffected = -1;
        SQLiteDatabase db = null;
        synchronized (lock) {
            try {
                db = appDb.getWritableDatabase(TABLE_NAME);
                ContentValues values = new ContentValues();
                values.put(COL_STATS_BY,obj.getStats_flag());
                values.put(COL_TOTAL_UPLOAD,obj.getTotal_uploads());
                values.put(COL_ACTUAL_COMMISSION, obj.getActual_commission());
                values.put(COL_EXPECTED_COMMISSION, obj.getExpected_commission());
                values.put(COL_MISSED_COMMISSION, obj.getMissed_commission());
                values.put(COL_BALANCE_COMMISSION, obj.getBalance_commission());
                values.put(COL_CAR_SOLD, obj.getCar_sold());

                values.put(COL_SUBMITTED_POLICY, obj.getSubmit_policy());
                values.put(COL_ACCPETED_POLICY, obj.getAccpet_policy());
                values.put(COL_MISSED_POLICY, obj.getMissed_policy());
                values.put(COL_REJECTED_POLICY, obj.getRejected_policy());

                values.put(COL_BIKE_SOLD, obj.getBike_sold());
                values.put(COL_CAR_EARNING, obj.getCar_earning());
                values.put(COL_BIKE_EARNING, obj.getBike_earning());
                values.put(COL_START_DATE, obj.getStartDate().toString());
                values.put(COL_END_DATE, obj.getEndDate().toString());
                String selection = COL_START_DATE + "=? ";
                String[] selectionArgs = new String[] { obj.getStartDate().toString() };
                try {
                    rowsAffected = db.update(TABLE_NAME, values, selection, selectionArgs);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                appDb.closeDB();
            }
        }
        return rowsAffected;
    }

    public Vector<StatsBean> getAllData(Context ctx) {
        Vector<StatsBean> list = new Vector<StatsBean>();
        synchronized (lock) {
            SQLiteDatabase db = null;
            Cursor cursor = null;
            try {
                db = appDb.getWritableDatabase(TABLE_NAME);
                String[] columns = new String[]{COL_STATS_BY, COL_ACTUAL_COMMISSION, COL_EXPECTED_COMMISSION, COL_BALANCE_COMMISSION, COL_MISSED_COMMISSION, COL_TOTAL_UPLOAD, COL_ACCPETED_POLICY, COL_SUBMITTED_POLICY, COL_MISSED_POLICY, COL_REJECTED_POLICY,  COL_CAR_SOLD, COL_BIKE_SOLD, COL_CAR_EARNING, COL_BIKE_EARNING};
                cursor = db.query(false, TABLE_NAME, columns, null, null, null, null, null, null);
                // cursor = db.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE " + COL_CUSTOMER_DOCUMENT_STATUS + " !=3 UNION ALL SELECT * FROM " + TABLE_NAME + " WHERE " + COL_CUSTOMER_DOCUMENT_STATUS + " ==3", null);

                if (cursor.moveToFirst()) {
                    do {
                        StatsBean bean = new StatsBean();
                        bean = getData(cursor);
                        list.add(bean);
                    } while (cursor.moveToNext());
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (cursor != null)
                    cursor.close();
                appDb.closeDB();
            }
        }
        return list;
    }

    public Vector<StatsBean> getstatsByDate(Context ctx, int statsBy) {
        Vector<StatsBean> list = new Vector<StatsBean>();
        synchronized (lock) {
            SQLiteDatabase db = null;
            Cursor cursor = null;
            try {
                db = appDb.getWritableDatabase(TABLE_NAME);
                //cursor = db.query(TABLE_NAME,null,COL_STATS_BY,new String[]{ "?"+statsBy},null,null,null);
                String query = "select * from " + TABLE_NAME + " where " + COL_STATS_BY + "=? ;";
                String[] selectionArgs = new String[] { statsBy+""};
                cursor = db.rawQuery(query, selectionArgs);
                if (cursor.moveToFirst()) {
                    do {
                        StatsBean bean = new StatsBean();
                        bean = getData(cursor);
                        list.add(bean);
                    } while (cursor.moveToNext());
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (cursor != null)
                    cursor.close();
                appDb.closeDB();
            }
        }
        return list;
    }

    private StatsBean getData(Cursor cursor) {

        int actual = cursor.getInt(cursor.getColumnIndex(COL_ACTUAL_COMMISSION));
        int statsBy = cursor.getInt(cursor.getColumnIndex(COL_STATS_BY));
        int expected = cursor.getInt(cursor.getColumnIndex(COL_EXPECTED_COMMISSION));
        int balance = cursor.getInt(cursor.getColumnIndex(COL_BALANCE_COMMISSION));
        int missed = cursor.getInt(cursor.getColumnIndex(COL_MISSED_COMMISSION));
        int total_upload = cursor.getInt(cursor.getColumnIndex(COL_TOTAL_UPLOAD));

        int submit_policy = cursor.getInt(cursor.getColumnIndex(COL_SUBMITTED_POLICY));
        int accpet_policy = cursor.getInt(cursor.getColumnIndex(COL_ACCPETED_POLICY));
        int missed_policy = cursor.getInt(cursor.getColumnIndex(COL_MISSED_POLICY));
        int reject_policy = cursor.getInt(cursor.getColumnIndex(COL_REJECTED_POLICY));


        int car_sold = cursor.getInt(cursor.getColumnIndex(COL_CAR_SOLD));
        int bike_sold = cursor.getInt(cursor.getColumnIndex(COL_BIKE_SOLD));
        int car_earn = cursor.getInt(cursor.getColumnIndex(COL_CAR_EARNING));
        int bike_earn = cursor.getInt(cursor.getColumnIndex(COL_BIKE_EARNING));


        StatsBean bean = new StatsBean();
        bean.setStats_flag(statsBy);
        bean.setTotal_uploads(total_upload);
        bean.setActual_commission(actual);
        bean.setExpected_commission(expected);
        bean.setBalance_commission(balance);
        bean.setMissed_commission(missed);
        bean.setSubmit_policy(submit_policy);
        bean.setAccpet_policy(accpet_policy);
        bean.setMissed_policy(missed_policy);
        bean.setRejected_policy(reject_policy);
        bean.setCar_sold(car_sold);
        bean.setBike_sold(bike_sold);
        bean.setCar_earning(car_earn);
        bean.setBike_earning(bike_earn);

        return bean;
    }

    // public Vector<BudgetBean> getAllBudget(Activity baseActivity, String
    // tokenId, boolean applyChecks) {
    // AppDb db = AppDb.getInstance(baseActivity);
    // BudgetTable budgetTable = (BudgetTable)
    // db.getTableObject(BudgetTable.TABLE_NAME);
    // return budgetTable.getAllBudgets(tokenId, baseActivity, applyChecks);
    // }

    public int deleteCustomer(String vehicleId, Context ctx) {
        SQLiteDatabase db = null;
        int rowsAffected = -1;
        synchronized (lock) {
            try {
                db = appDb.getWritableDatabase(TABLE_NAME);
                String selection = COL_START_DATE + "=? ";
                String[] selectionArgs = new String[] { vehicleId };
                rowsAffected = db.delete(TABLE_NAME, selection, selectionArgs);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                appDb.closeDB();
            }
        }
        return rowsAffected;
    }

    public int deleteAllData(Context ctx) {
        SQLiteDatabase db = null;
        int rowsAffected = -1;
        synchronized (lock) {
            try {
                db = appDb.getWritableDatabase(TABLE_NAME);
                String selection = "1";
                String[] selectionArgs = new String[] {};
                rowsAffected = db.delete(TABLE_NAME, selection, selectionArgs);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                appDb.closeDB();
            }
        }
        return rowsAffected;
    }
}
