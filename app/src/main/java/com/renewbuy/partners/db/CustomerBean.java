package com.renewbuy.partners.db;

import java.util.Date;

public class CustomerBean {

	int lead_id ;
	String customer_name;
	String customer_email;
	String customer_mobile;
	String customer_vehical_name;
	int customer_document_status;
	int customer_vehical_class;
	Date customer_prev_policy_expiry;
	double customer_expected_commission;
	double customer_actual_commission;
	Integer vehicle_id;
	String rejection_reason;
	String quotes_url;

	public int getCustomer_vehical_class() {
		return customer_vehical_class;
	}

	public void setCustomer_vehical_class(int customer_vehical_class) {
		this.customer_vehical_class = customer_vehical_class;
	}

	public Integer getVehicle_id() {
		return vehicle_id;
	}

	public void setVehicle_id(Integer vehicle_id) {
		this.vehicle_id = vehicle_id;
	}

	public String getRejection_reason() {
		return rejection_reason;
	}

	public void setRejection_reason(String rejection_reason) {
		this.rejection_reason = rejection_reason;
	}

	public String getQuotesUrl() {
		return quotes_url;
	}

	public void setQuotesUrl(String url) {
		this.quotes_url = url;
	}

	public String getCustomer_name() {
		return customer_name;
	}

	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}

	public String getCustomer_email() {
		return customer_email;
	}

	public void setCustomer_email(String customer_email) {
		this.customer_email = customer_email;
	}

	public String getCustomer_mobile() {
		return customer_mobile;
	}

	public void setCustomer_mobile(String customer_mobile) {
		this.customer_mobile = customer_mobile;
	}

	public String getCustomer_vehical_name() {
		return customer_vehical_name;
	}

	public void setCustomer_vehical_name(String customer_vehical_name_number) {
		this.customer_vehical_name = customer_vehical_name_number;
	}

	public int getCustomer_document_status() {
		return customer_document_status;
	}

	public void setCustomer_document_status(int customer_document_status) {
		this.customer_document_status = customer_document_status;
	}

	public Date getCustomer_prev_policy_expiry() {
		return customer_prev_policy_expiry;
	}

	public void setCustomer_prev_policy_expiry(Date customer_prev_policy_expiry) {
		this.customer_prev_policy_expiry = customer_prev_policy_expiry;
	}

	public double getCustomer_expected_commission() {
		return customer_expected_commission;
	}

	public void setCustomer_expected_commission(double customer_expected_commission) {
		this.customer_expected_commission = customer_expected_commission;
	}

	public double getCustomer_actual_commission() {
		return customer_actual_commission;
	}

	public void setCustomer_actual_commission(double customer_actual_commission) {
		this.customer_actual_commission = customer_actual_commission;
	}

	public int getLead_id() {
		return lead_id;
	}

	public void setLead_id(int lead_id) {
		this.lead_id = lead_id;
	}
}
