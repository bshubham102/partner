package com.renewbuy.partners.db;

import java.util.Date;

public class NotificationBean {

    String title;
    String small_body_1;
    String small_body_2;
    String body;
    String tab;
    String type;
    String id,level ,email;


    String OnclickAction;
    Date time_stamp , expiryTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLevel() {
        return level;
    }

    public String getOnclickAction() {
        return OnclickAction;
    }

    public void setOnclickAction(String onclickAction) {
        OnclickAction = onclickAction;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getExpiryTime() {
        return expiryTime;
    }

    public void setExpiryTime(Date expiryTime) {
        this.expiryTime = expiryTime;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSmall_body_1() {
        return small_body_1;
    }

    public void setSmall_body_1(String small_body_1) {
        this.small_body_1 = small_body_1;
    }

    public String getSmall_body_2() {
        return small_body_2;
    }

    public void setSmall_body_2(String small_body_2) {
        this.small_body_2 = small_body_2;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getTab() {
        return tab;
    }

    public void setTab(String tab) {
        this.tab = tab;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getTimeStamp() {
        return time_stamp;
    }

    public void setTimeStamp(Date time_stamp) {
        this.time_stamp = time_stamp;
    }

    public Date getTime_stamp() {
        return time_stamp;
    }

    public void setTime_stamp(Date time_stamp) {
        this.time_stamp = time_stamp;
    }
}