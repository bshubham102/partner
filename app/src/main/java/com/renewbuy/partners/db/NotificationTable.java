package com.renewbuy.partners.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.renewbuy.partners.MainActivity;
import com.renewbuy.partners.Utils;
import com.renewbuy.partners.modelClass.NotificationModel;

import java.util.Vector;

/**
 * Created by shubham on 31/5/16.
 */
public class NotificationTable  {

    public static final String TABLE_NAME = "m_notification";

    public static final String COL_NOTIFI_ID = "_id";
    public static final String COL_NOTIFI_USER_ID = "user_id";
    public static final String COL_NOTIFI_TITLE = "title";
    public static final String COL_NOTIFI_BODY = "body";
    public static final String COL_NOTIFI_ICON = "icon";
    public static final String COL_NOTIFI_ONCLICK_ACTION = "onclick_action";
    public static final String COL_NOTIFI_READ_STATUS = "read_status";
    public static final String COL_NOTIFI_TIME_STAMP = "time";
    public static final String COL_NOTIFI_LEVEL = "level";
    public static final String COL_NOTIFI_EXPIRY_TIME_STAMP = "expiry_time";

    private AppDb appDb ;
    Object sync = new Object();

    public NotificationTable(AppDb appDb) {
        this.appDb = appDb;
    }

    public void createTable(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME +
                " (" +
                COL_NOTIFI_ID             + " Integer," +
                COL_NOTIFI_USER_ID        + " text," +
                COL_NOTIFI_TITLE          + " text," +
                COL_NOTIFI_BODY           + " text,"    +
                COL_NOTIFI_ICON           + " text,"    +
                COL_NOTIFI_ONCLICK_ACTION + " text,"    +
                COL_NOTIFI_READ_STATUS    + " text," +
                COL_NOTIFI_LEVEL          + " Integer," +
                COL_NOTIFI_TIME_STAMP     + " text,"    +
                COL_NOTIFI_EXPIRY_TIME_STAMP     + " text "    + // Format MM/DD/YY
                " )";

        db.execSQL(CREATE_TABLE);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
    }
    public String insertData(NotificationModel notification) {
        SQLiteDatabase db = null;
        long returnValue = 0;
        long dateTime_Millis = 0;
        synchronized (sync) {

            try {
                db = appDb.getWritableDatabase(TABLE_NAME);

                ContentValues values = new ContentValues();
                values.clear();

                if(notification.getEmail() != null && !notification.getEmail().equalsIgnoreCase("null") && notification.getEmail().trim().length() > 0){
                    values.put(COL_NOTIFI_USER_ID,notification.getEmail());
                }
                if(notification.getId() != null && !notification.getId().equalsIgnoreCase("null") && notification.getId().trim().length() > 0){
                    values.put(COL_NOTIFI_ID,notification.getId());
                }
                if ( notification.getTitle() != null && !notification.getTitle().equalsIgnoreCase("null") && notification.getTitle().trim().length() > 0) {
                    values.put(COL_NOTIFI_TITLE, notification.getTitle());
                }
                if ( notification.getDescription() != null && !notification.getDescription().equalsIgnoreCase("null") && notification.getDescription().trim().length() > 0) {
                    values.put(COL_NOTIFI_BODY, notification.getDescription());
                }
                if( notification.getIcon() != null && !notification.getIcon().equalsIgnoreCase("null") && notification.getIcon().trim().length() > 0){
                    values.put(COL_NOTIFI_ICON,notification.getIcon());
                }
                if( notification.getOnclick_Action() != null && !notification.getOnclick_Action().equalsIgnoreCase("null") && notification.getOnclick_Action().trim().length() > 0){
                    values.put(COL_NOTIFI_ONCLICK_ACTION,notification.getOnclick_Action());
                }

                if( !notification.isUnread() ) {
                    values.put(COL_NOTIFI_READ_STATUS , "0");
                }else {
                    values.put(COL_NOTIFI_READ_STATUS , "1");
                }

                if( notification.getLevel() == null && !notification.getLevel().equalsIgnoreCase("null") && notification.getLevel().trim().length() > 0){
                    values.put(COL_NOTIFI_LEVEL , notification.getLevel());
                }

                if( notification.getExpiry_timestamp() == null && !notification.getExpiry_timestamp().equalsIgnoreCase("null") && notification.getExpiry_timestamp().trim().length() > 0){
                    values.put(COL_NOTIFI_EXPIRY_TIME_STAMP , notification.getExpiry_timestamp());
                }

                if (notification.getTimestamp() != null) {
                    values.put(COL_NOTIFI_TIME_STAMP, notification.getTimestamp());
                } else {
                    values.putNull(COL_NOTIFI_TIME_STAMP);
                }

                returnValue = db.insert(TABLE_NAME, null, values);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {

                appDb.closeDB();
            }
        }
        if (returnValue > 0l) {
            return String.valueOf(dateTime_Millis);

        } else {
            return "";
        }
    }

    public int markRead(){
        int rows = 0;
        SQLiteDatabase db = null ;
        Cursor cursor = null ;
        synchronized (sync){
            try{
                db = appDb.getWritableDatabase(TABLE_NAME);
                ContentValues contentValues = new ContentValues();
                contentValues.put(COL_NOTIFI_READ_STATUS,0);
                String email = Utils.getStringFromPreferences(MainActivity.activity,Utils.emailKey);
                rows = db.update(TABLE_NAME,contentValues,COL_NOTIFI_USER_ID+"='"+email +"' AND "+COL_NOTIFI_READ_STATUS+"=1",null);

            }catch (Exception e){
                e.printStackTrace();
                rows = -1;
            }finally {
                if (cursor != null)
                    cursor.close();

                appDb.closeDB();
            }
        }
        return rows;
    }
    public int getCount(String column_id , String value ) {
        int count ;
        SQLiteDatabase db = null ;
        Cursor cursor = null ;
        synchronized (sync){
            try{
                db =appDb.getReadableDatabase(NotificationTable.TABLE_NAME);

                String email = Utils.getStringFromPreferences(MainActivity.activity,Utils.emailKey);
                Log.i("createTable: ",db.getPath());
                String[] columns  =  new String[]{
                        column_id
                };
                cursor =  db.query(TABLE_NAME,columns,column_id+"=? AND "+COL_NOTIFI_USER_ID+"=?",new String[]{value , email},null,null,null);
                count = cursor.getCount();
            }catch (Exception e){
                e.printStackTrace();
                count = 100 ;
            }finally {
                if (cursor != null)
                    cursor.close();


                appDb.closeDB();
            }
        }
        return count ;
    }


    public Vector<NotificationModel> getNotifications(Context ctx) {
        Vector<NotificationModel> list = new Vector<NotificationModel>();
        synchronized (sync) {
            SQLiteDatabase db = null;
            Cursor cursor = null;
            try {
                db = appDb.getWritableDatabase(TABLE_NAME);
                String[] columns = {
                        COL_NOTIFI_TITLE ,
                        COL_NOTIFI_BODY ,
                        COL_NOTIFI_ICON ,
                        COL_NOTIFI_ONCLICK_ACTION,
                        COL_NOTIFI_READ_STATUS,
                        COL_NOTIFI_TIME_STAMP
                };
                String[] values = {
                     Utils.getStringFromPreferences(MainActivity.activity,Utils.emailKey)
                };
                cursor = db.query(false, TABLE_NAME, columns, COL_NOTIFI_USER_ID+"=?",values, null, null, COL_NOTIFI_TIME_STAMP+" ASC", null);
                if (cursor.moveToFirst()) {
                    do {
                        NotificationModel bean = new NotificationModel();
                        bean = getData(cursor);
                        list.insertElementAt(bean, 0);
                    } while (cursor.moveToNext());
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (cursor != null)
                    cursor.close();

                appDb.closeDB();
            }
        }
        return list;
    }

    public boolean deleteAllData(){
        int count = 0;
        synchronized (sync) {
            SQLiteDatabase db = null;
            Cursor cursor = null;
            try {
                db = appDb.getWritableDatabase(TABLE_NAME);
                count = db.delete(TABLE_NAME,null,null);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (cursor != null)
                    cursor.close();

                appDb.closeDB();
            }
        }
        if(count > 0)
            return true;

        return false ;
    }

    private NotificationModel getData(Cursor cursor) {
        String  title    = cursor.getString(cursor.getColumnIndex(COL_NOTIFI_TITLE));
        String  body    = cursor.getString(cursor.getColumnIndex(COL_NOTIFI_BODY));
        String  icon    = cursor.getString(cursor.getColumnIndex(COL_NOTIFI_ICON));
        String  onclick = cursor.getString(cursor.getColumnIndex(COL_NOTIFI_ONCLICK_ACTION));
        String  time    = cursor.getString(cursor.getColumnIndex(COL_NOTIFI_TIME_STAMP));
        String isunread = cursor.getColumnName(cursor.getColumnIndex(COL_NOTIFI_READ_STATUS));

        NotificationModel bean = new NotificationModel();
        bean.setTitle(title);
        bean.setDescription(body);
        bean.setIcon(icon);
        bean.setOnclick_Action(onclick);
        //bean.setTimeStamp();
        try {
            if (time != null && !time.equals("0")) {
                bean.setTimestamp(time);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return bean;
    }

    public boolean deleteExpiryNotification(String Date) {
        int count = 0;
        synchronized (sync) {
            SQLiteDatabase db = null;
            Cursor cursor = null;
            try {
                db = appDb.getWritableDatabase(TABLE_NAME);
                count = db.delete(TABLE_NAME,COL_NOTIFI_EXPIRY_TIME_STAMP+" < "+Date,null);
                Log.i("Count", "deleteExpiryNotification Count: "+count);
            } catch (Exception e) {
                e.printStackTrace();
                Log.i("Count", "deleteExpiryNotification Count: "+count+" "+e.toString());
            } finally {
                if (cursor != null)
                    cursor.close();

                appDb.closeDB();
            }
        }
        if(count > 0)
            return true;

        return false ;
    }
}
