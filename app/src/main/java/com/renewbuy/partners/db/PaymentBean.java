package com.renewbuy.partners.db;

import java.util.Date;

/**
 * Created by devlp on 12/10/15.
 */
public class PaymentBean {
    String customer_name;
    String customer_email;
    String customer_mobile;
    String customer_vehical_name;
    int customer_document_status;
    Date customer_prev_policy_expiry;
    double customer_earned, customer_total_premium, customer_odpremium;
    Integer vehicle_id;
    String policyno;
    String insurer;

    int payment_id ;


    String docUrl="";

    public String getDocUrl() {
        return docUrl;
    }

    public void setDocUrl(String docUrl) {
        this.docUrl = docUrl;
    }


    public int getPayment_id() {
        return payment_id;
    }

    public void setPayment_id(int payment_id) {
        this.payment_id = payment_id;
    }



    public Integer getVehicle_id() {
        return vehicle_id;
    }

    public void setVehicle_id(Integer vehicle_id) {
        this.vehicle_id = vehicle_id;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getCustomer_email() {
        return customer_email;
    }

    public void setCustomer_email(String customer_email) {
        this.customer_email = customer_email;
    }

    public String getCustomer_mobile() {
        return customer_mobile;
    }

    public void setCustomer_mobile(String customer_mobile) {
        this.customer_mobile = customer_mobile;
    }

    public String getCustomer_vehical_name() {
        return customer_vehical_name;
    }

    public void setCustomer_vehical_name(String customer_vehical_name_number) {
        this.customer_vehical_name = customer_vehical_name_number;
    }

    public int getCustomer_document_status() {
        return customer_document_status;
    }

    public void setCustomer_document_status(int customer_document_status) {
        this.customer_document_status = customer_document_status;
    }

    public Date getCustomer_prev_policy_expiry() {
        return customer_prev_policy_expiry;
    }

    public void setCustomer_prev_policy_expiry(Date customer_prev_policy_expiry) {
        this.customer_prev_policy_expiry = customer_prev_policy_expiry;
    }

    public double getCustomer_earned() {
        return customer_earned;
    }

    public void setCustomer_earned(double earned) {
        this.customer_earned = earned;
    }

    public double getCustomer_total_premium() {
        return customer_total_premium;
    }

    public void setCustomer_total_premium(double customer_total_premium) {
        this.customer_total_premium = customer_total_premium;
    }

    public double getOdpremium() {
        return customer_odpremium;
    }

    public void setOdpremium(double odpremium) {
        this.customer_odpremium = odpremium;
    }

    public String getPolicyNo() {
        return policyno;
    }

    public void setPolicyNo(String policyno) {
        this.policyno = policyno;
    }

    public String getInsurer() {
        return insurer;
    }

    public void setInsurer(String insurer) {
        this.insurer = insurer;
    }
}
