package com.renewbuy.partners;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

//import com.android.pluto.LocationFinder;

public class MaxLifeFragment extends Fragment {

    View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_max_life, container, false);


        final EditText max_code = (EditText) view.findViewById(R.id.max_code);


        final TextView by_register = (TextView) view.findViewById(R.id.mlRegisterText);

        by_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                Bundle fragArgs1 = new Bundle();
                fragArgs1.putString("pageUrl", "https://www.renewbuy.com/p/vahan-tnc");

                Fragment terms_fragment = new StaticPageFragment();
                terms_fragment.setArguments(fragArgs1);

                fragmentTransaction.addToBackStack("registerTermsConditions");
                fragmentTransaction.replace(R.id.main_frame, terms_fragment).commit();*/
            }
        });

        String[] rmNameList = new String[]{"Select", "Rahul Rai", "Vivek Singh", "Pooja Singh", "Gagan Chandra", "Others"};
        final Spinner rm_name = (Spinner) view.findViewById(R.id.rm_name);

        ArrayAdapter<String> nameAdapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_spinner_dropdown_item, rmNameList);
        nameAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        rm_name.setAdapter(nameAdapter);

        final CheckBox agreeTerms = (CheckBox) view.findViewById(R.id.mlAgreeTerms);

        Button btnSubmit = (Button) view.findViewById(R.id.maxFileSubmit);

        btnSubmit.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String code = max_code.getText().toString();
                /*if(rm_name.getSelectedItemId()==0){
                    Utils.displayAlert("Error", "Please select name.");
                    return;
                }else */
                if(rm_name.getSelectedItemPosition()==0)
                {
                    Utils.displayAlert(getActivity(),"Warning", "Please select valid RM Name");
                    return;
                }else if(code==null || code.trim().isEmpty())
                {
                    max_code.setError("Please enter your max code");
                    return;
                } else if (!agreeTerms.isChecked())
                {
                    Utils.displayAlert(getActivity(),"Error", "Please agree to the terms & conditions before proceeding.");
                    return;
                }
                // Get the Username in Fr_RegisterNextStep
                //Utils.saveDataToPreferences(getActivity(), "MaxName", PartnersRegistrationFragment.full_name.getText().toString());
                Utils.saveDataToPreferences(getActivity(), Utils.utmSourceKey, "MaxOne");
                if (rm_name.getSelectedItemPosition() != 0)
                Utils.saveDataToPreferences(getActivity(), Utils.utmMediumKey, rm_name.getSelectedItem().toString());
                Utils.saveDataToPreferences(getActivity(), Utils.utmTermKey, code);

                String partnerCode = getArguments().getString("partner_code");
                Fr_RegisterNextStep.register(partnerCode, Fr_RegisterNextStep.edt_pin_code.getText().toString());

                //getActivity().onBackPressed();

            }
        });

        return view;
    }

    boolean validate(String city){
        if( (city.length()==6) && (city.matches("[0-9]+") ) )
            return true;

        return false;
    }
}
