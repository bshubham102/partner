package com.renewbuy.partners;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.renewbuy.mercury.Utility;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegisterFragment extends Fragment {

    private View view ;

    public static EditText password, full_name, email, mobileNo, partnerCode;
    private Button registerButton ;

    private TextInputLayout name_layout, email_layout, password_layout, mobile_layout;

    private TextView register_show_pwd;

    public static String postalCode="";

    public static String emailid="";
    public static String mobile="";
    public static String pwd="";

    public static String partnerCodeText="";

    Fr_RegisterNextStep fr_registerNextStep ;

    public RegisterFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_register, container, false);

        //Intialize View
        init(view);

        //Set the Primary Email to TextView
        String primaryEmail = Utils.getPrimaryEmail(getActivity());
        if (!primaryEmail.isEmpty())
            email.setText(primaryEmail);

        String pCode = Utils.getStringFromPreferences(getActivity(),Utils.partnerCodeKey);
        if(!pCode.isEmpty()) {
            partnerCode.setText(pCode);
            partnerCode.setEnabled(false);
        }

        // On Register Button Clicked.
        registerButton.setOnClickListener(new MyClickListener());

        return view ;
    }

    private void init(View view) {
        full_name   = (EditText) view.findViewById(R.id.full_name);
        password    = (EditText) view.findViewById(R.id.password);
        email       = (EditText) view.findViewById(R.id.email);
        mobileNo    = (EditText) view.findViewById(R.id.mobile);
        partnerCode = (EditText) view.findViewById(R.id.partnerCode);


        //Initalize the layout for showing error
        name_layout     = (TextInputLayout) view.findViewById(R.id.input_layout_name);
        email_layout    = (TextInputLayout) view.findViewById(R.id.input_layout_email);
        password_layout = (TextInputLayout) view.findViewById(R.id.input_layout_password);
        mobile_layout   = (TextInputLayout) view.findViewById(R.id.input_layout_mobile);


        // Initialize the submit button
        registerButton = (Button) view.findViewById(R.id.register);
    }

    private class MyClickListener implements View.OnClickListener{
        @Override
        public void onClick(View v) {

            partnerCodeText = partnerCode.getText().toString().trim();

            if (Utils.isRepeatedClicks("registerSubmitButton")) {
                return;
            }

            String first_name = full_name.getText().toString().trim();

            if(first_name.length() == 0){
                setError(name_layout,"Name should not be empty");
                return ;
            }/*else if(!Utility.validateString(first_name,"[A-Za-z]+")){
                setError(name_layout,"Name contains only alphabet");
                return ;
            }*/else {
                hideError(name_layout);
            }

            if(email.getText().toString().equals(""))
            {
                email_layout.setError("Email should not be empty");
                email.requestFocus();
                return;
            }

            if (!Utils.isValidEmailId(email.getText().toString())) {
                setError(email_layout,"Please enter valid Email Id");
                return;
            } else {
                hideError(email_layout);
            }

            String mobile_no = mobileNo.getText().toString() ;

            if (mobile_no.length() <= 0) {
                setError(mobile_layout,"Mobile number should not be empty");
                return ;
            }else if (mobile_no.contains(" ")) {
                setError(mobile_layout,"Space not allowed in mobile number");
                return ;
            }else if (!Utility.validateString(mobile_no,"[0-9]+")) {
                setError(mobile_layout,"Please enter valid mobile number!");
                return ;
            }else if (!Utility.validateString(mobile_no,"^[789]\\d+")) {
                setError(mobile_layout,"Mobile number should start with 7,8 or 9");
                return ;
            } else {
                hideError(mobile_layout);
            }

            if (password.getText().toString().isEmpty())  {
                setError(password_layout,"Password should not be empty!");
                return ;
            } else if (password.getText().toString().length() < 6){
                setError(password_layout,"Password should be more than 6 character");
                return ;
            } else {
                hideError(password_layout);
                if (fr_registerNextStep == null)
                    fr_registerNextStep = new Fr_RegisterNextStep();

                if (fr_registerNextStep.isAdded())
                    return;
                else {
                    Utils.hideKeyboard(getActivity());

                    Bundle fragArgs = new Bundle();
                    fragArgs.putString("partner_code", partnerCodeText);
                    FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                    fr_registerNextStep.setArguments(fragArgs);
                    fragmentTransaction.replace(R.id.frag_container, fr_registerNextStep);
                    fragmentTransaction.addToBackStack("NextStep");
                    fragmentTransaction.commitAllowingStateLoss();
                }
            }
        }
    }

    private void setError(TextInputLayout layout , String msg){
        layout.setErrorEnabled(true);
        layout.setError(msg);
        layout.requestFocus();
    }
    private void hideError(TextInputLayout layout){
        layout.setError(null);
        layout.clearFocus();
        layout.setErrorEnabled(false);
    }

}
