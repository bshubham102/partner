package com.renewbuy.partners;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.renewbuy.partners.Sales.MissedTransaction;
import com.renewbuy.partners.Sales.PurchasedTransaction;
import com.renewbuy.partners.Sales.RejectedTransaction;
import com.renewbuy.partners.Sales.SubmitTransaction;

import java.util.ArrayList;
import java.util.List;


public class TransactionFragment extends Fragment {

    private int[] tabTitle = {
            R.string.submitted,
            R.string.missed,
            R.string.rejected,
            R.string.purchased/*,
            R.string.renewals*/
    };

    private Fragment[] fragList = {
            new SubmitTransaction(),
            new MissedTransaction(),
            new RejectedTransaction(),
            new PurchasedTransaction()
    };

    private Context context;
    private FragmentManager fragmentManager ;
    private TabLayout tabLayout ;
    private ViewPager viewPager ;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_transaction, container, false);

        // Get reference of Parent Activity
        context = getActivity();
        fragmentManager = getChildFragmentManager();
        init(view);
        return view ;
    }

    private void init(View view) {
        viewPager = (ViewPager) view.findViewById(R.id.transaction_viewPager);
        viewPager.setOffscreenPageLimit(fragList.length);
        setUpPager();

        tabLayout = (TabLayout) view.findViewById(R.id.transaction_tab_layout);
        tabLayout.setupWithViewPager(viewPager);
        setUpTab();

    }

    private void setUpTab() {
        invalidateTabMenu();
        tabLayout.getTabAt(0).getCustomView().setSelected(true);
    }

    private void setUpPager() {
        PagerAdapter adapter = new PagerAdapter(fragmentManager);
        for (int i = 0; i <=fragList.length-1 ; i++) {
            adapter.addFrag(fragList[i],getString(tabTitle[i]));
        }
        viewPager.setAdapter(adapter);
        viewPager.setOnPageChangeListener(new PageListener());
    }

    //ViewPager Adapter class
    private class PagerAdapter extends FragmentStatePagerAdapter {

        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitle = new ArrayList<>();

        public PagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment , String title) {
            mFragmentList.add(fragment);
            mFragmentTitle.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitle.get(position);
        }
    }

    // ViewPager Page Change Listener
    private class PageListener implements ViewPager.OnPageChangeListener{
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        }

        @Override
        public void onPageSelected(int position) {
            if( Utils.indexTransactionNotfication == position && Utils.hasRefferalsNotfication ) {

                Utils.hasRefferalsNotfication = false;
                Utils.indexTransactionNotfication = -1;

                LinearLayout linearLayout = (LinearLayout) tabLayout.getTabAt(position).getCustomView();
                RelativeLayout relativeLayout = (RelativeLayout) linearLayout.getChildAt(0);
                ImageView counter = (ImageView) relativeLayout.getChildAt(1);
                counter.setVisibility(View.GONE);

                if( MainActivity.counter != null)
                    MainActivity.counter.setVisibility(View.GONE);

            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public void invalidateTabMenu() {

        for (int i = 0; i <= fragList.length -1 ; i++) {
            LinearLayout layout = (LinearLayout) LayoutInflater.from(context).inflate(R.layout.trans_custom_tab, null);

            TextView  title   = (TextView)  layout.findViewById(R.id.trans_tab_title);
            ImageView counter = (ImageView) layout.findViewById(R.id.trans_tab_counter);

            title.setText(tabTitle[i]);

            if(Utils.hasRefferalsNotfication && Utils.indexTransactionNotfication == i)
                counter.setVisibility(View.VISIBLE);

            tabLayout.getTabAt(i).setCustomView(layout);
        }
    }
}
