package com.renewbuy.partners.modelClass;

/**
 * Created by shubham on 30/8/16.
 */
public class DrawerModel {
    private int ResourceId;
    private String Title ;

    public DrawerModel(int resourceId, String title) {
        ResourceId = resourceId;
        Title = title;
    }

    public int getResourceId() {
        return ResourceId;
    }

    public void setResourceId(int resourceId) {
        ResourceId = resourceId;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }
}
