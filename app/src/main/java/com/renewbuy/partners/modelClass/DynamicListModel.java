package com.renewbuy.partners.modelClass;

/**
 * Created by shubham on 16/8/16.
 */
public class DynamicListModel {
    private int id ;
    private String Name ;

    public DynamicListModel() {
    }

    public DynamicListModel(int id, String name) {
        this.id = id;
        Name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }
}
