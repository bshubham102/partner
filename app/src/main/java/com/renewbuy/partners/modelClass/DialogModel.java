package com.renewbuy.partners.modelClass;

/**
 * Created by shubham on 13/9/16.
 */
public class DialogModel {
    String title ;
    int iconResourceID ;

    public DialogModel(String title, int iconResourceID) {
        this.title = title;
        this.iconResourceID = iconResourceID;
    }

    public String getTitle() {
        return title;
    }

    public int getIconResourceID() {
        return iconResourceID;
    }
}
