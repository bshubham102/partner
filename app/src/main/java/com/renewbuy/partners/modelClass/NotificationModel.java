package com.renewbuy.partners.modelClass;

import android.graphics.Bitmap;

/**
 * Created by shubham on 16/5/16.
 */
public class NotificationModel {

    private String id;
    private String title;
    private String recipient;
    private String level;
    private String timestamp;
    private String icon;
    private String onclick_Action;
    private String description;
    private String expiry_timestamp;


    private String email ;
    private boolean unread ;
    private Bitmap bitmap;


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getOnclick_Action() {
        return onclick_Action;
    }

    public void setOnclick_Action(String onclick_Action) {
        this.onclick_Action = onclick_Action;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getExpiry_timestamp() {
        return expiry_timestamp;
    }

    public void setExpiry_timestamp(String expiry_timestamp) {
        this.expiry_timestamp = expiry_timestamp;
    }

    public boolean isUnread() {
        return unread;
    }

    public void setUnread(boolean unread) {
        this.unread = unread;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
