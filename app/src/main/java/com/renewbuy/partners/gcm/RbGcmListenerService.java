package com.renewbuy.partners.gcm;

/**
 * Created by sandeep on 4/6/15.
 */

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.text.Html;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.gcm.GcmListenerService;
import com.renewbuy.partners.Config;
import com.renewbuy.partners.MainActivity;
import com.renewbuy.partners.MultiUseActivity;
import com.renewbuy.partners.R;
import com.renewbuy.partners.SplashScreen;
import com.renewbuy.partners.Utils;
import com.renewbuy.partners.db.AppDb;
import com.renewbuy.partners.db.NotificationTable;
import com.renewbuy.partners.modelClass.NotificationModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

public class RbGcmListenerService extends GcmListenerService {

    private static final String TAG = "RbGcmListenerService";
    private static final int EXPIRY_DAYS = 30;

    private String id, email ,level, expiry, onclickAction, icon;

    /**
     * Called when message is received.
     *
     * @param from SenderID of the sender.
     * @param data Data bundle containing message data as key/value pairs.
     *             For Set of keys use data.keySet().
     */
    // [START receive_message]
    @RequiresApi(api = Build.VERSION_CODES.HONEYCOMB)
    @Override
    public void onMessageReceived(String from, Bundle data) {
        Log.d(TAG, "From: " + from);

        String messageType = data.getString(Config.GCM_KEY_MESSAGE_TYPE);

        if (messageType.contains(Config.TYPE_NOTIFICATION)) {

            id  = data.getString(Config.GCM_KEY_ID);
            final String body = data.getString(Config.GCM_KEY_BODY);
            final String title = data.getString(Config.GCM_KEY_TITLE);

            String small_body_1 = data.getString(Config.GCM_KEY_SMALL_BODY_1);
            String small_body_2 = data.getString(Config.GCM_KEY_SMALL_BODY_2);

            final String tab = data.getString(Config.GCM_KEY_TAB);

            level = data.getString(Config.GCM_KEY_LEVEL);
            expiry = data.getString(Config.GCM_KEY_EXPIRY_TIMESTAMP);
            onclickAction = data.getString(Config.GCM_KEY_ONCLICK_ACTION);
            icon = data.getString(Config.GCM_KEY_ICON);

            showNotification(title, body, small_body_1, small_body_2, tab, messageType);
        }

    }
    // [END receive_message]

    /**
     * Create and show a simple notification containing the received GCM message.
     *
     * @param message M message received.
     */
    @RequiresApi(api = Build.VERSION_CODES.HONEYCOMB)
    private void showNotification(String title, String message, String small_body_1, String small_body_2, String tab, String messageType) {
        saveNotification(title, message, small_body_1, small_body_2, tab, messageType);

        message = Html.fromHtml(message).toString();

        Intent intent = null;
        if(!onclickAction.equals(" ") && !onclickAction.isEmpty()){
            intent = Utils.getClickAction(onclickAction);
        } else {
            if (MultiUseActivity.instance != null) {
                intent = new Intent(this, MultiUseActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(MultiUseActivity.SELECT_NOTIFICATION, MultiUseActivity.SELECT_NOTIFICATION);
                bundle.putString(MultiUseActivity.TITLE, "Notifications");
                intent.putExtras(bundle);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            }else if (MainActivity.activity != null) {
                intent = new Intent(this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            } else {
                intent = new Intent(this, SplashScreen.class);
            }
        }
        intent.putExtra(Utils.notificationTab, tab);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        if (small_body_1 == null || small_body_1.trim().equals("")) {
            small_body_1 = message;
            small_body_2 = null;
        }


        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(),R.drawable.icon))
                .setSmallIcon(R.drawable.notification_icon,2)
                .setColor(getResources().getColor(R.color.light_blue))
                .setContentTitle(title)
                .setContentText(small_body_1)
                .setSubText(small_body_2)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(message)
                        .setSummaryText(null))
                .setContentIntent(pendingIntent)
                .setPriority(Notification.PRIORITY_MAX);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);


        Random r = new Random();
        notificationManager.notify(r.nextInt(1000) /* ID of notification */, notificationBuilder.build());
    }

    void saveNotification(String title, String body, String small_body_1, String small_body_2, String tab, String messgaeType){
        Date date = Calendar.getInstance().getTime();
        Date expiryTimStamp = null;

        email = Utils.getStringFromPreferences(this,Utils.emailKey);

        AppDb db = AppDb.getInstance(getBaseContext());
        NotificationTable notificationTable = (NotificationTable) db.getNotificationTableObject(NotificationTable.TABLE_NAME);

        if (id == null || id.equals("") || id.length() <= 0)
            id = " ";

        if (level ==null || level.equals("") || level.length() <= 0)
            level = "1";

        if(onclickAction == null || onclickAction.equals("") || onclickAction.length() <= 0)
             onclickAction = " ";

        if (expiry ==null || expiry.equals("") || expiry.length() <= 0 || expiry.equals("None")) {
            expiryTimStamp = Utils.getAfterDaysDate(EXPIRY_DAYS);
        } else{
            String[] split = expiry.split("T");
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            try {
                expiryTimStamp = formatter.parse(split[0]);
            } catch (ParseException e) {
                e.printStackTrace();
                Log.i(TAG, "saveNotification: expiryTimestamp exception");
                expiryTimStamp = Utils.getAfterDaysDate(EXPIRY_DAYS);
            }
        }


        final NotificationModel bean = new NotificationModel();

        bean.setEmail(Utils.getStringFromPreferences(this,Utils.emailKey));
        bean.setTitle(title);
        bean.setId(id);
        bean.setDescription(body);
        bean.setIcon(icon);
        bean.setOnclick_Action(onclickAction);
        bean.setUnread(true);
        bean.setTimestamp(String.valueOf(Utils.convertDateToEpoch(date)));
        bean.setLevel(level);
        bean.setExpiry_timestamp(String.valueOf(Utils.convertDateToEpoch(expiryTimStamp)));
        notificationTable.insertData(bean);

        if(MultiUseActivity.instance != null) {
            MultiUseActivity.instance.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (com.renewbuy.partners.Notification.instance != null) {
                        com.renewbuy.partners.Notification.notificationModelList.add(0, bean);
                        com.renewbuy.partners.Notification.adapter.notifyDataSetChanged();
                    } else{
                        MainActivity.activity.incrementNotificationCount();
                    }
                }
            });
        } else{
            if(MainActivity.activity != null)
                MainActivity.activity.incrementNotificationCount();
        }
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        Toast.makeText(this,"in service", Toast.LENGTH_LONG).show();
    }
}