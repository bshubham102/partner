package com.renewbuy.partners.gcm;

import android.content.Context;
import android.os.Build;
import android.telephony.TelephonyManager;

import com.renewbuy.partners.Api;
import com.renewbuy.partners.Config;
import com.renewbuy.partners.IntroActivity;
import com.renewbuy.partners.Utils;

import java.util.HashMap;

/**
 * Created by shubham on 27/6/16.
 */
public class DeviceDetails {

    Context context;
    String deviceId  , RESOURCES_SDK , PRODUCT ,BRAND ,IMEI , GCM_ID ;
    int  APPVER;
    IntroActivity homeActivity ;


    public DeviceDetails(Context context ) {
        this.context = context;
        homeActivity = IntroActivity.activity ;

        deviceId = Utils.getDeviceId(context) ;
        RESOURCES_SDK = Build.VERSION.RELEASE ;
        PRODUCT = Build.PRODUCT;
        BRAND = Build.BRAND ;
        if( Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP_MR1) {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            IMEI = telephonyManager.getDeviceId();
        }
        APPVER =  Utils.getVersionCode(context);
        this.GCM_ID = Utils.getStringFromPreferences(context, Config.GCM_GENERATED_ID);

        // Call the API
        Api.updateUser(context, getDetail());
    }

    public HashMap<String,String> getDetail(){
        HashMap<String,String> map = new HashMap<String,String>();

        map.put("platform","2");
        map.put("app","1");
        map.put("device_id",deviceId);

        if(GCM_ID != null)
          map.put("gcm_id", GCM_ID);

        if( IMEI != null)
            map.put("imei", IMEI);

        map.put("os_version", RESOURCES_SDK);
        map.put("model", PRODUCT);
        map.put("manufacturer", BRAND);
        map.put("is_last_launched", "true");
        map.put("is_active","true");
        map.put("app_version", String.valueOf(APPVER));

        return map ;
    }
}
