package com.renewbuy.partners.gcm;

import android.content.Intent;

import com.google.android.gms.iid.InstanceIDListenerService;
import com.renewbuy.partners.Config;
import com.renewbuy.partners.Utils;

/**
 * Created by sandeep on 8/6/15.
 */
public class IIDListenerService extends InstanceIDListenerService {
    private static final String TAG = "InstanceIDService";

    @Override
    public void onTokenRefresh() {
        // Fetch updated Instance ID token and notify our app's server of any changes (if applicable).
        Utils.saveDataToPreferences(getApplicationContext(), Config.GCM_TOKEN_SENT_TO_SERVER, false);

        Intent intent = new Intent(this, RegistrationIntentService.class);
        startService(intent);
    }
}