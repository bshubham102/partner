package com.renewbuy.partners;


import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.renewbuy.mercury.models.FinanceModel;


/**
 * A simple {@link Fragment} subclass.
 */
public class BankDetail extends Fragment {
    
    private View view ;
    private Context context ;
    private TextView detail_txt ;
    private TextInputLayout namelayout ,numberlayout ,ifsclayout , panlayout ;
    private EditText accountNameField, accountNumberField, ifscCodeField, panNo ;
    private Button btn_bank_detail ;
    private LinearLayout callLayout ;
    private RelativeLayout detailsLayout ;
    public static BankDetail instance ;

    private boolean hasDetails ;


    public BankDetail() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_bank_detail, container, false);
        
        //get the refernce of Parent Activity
        context = getActivity() ;
        
        //Intialize Views 
        init(view);

        boolean b = Utils.getBooleanFromPreferences(context,Utils.hasAllAccountDetailsFlag) ;
        String appendMsg ;
        if(b)
            appendMsg = "Update "+detail_txt.getText();
        else
            appendMsg = "Add "+ detail_txt.getText();

        detail_txt.setText(appendMsg);

        // set Data to fields
        setData();

        instance = this ;
        return view ;
    }

    public void setData() {
        String holderName = Utils.getStringFromPreferences(context,Utils.accountNameKey);
        String accountNumber = Utils.getStringFromPreferences(context,Utils.accountNumberKey) ;
        String ifscNumber = Utils.getStringFromPreferences(context,Utils.ifscCodeKey);
        String PanCard = Utils.getStringFromPreferences(context,Utils.panNoKey);

        if(holderName != null && holderName.length() > 0) {
            accountNameField.setText(holderName);
            hasDetails = true ;
        }

        if(accountNumber != null && accountNumber.length() > 0)
            accountNumberField.setText(accountNumber);

        if(ifscNumber != null && ifscNumber.length() > 0)
            ifscCodeField.setText(ifscNumber);

        if(PanCard != null&& PanCard.length() > 0)
            panNo.setText(PanCard);

    }

    private void disableEditText(boolean b) {
        accountNameField.setEnabled(b);
        accountNumberField.setEnabled(b);
        ifscCodeField.setEnabled(b);
        panNo.setEnabled(b);
    }

    private void init(View view) {
        detail_txt = (TextView) view.findViewById(R.id.details_text);

        detailsLayout = (RelativeLayout) view.findViewById(R.id.update_details_layout);
        callLayout = (LinearLayout) view.findViewById(R.id.submit_call);

        namelayout = (TextInputLayout) view.findViewById(R.id.input_layout_acc_name);
        numberlayout = (TextInputLayout) view.findViewById(R.id.input_layout_acc_number);
        ifsclayout = (TextInputLayout) view.findViewById(R.id.input_layout_acc_ifsc);
        panlayout = (TextInputLayout) view.findViewById(R.id.input_layout_acc_pan);

        accountNameField = (EditText) view.findViewById(R.id.input_account_name);
        accountNumberField = (EditText) view.findViewById(R.id.input_account_number);
        ifscCodeField = (EditText) view.findViewById(R.id.input_account_ifsc);
        panNo = (EditText) view.findViewById(R.id.input_account_pan_number);
        btn_bank_detail = (Button) view.findViewById(R.id.btn_details_submit);

        setListener();
    }

    private void setListener() {
        btn_bank_detail.setOnClickListener(new MYClickListener());
        callLayout.setOnClickListener(new MYClickListener());
    }

    private class MYClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btn_details_submit:
                     if(validateFields()){
                         // Form the data structure
                         /*HashMap<String, String> params = new HashMap<String, String>();
                         params.put("ac_name", accountNameField.getText().toString().trim().toUpperCase());
                         params.put("ac_number", accountNumberField.getText().toString().trim().toUpperCase());
                         params.put("ac_ifsc_code", ifscCodeField.getText().toString().trim().toUpperCase());
                         params.put("pan_number", panNo.getText().toString().trim().toUpperCase());*/

                         String name = accountNameField.getText().toString().trim().toUpperCase() ;
                         String number = accountNumberField.getText().toString().trim().toUpperCase() ;
                         String ifsc  = ifscCodeField.getText().toString().trim().toUpperCase() ;
                         String pan =  panNo.getText().toString().trim().toUpperCase() ;

                         Utils.saveBankDetailsInSettings(MultiUseActivity.instance,name,number,ifsc,pan);

                         FinanceModel model = new FinanceModel(name,number,null,null,ifsc,pan);

                         // Call the API
                         Utils.hideKeyboard(context);
                         Api.updateExecutive(getActivity(), model);
                     }
                break;

                case R.id.submit_call:
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:"+context.getResources().getString(R.string.rb_toll_free)));
                    context.startActivity(intent);
                break ;
            }
        }
    }

    private Boolean validateFields() {
        EditText[] fields = {accountNameField, accountNumberField};
        TextInputLayout[] layout = {namelayout,numberlayout} ;
        int i=0 ;
        for (EditText field : fields) {
            String value = field.getText().toString().trim();
            if (value.length() <= 3) {
                setError(layout[i],"Please enter a valid value");
                return false;
            } else {
                hideError(layout[i]);
            }
            i=i++;
        }

        if (ifscCodeField.getText().toString().trim().length() < 11) {
            setError(ifsclayout,"Please enter a valid IFSC Code");
            return false;
        }else{
            hideError(ifsclayout);
        }

        if (panNo.getText().toString().trim().length() != 10) {
            setError(panlayout,"Please enter a valid Pan Number");
            return false;
        }else{
            hideError(panlayout);
        }

        return true;
    }


    private void setError(TextInputLayout layout , String msg){
        layout.setErrorEnabled(true);
        layout.setError(msg);
        layout.requestFocus();
    }
    private void hideError(TextInputLayout layout){
        layout.setError(null);
        layout.clearFocus();
        layout.setErrorEnabled(false);
    }
}
