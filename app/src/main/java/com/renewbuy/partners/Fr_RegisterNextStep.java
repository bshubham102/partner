package com.renewbuy.partners;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.renewbuy.mercury.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by d2c3 on 14/4/15.
 */

public class Fr_RegisterNextStep extends Fragment {

	private MaxLifeFragment maxLifeFragment = null;
	static IntroActivity context;
	static FragmentManager fm;

    RadioGroup rg1;

    private String partner_code;

    private LinearLayout terms_layout ;

    public static String postalCode="";
    private ArrayList<String> carName,  carId;
    public String id;
    public TextView txt_month, txt_year, txt_date, txt_anivermonth, txt_aniveryear,  txt_aniverdate;
    public static EditText edt_pin_code, edt_city, edt_business_detail;

    private TextView genderError, birthError, AniverseryError ;
    private TextInputLayout addressLayout, pinCodeLayout ;

    public Button but_register;
    public String dobDate="", dobMonth="",  dobYear="", pinCode="";

    public static String aniversisrybDate="";
    public static String aniversiryMonth="";
    public static String anversiryYear="";
    public static int    maleFemale = 1 ;
    public static String manageDate="";
    public static String manageAniDate="";
    private boolean isVahanPartner;


    @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fr_register_next_step, container, false);

        context = (IntroActivity) getActivity();
        fm = getFragmentManager();

        //get PArtner Code ;
        savedInstanceState = getArguments();
        if(savedInstanceState != null)
            partner_code = savedInstanceState.getString("partner_code");

        carName = new ArrayList<String>();
        carId   = new ArrayList<String>();

        txt_month=(TextView)view.findViewById(R.id.txt_month);
        txt_year=(TextView)view.findViewById(R.id.txt_year);
        txt_date=(TextView)view.findViewById(R.id.txt_date);

        //Intialize the error textview
        genderError = (TextView) view.findViewById(R.id.txt_gender_error);
        birthError = (TextView) view.findViewById(R.id.txt_dob_error);
        AniverseryError = (TextView) view.findViewById(R.id.txt_marriage_error);

        //intialize the textinputlayout
        addressLayout = (TextInputLayout) view.findViewById(R.id.input_layout_business);
        pinCodeLayout = (TextInputLayout) view.findViewById(R.id.input_layout_pin_code);


        txt_aniverdate=(TextView)view.findViewById(R.id.txt_aniverdate);
        txt_aniveryear=(TextView)view.findViewById(R.id.txt_aniveryear);
        txt_anivermonth=(TextView)view.findViewById(R.id.txt_anivermonth);

        edt_pin_code=(EditText)view.findViewById(R.id.txt_pin_code);
        edt_business_detail=(EditText)view.findViewById(R.id.edt_business_detail);

        rg1 = (RadioGroup) view.findViewById(R.id.rg1);

        terms_layout = (LinearLayout) view.findViewById(R.id.terms_layout);

        final CheckBox agreeTerms = (CheckBox)view.findViewById(R.id.cbAgreeTerms);
        final TextView by_register=(TextView)view.findViewById(R.id.registerText);

        but_register=(Button)view.findViewById(R.id.register);

        if(partner_code != null && !partner_code.equals("")) {
            if(partner_code.equalsIgnoreCase("vahan") || partner_code.toLowerCase().startsWith("vhn")) {
                isVahanPartner = true;
                terms_layout.setVisibility(View.GONE);
                but_register.setText(context.getResources().getString(R.string.next));
            }else{
                isVahanPartner = false;
            }
        }else{
            isVahanPartner = false ;
        }

        if(RegisterFragment.postalCode != null && RegisterFragment.postalCode.length()>0) {
            edt_pin_code.setText(RegisterFragment.postalCode);
        }

        rg1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // find which radio button is selected
                if(checkedId == R.id.radio_male) {
                    maleFemale=1;
                } else if(checkedId == R.id.radio_female) {
                    maleFemale=2;
                }
            }

        });

        but_register.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v) {
                if(maleFemale == 0) {
                    genderError.setVisibility(View.VISIBLE);
                    return ;
                } else{
                    genderError.setVisibility(View.GONE);
                }

                if(dobDate.length()==0) {
                    setError(birthError,"Please Enter Date of Birth");
                    return ;
                } else if(dobMonth.length()==0) {
                    setError(birthError,"Please Enter Month of Birth");
                    return ;
                } else if(dobYear.length()==0) {
                    setError(birthError,"Please Enter Year of Birth");
                    return ;
                } else if(!Utils.isValidFormat("yyyy-M-d",dobYear+"-"+dobMonth+"-"+dobDate)){
                    setError(birthError,"DOB is not correct");
                    return;
                } else{
                    hideError(birthError);
                }


                String business = edt_business_detail.getText().toString().trim();
                if(business.length()==0) {
                    setError(addressLayout,"Please enter business address");
                    return ;
                } else {
                    hideError(addressLayout);
                }

                String pin = edt_pin_code.getText().toString().trim();
                if(pin.length()==0) {
                    setError(pinCodeLayout,"Please Enter pin code");
                    return ;
                } else if(!Utility.validateString(pin,"[0-9]+")){
                    setError(pinCodeLayout,"Please enter valid pin code");
                    return ;
                }else {
                    hideError(pinCodeLayout);
                }

                if (!isVahanPartner && !agreeTerms.isChecked()) {
                    Utils.displayAlert(getActivity(),"Error", "Please agree to the terms & conditions before proceeding");
                    return;
                }

                else {
                    if( !aniversisrybDate.isEmpty() && !aniversiryMonth.isEmpty() &&  !anversiryYear.isEmpty())
                    {
                        if(!Utils.isValidFormat("yyyy-M-d",anversiryYear+"-"+aniversiryMonth+"-"+aniversisrybDate))
                        {
                            Utils.displayAlert("Error", "DOA is not correct");
                            return;
                        }
                    }
                    manageDate=dobYear+"-"+dobMonth+"-"+dobDate;
                    manageAniDate=anversiryYear+"-"+aniversiryMonth+"-"+aniversisrybDate;
                    pinCode=edt_pin_code.getText().toString();

                    if (isVahanPartner) {
                        Utils.hideKeyboard(getActivity());

                        if (maxLifeFragment == null)
                            maxLifeFragment = new MaxLifeFragment();

                        if (maxLifeFragment.isAdded())
                            return;
                        else  {
                            Bundle fragArgs = new Bundle();
                            fragArgs.putString("partner_code", partner_code);
                            FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                            maxLifeFragment.setArguments(fragArgs);
                            fragmentTransaction.replace(R.id.frag_container, maxLifeFragment);
                            fragmentTransaction.addToBackStack("MaxLife");
                            fragmentTransaction.commitAllowingStateLoss();
                        }
                    } else {
                        Utils.hideKeyboard(getActivity());
                        register(RegisterFragment.partnerCode.getText().toString(), RegisterFragment.postalCode);
                    }
                }
            }
        });


       txt_month.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                carName.clear();
                carId.clear();

                // Change loop condition
                for(int i=1;i<=12;i++) {
                    carId.add(String.valueOf((i)));
                }

                carName.add("Jan");
                carName.add("Feb");
                carName.add("March");
                carName.add("April");
                carName.add("May");
                carName.add("June");
                carName.add("July");
                carName.add("Aug");
                carName.add("Sep");
                carName.add("Oct");
                carName.add("Nov");
                carName.add("Dec");


                String month="month";
                selectDropDown(getActivity(), txt_month, "Select Month", carName,month);

            }
       });

        txt_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                carName.clear();
                carId.clear();
                //Change loop condition on 7-may-2016
                for(int i=1;i<=31;i++)
                {
                    carId.add(String.valueOf((i)));
                }
                for(int i=1;i<=31;i++)
                {
                    carName.add(String.valueOf((i)));
                }

                String date="date";
               // whichOne="date";
                selectDropDown(getActivity(), txt_date, "Select Date", carName,date);

            }
        });

       txt_year.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                carName.clear();
                carId.clear();

                for(int i=15;i<115;i++)
                {

                    carName.add(String.valueOf(getPreviousYear(i)));
                    carId.add(String.valueOf(getPreviousYear(i)));

                }

                String year="year";
                selectDropDown(getActivity(), txt_year, "Select Year", carName,year);

            }
       });


        txt_aniverdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                carName.clear();
                carId.clear();
                for(int i=1;i<=31;i++)
                {
                    carId.add(String.valueOf((i)));
                }
                for(int i=1;i<=31;i++)
                {
                    carName.add(String.valueOf((i)));
                }

                 String aniDate="aniDate";

                selectDropDown(getActivity(), txt_aniverdate, "Select Date", carName,aniDate);

            }
        });

        txt_aniveryear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                carName.clear();
                carId.clear();

                for(int i=0;i<115;i++)
                {

                    carName.add(String.valueOf(getPreviousYear(i)));
                    carId.add(String.valueOf(getPreviousYear(i)));

                }
                String aniYear="aniYear";
                selectDropDown(getActivity(), txt_aniveryear, "Select Year", carName,aniYear);
            }
        });

        txt_anivermonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                carName.clear();
                carId.clear();
                for(int i=1;i<=12;i++)
                {
                    carId.add(String.valueOf((i)));

                }
                carName.add("Jan");
                carName.add("Feb");
                carName.add("March");
                carName.add("April");
                carName.add("May");
                carName.add("June");
                carName.add("July");
                carName.add("Aug");
                carName.add("Sep");
                carName.add("Oct");
                carName.add("Nov");
                carName.add("Dec");

                String aniMonth="aniMonth";
                selectDropDown(getActivity(), txt_anivermonth, "Select Month", carName,aniMonth);

            }
        });
		return view;
	}
    public void showAlert(Context ctx, String msg) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ctx);
        alertDialogBuilder.setTitle("Notification");

        // by shubham
        msg = msg.substring(0,1).toUpperCase() + msg.substring(1);

        alertDialogBuilder
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }

    private int getPreviousYear(int i) {
        Calendar prevYear = Calendar.getInstance();
        prevYear.add(Calendar.YEAR, -i);
        return prevYear.get(Calendar.YEAR);
    }
    private int getCurrentYear(int i) {
        Calendar prevYear = Calendar.getInstance();
        prevYear.add(Calendar.YEAR, 15);
        return prevYear.get(Calendar.YEAR);
    }


    public void selectDropDown(Context ctx, final TextView txt, String title, List<String> list, final String whichone){
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(ctx);

        // builderSingle.setIcon(R.drawable.ic_launcher);
        builderSingle.setTitle(title);
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(ctx,android.R.layout.select_dialog_singlechoice, list);


        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                String strName = arrayAdapter.getItem(which);
                //http://stackoverflow.com/questions/15762905/how-to-display-list-view-in-alert-dialog-in-android
                //tv_selectCategory.setText(strName);
                Log.e("111111111111", "111111111111111111  " + strName);
                id=carId.get(which);
                txt.setText(strName);
                if(whichone.equals("date"))
                {
                  dobDate=id;

                }else if(whichone.equals("month"))
                {
                  dobMonth=id;

                }else if(whichone.equals("year"))
                {
                   dobYear=id;
                }
                else if(whichone.equals("aniDate"))
                {
                    aniversisrybDate=id;
                }
                else if(whichone.equals("aniMonth"))
                {
                    aniversiryMonth=id;
                }
                else if(whichone.equals("aniYear"))
                {
                    anversiryYear=id;
                }

            }
        });
        builderSingle.show();
    }

    public static void register(String partnerCodeText, String city){
        String emailid=RegisterFragment.email.getText().toString();
        String mobile=RegisterFragment.mobileNo.getText().toString();
        String pwd=RegisterFragment.password.getText().toString();

        JSONObject params = new JSONObject();
        try {
            params.put("email", emailid);
            params.put("mobile", mobile);
            params.put("password", pwd);

            if (!partnerCodeText.isEmpty()) {
                params.put("partner_code", partnerCodeText);
            }

            params.put("is_executive", true);
            params.put("pin_code", edt_pin_code.getText().toString());

            String name = RegisterFragment.full_name.getText().toString();

            if(name != null) {
                if (name.contains(" ")) {
                    int i = name.lastIndexOf(" ");

                    params.put("first_name", name.substring(0, i));
                    params.put("last_name", name.substring(i + 1, name.length()));
                } else {
                    params.put("first_name", name);
                }
            }

            JSONObject addressObj=new JSONObject();

            addressObj.put("city", "");
            addressObj.put("pin_code", edt_pin_code.getText().toString());
            addressObj.put("address_line1", edt_business_detail.getText().toString());


            JSONObject profileObj=new JSONObject();
            profileObj.put("dob",  manageDate);
            profileObj.put("gender", maleFemale);


            if(!aniversisrybDate.isEmpty() && !aniversiryMonth.isEmpty() && !anversiryYear.isEmpty())
            {
                profileObj.put("doa", manageAniDate);
            }

            profileObj.put("primary_address",addressObj);

            params.put("profile", profileObj);


        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        // Make a JSON Rest Call.
        PartnersRegistrationHelper.createJsonRestCall(context, params);
    }

    @Override
    public void onResume() {
        super.onResume();
         getDatesBack();
    }

    public  void getDatesBack(){
        String[] str = {"Jan","Feb","March","April","May","June","July","Aug","Sep","Oct","Nov","Dec"};

        // Set the DOB when user  came from first screen after errror
        if(!dobDate.isEmpty() && !dobDate.equals("")){
            txt_date.setText(dobDate);
        }
        if(!dobMonth.isEmpty() && !dobMonth.equals("")){
            txt_month.setText(str[Integer.parseInt(dobMonth)-1]);
        }
        if(!dobYear.isEmpty() && !dobYear.equals("")){
            txt_year.setText(dobYear);
        }

        // Set the DOA  when user  came from Last  screen after error
        if(!aniversisrybDate.isEmpty() && !aniversisrybDate.equals("")){
            txt_aniverdate.setText(aniversisrybDate);
        }
        if(!aniversiryMonth.isEmpty() && !aniversiryMonth.equals("")){
            txt_anivermonth.setText(str[Integer.parseInt(aniversiryMonth)-1]);
        }
        if(!anversiryYear.isEmpty() && !anversiryYear.equals("")){
            txt_aniveryear.setText(anversiryYear);
        }

    }

    private void setError(TextInputLayout layout , String msg){
        layout.setErrorEnabled(true);
        layout.setError(msg);
        layout.requestFocus();
    }
    private void hideError(TextInputLayout layout){
        layout.setError(null);
        layout.clearFocus();
        layout.setErrorEnabled(false);
    }
    private void setError(TextView layout , String msg){
        layout.setText(msg);
        layout.setVisibility(View.VISIBLE);
    }
    private void hideError(TextView layout){
        layout.setVisibility(View.GONE);
    }
}