package com.renewbuy.partners;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * A simple {@link Fragment} subclass.
 */
public class Wallet extends Fragment {


    private TextView currentDate;
    private TextView totalpayout;

    public Wallet() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_wallet, container, false);

        //Intailize views
        init(view);

        //SetData
        setData();

        return view ;
    }

    private void init(View view) {

        currentDate = (TextView) view.findViewById(R.id.total_date);
        totalpayout = (TextView) view.findViewById(R.id.payout);
    }

    private void setData() {
        SimpleDateFormat format = new SimpleDateFormat("dd MMM, yyyy");
        Date date = new Date();
        currentDate.setText(currentDate.getText()+" "+format.format(date));

        totalpayout.setText(totalpayout.getText()+"5000/-");

    }

}
