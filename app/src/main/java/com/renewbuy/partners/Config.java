package com.renewbuy.partners;

/**
 * Created by sandeep on 4/6/15.
 */

public interface Config {

    static final String DOMAIN = Utils.DOMAIN ;

    static final String URL_CURRENT_USER = DOMAIN + "/api/v1/users/me/";
    static final String Device_URL       = DOMAIN + "/api/v1/devices/update_or_create/" ;
    static final String URL_SET_SOURCE   = DOMAIN + "/api/v1/users/%1$s/set_source/";
    static final String URL_EXECUTIVE_SET_PARTNER = DOMAIN + "/api/v1/executives/%1$s/set_partner/";
    static final String HEALTH_URL = DOMAIN + "" ;

    // Google project id
    static final String GOOGLE_SENDER_ID = "738093500899";

    static final String TAG = "RB Partners";

    /* GCM CONSTANTS */
    static final String GCM_KEY_MESSAGE_TYPE = "type";
    static final String GCM_KEY_TITLE = "title";
    static final String GCM_KEY_BODY = "body";
    static final String GCM_KEY_SMALL_BODY_1 = "small_body_1";
    static final String GCM_KEY_SMALL_BODY_2 = "small_body_2";
    static final String GCM_KEY_TAB = "tab";
    public final String GCM_KEY_ICON = "icon";
    static final String TYPE_NOTIFICATION = "SHOW_NOTIFICATION";
    static final String TYPE_OFFER="SHOW_OFFER";

    //Need Add More data in Notification same as MyWheels
    static final String GCM_KEY_ID = "id";
    static final String GCM_KEY_LEVEL = "level";
    static final String GCM_KEY_EXPIRY_TIMESTAMP = "expiry_timestamp";
    static final String GCM_KEY_ONCLICK_ACTION = "onclick_action";

    public  static  final String GCM_GENERATED_ID = "gcm_id";
    public static final String GCM_TOKEN_SENT_TO_SERVER = "sentGcmTokenToServer";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";

    public static final String TAB_CUSTOMERS = "MyCustomer";
    public static final String TAB_EARNINGS = "MyEarning";
    public static final String TAB_ISSUE_POLICY = "IssuePolicy";
    public static final String TAB_CAPTURE_POLICY = "CaptureTab";

   // WebView pages URL
   public static final String ABOUT_US  = "https://www.renewbuy.com/p/rb-partners/about-us/?source=app";
   public static final String FAQ  = "https://www.renewbuy.com/p/rb-partners/faqs/?source=app";
   public static final String TERMS_CONDITION  =  "https://www.renewbuy.com/p/rb-partners/terms-conditions/?source=app";
   public static final String PRIVACY_POLICY  =  "https://www.renewbuy.com/p/rb-partners/privacy-policy/?source=app";
   public static final String INSURE_CONTACT = "https://www.renewbuy.com/p/rb-partners/insurer-contacts/?source=app" ;
}