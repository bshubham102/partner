package com.renewbuy.partners;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by shubham on 31/8/16.
 */
public class FeedbackFragment extends Fragment {
    private MultiUseActivity activity;

    public static FeedbackFragment feedback_instance;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_feedback, container, false);
        activity = (MultiUseActivity) getActivity();
        feedback_instance = this;

        if (Utils.getAuthToken(activity) == null) {
            Toast.makeText(activity.getApplicationContext(),
                    "Please Log In", Toast.LENGTH_LONG).show();
        }

        final int rating = MainActivity.RATING;
        final EditText et_message = (EditText) view.findViewById(R.id.et_msg);
        Button btn_feedback_submit = (Button) view.findViewById(R.id.feedback_submit);
        btn_feedback_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(et_message.getText().toString().length() > 0 ) {
                    try {
                        Api.feedback(activity, rating, et_message);


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(activity, "Please enter your Feedback", Toast.LENGTH_SHORT).show();
                }
            }
        });
        return view;
    }
}
