package com.renewbuy.partners;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.CardView;
import android.transition.ChangeBounds;
import android.transition.ChangeImageTransform;
import android.transition.ChangeTransform;
import android.transition.TransitionSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Profile extends Fragment  {

    private View      view ;
    private Context   context ;
    private CardView  profileCard ;
    private TextView  name, email, phone, verify, accName, accNumber, accIFSC, accPan ;
    private ImageView profileImage ;
    private Button    changePassord , editBankDetails ;
    private LinearLayout fillLayout, emptyLayout ;

    public static Profile instance ;

    private FragmentManager fragmentManager ;
    private FragmentTransaction transaction;

    String username, useremail, contact, accountName, accountNumber, IFSC, Pancard ;

    public Profile() {
        // Required empty public constructor
    }

    @SuppressLint("CommitTransaction")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_profile, container, false);

        //Reference of Parent Activity
        context = getActivity() ;

        //Intialize the View
        init(view);

        //Intialize Fragment Mangers nd Transaction
        transaction = MultiUseActivity.instance.fragmentManager.beginTransaction();

        //Set Data
        setData();

        instance = this ;

        setListner() ;

        return view ;
    }

    private void init(View view) {

        // Image nad Name Section
        profileCard = (CardView) view.findViewById(R.id.profile_card);
        profileImage = (ImageView) view.findViewById(R.id.img_profile_img);
        name = (TextView) view.findViewById(R.id.txt_username);

        //Personal Details Section
        email  = (TextView) view.findViewById(R.id.txt_username_email);
        phone  = (TextView) view.findViewById(R.id.txt_username_contact);
        verify = (TextView) view.findViewById(R.id.txt_username_contact_verify);
        changePassord = (Button) view.findViewById(R.id.btn_username_password);

        //Bank Details Section
        editBankDetails = (Button) view.findViewById(R.id.btn_edit_bank_details);

        //Bank Details Layout
        fillLayout  = (LinearLayout) view.findViewById(R.id.fill_layout_bank_details);
        emptyLayout = (LinearLayout) view.findViewById(R.id.empty_layout_bank_details);

        accName = (TextView) view.findViewById(R.id.txt_username_account_name);
        accNumber = (TextView) view.findViewById(R.id.txt_username_account_number);
        accIFSC = (TextView) view.findViewById(R.id.txt_username_ifsc);
        accPan = (TextView) view.findViewById(R.id.txt_username_pancard);
    }

    public void intializeDataString(){
        username  = Utils.getStringFromPreferences(context,Utils.executiveNameKey);
        useremail = Utils.getStringFromPreferences(context,Utils.emailKey);
        contact   = Utils.getStringFromPreferences(context,Utils.executiveMobileKey);

        accountName = Utils.getStringFromPreferences(context,Utils.accountNameKey);
        accountNumber = Utils.getStringFromPreferences(context,Utils.accountNumberKey);
        IFSC = Utils.getStringFromPreferences(context,Utils.ifscCodeKey);
        Pancard = Utils.getStringFromPreferences(context,Utils.panNoKey);
    }

    public void setData() {
        intializeDataString();

        setProfileImage();

        name.setText(username);
        email.setText(useremail);
        if(!contact.equals("")) {
            if (!contact.startsWith("+91"))
                phone.setText("+91 " + contact);
            else
                phone.setText(contact);

            verify.setText("Verified");
            verify.setTextColor(Color.parseColor("#097e17"));

        }

        if(!accountName.equals("") || !accountNumber.equals("") || !IFSC.equals("") || !Pancard.equals("")) {
            fillLayout.setVisibility(View.VISIBLE);
            emptyLayout.setVisibility(View.GONE);
            editBankDetails.setVisibility(View.VISIBLE);
            accName.setText(accountName);
            accNumber.setText(accountNumber);
            accIFSC.setText(IFSC);
            accPan.setText(Pancard);
        }else{
            editBankDetails.setVisibility(View.GONE);
        }
    }

    private void setListner() {
        profileCard.setOnClickListener(new MyClickListener());
        changePassord.setOnClickListener(new MyClickListener());
        emptyLayout.setOnClickListener(new MyClickListener());
        editBankDetails.setOnClickListener(new MyClickListener());
        verify.setOnClickListener(new MyClickListener());
    }

    @Override
    public void onResume() {
        super.onResume();
      //  setProfileImage();
    }

    public void setProfileImage(){
        if(MainActivity.bitmap != null){
            profileImage.setImageBitmap(MainActivity.bitmap);
        }
    }

    private class MyClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.profile_card:

                    ProfileDetail details = ProfileDetail.newInstance() ;

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        details.setSharedElementEnterTransition(new Animation());
                        details.setEnterTransition(new ChangeBounds());
                        setExitTransition(new ChangeBounds());
                        details.setSharedElementReturnTransition(new Animation());
                        transaction.addSharedElement(profileImage,profileImage.getTransitionName()) ;
                    }

                    transaction.replace(R.id.multiFragmnetContainer,details);
                    transaction.addToBackStack("ProfileDetail");
                    transaction.commit();

                break ;

                case R.id.btn_username_password:
                    //Toast.makeText(context, "Change Password", Toast.LENGTH_SHORT).show();
                    startForgotActivity();
                break;

                case R.id.btn_edit_bank_details:
                    startBankActivity();
                break;

                case R.id.txt_username_contact_verify:
                    //Toast.makeText(context, "Verify Contact Number", Toast.LENGTH_SHORT).show();
                break ;

                case R.id.empty_layout_bank_details:
                    startBankActivity();
                break;
            }
        }
    }

    private void startForgotActivity() {
        Intent intent = new Intent(MainActivity.activity, MultiUseActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(MultiUseActivity.SELECT_CHANGE_PASSWORD,MultiUseActivity.SELECT_CHANGE_PASSWORD);
        bundle.putString(MultiUseActivity.TITLE,"Change Password");
        intent.putExtras(bundle);
        startActivity(intent);
    }

    private void startBankActivity() {
        Intent intent = new Intent(MainActivity.activity, MultiUseActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(MultiUseActivity.SELECT_BANK_DETAIL,MultiUseActivity.SELECT_BANK_DETAIL);
        bundle.putString(MultiUseActivity.TITLE,"Your Bank Details");
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @SuppressLint("NewApi")
    private class Animation extends TransitionSet {

        public Animation() {
            setOrdering(ORDERING_TOGETHER);
            addTransition(new ChangeBounds()).
            addTransition(new ChangeTransform()).
            addTransition(new ChangeImageTransform());
        }
    }
}
