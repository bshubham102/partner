package com.renewbuy.partners;


import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.renewbuy.partners.Adapter.NotificationAdapter;
import com.renewbuy.partners.db.AppDb;
import com.renewbuy.partners.db.NotificationTable;
import com.renewbuy.partners.modelClass.NotificationModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;


/**
 * A simple {@link Fragment} subclass.
 */
public class Notification extends Fragment {

    private static final String TAG = "Reminder";

    View rootView;
    Handler handler = new Handler();
    LinearLayout layout;

    public ListView listView;
    Context context;
    public static Notification instance ;
    public static NotificationAdapter adapter;
    public static List<NotificationModel> notificationModelList;

    private AppDb appDb ;
    private NotificationTable table;


    public Notification() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView =  inflater.inflate(R.layout.fragment_notification, container, false);

        context  = getActivity();
        layout   = (LinearLayout) rootView.findViewById(R.id.notification_empty_layout);
        listView = (ListView)     rootView.findViewById(R.id.lv_notification);
        //listView.setDividerHeight(0);
        appDb = AppDb.getInstance(context);
        table = (NotificationTable) appDb.getNotificationTableObject(NotificationTable.TABLE_NAME);
        notificationModelList = new ArrayList<>();

        GetNotification();

        ChangeStatus();

        instance = this ;

        return rootView ;
    }

    private void ChangeStatus() {
        MainActivity.activity.resetNotficationCounter();
    }

    private void GetNotification(){
        Vector<NotificationModel> models =  table.getNotifications(context);
        for (int i = 0 ; i<= models.size()-1 ; i++){
            notificationModelList.add(models.get(i)) ;
        }
        changeUI();
    }

    private void changeUI() {
        if (notificationModelList.size() > 0) {
            adapter = new NotificationAdapter(getActivity(), R.layout.notification_row, notificationModelList);
            listView.setAdapter(adapter);
            //makeAllAsRead();
        } else {
            layout.setVisibility(View.VISIBLE);
        }
    }

}
