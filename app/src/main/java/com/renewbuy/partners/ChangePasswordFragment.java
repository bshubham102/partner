package com.renewbuy.partners;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.renewbuy.mercury.Authentication;
import com.renewbuy.mercury.callBacks.CallBack;
import com.renewbuy.mercury.models.ChangePasswordModel;
import com.renewbuy.mercury.models.ErrorModel;

/**
 * Created by d2c3 on 18/4/15.
 */
public class ChangePasswordFragment extends Fragment {
    private EditText oldPasswordField;
    private EditText newPassword1Field;
    private EditText newPassword2Field;
    private TextView change_old_show_pwd;
    private TextView change_newPwd1_show_pwd;
    private TextView change_newPwd2_show_pwd;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.change_password, container, false);

        oldPasswordField = (EditText) view.findViewById(R.id.old_password);
        newPassword1Field = (EditText) view.findViewById(R.id.new_password1);
        newPassword2Field = (EditText) view.findViewById(R.id.new_password2);

        change_old_show_pwd = (TextView) view.findViewById(R.id.tv_change_old_show_pwd);
        change_newPwd1_show_pwd = (TextView) view.findViewById(R.id.tv_change_newPwd1_show_pwd);
        change_newPwd2_show_pwd = (TextView) view.findViewById(R.id.tv_change_newPwd2_show_pwd);

        change_old_show_pwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Editable pwd = oldPasswordField.getText();
                if(pwd!=null && !pwd.toString().isEmpty()) {
                    if (change_old_show_pwd.getText().equals("Show")) {
                        change_old_show_pwd.setText("Hide");

                        oldPasswordField.setInputType(InputType.TYPE_CLASS_TEXT);
                        oldPasswordField.setTransformationMethod(null);
                        oldPasswordField.setSelection(pwd.length());
                    } else {
                        change_old_show_pwd.setText("Show");

                        oldPasswordField.setTransformationMethod(PasswordTransformationMethod.getInstance());
                        oldPasswordField.setSelection(pwd.length());
                    }
                }
            }
        });

        oldPasswordField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s != null && !s.toString().isEmpty()) {
                    change_old_show_pwd.setVisibility(View.VISIBLE);
                } else {
                    change_old_show_pwd.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        change_newPwd1_show_pwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Editable pwd = newPassword1Field.getText();
                if (pwd != null && !pwd.toString().isEmpty()) {
                    if (change_newPwd1_show_pwd.getText().equals("Show")) {
                        change_newPwd1_show_pwd.setText("Hide");

                        newPassword1Field.setInputType(InputType.TYPE_CLASS_TEXT);
                        newPassword1Field.setTransformationMethod(null);
                        newPassword1Field.setSelection(pwd.length());
                    } else {
                        change_newPwd1_show_pwd.setText("Show");

                        newPassword1Field.setTransformationMethod(PasswordTransformationMethod.getInstance());
                        newPassword1Field.setSelection(pwd.length());
                    }
                }
            }
        });

        newPassword1Field.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s != null && !s.toString().isEmpty()) {
                    change_newPwd1_show_pwd.setVisibility(View.VISIBLE);
                } else {
                    change_newPwd1_show_pwd.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        change_newPwd2_show_pwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Editable pwd = newPassword2Field.getText();
                if (pwd != null && !pwd.toString().isEmpty()) {
                    if (change_newPwd2_show_pwd.getText().equals("Show")) {
                        change_newPwd2_show_pwd.setText("Hide");

                        newPassword2Field.setInputType(InputType.TYPE_CLASS_TEXT);
                        newPassword2Field.setTransformationMethod(null);
                        newPassword2Field.setSelection(pwd.length());
                    } else {
                        change_newPwd2_show_pwd.setText("Show");

                        newPassword2Field.setTransformationMethod(PasswordTransformationMethod.getInstance());
                        newPassword2Field.setSelection(pwd.length());
                    }
                }
            }
        });

        newPassword2Field.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s != null && !s.toString().isEmpty()) {
                    change_newPwd2_show_pwd.setVisibility(View.VISIBLE);
                } else {
                    change_newPwd2_show_pwd.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        final Button btnChangePassword = (Button) view.findViewById(R.id.btn_change_password);

        btnChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String oldPassword = oldPasswordField.getText().toString().trim();
                String newPassword1 = newPassword1Field.getText().toString().trim();
                String newPassword2 = newPassword2Field.getText().toString().trim();

                if (oldPassword.isEmpty()) {
                    oldPasswordField.setError("Please enter password");
                    oldPasswordField.requestFocus();
                } else if (newPassword1.isEmpty()) {
                    newPassword1Field.setError("Please enter password");
                    newPassword1Field.requestFocus();
                } else if (newPassword2.isEmpty()) {
                    newPassword2Field.setError("Please enter password");
                    newPassword2Field.requestFocus();
                } else if (!newPassword2.equals(newPassword1)) {
                    newPassword2Field.setError("New passwords are not matching");
                    newPassword2Field.requestFocus();
                } else {
                    final ProgressDialog progressDialog = ProgressDialog.show(getActivity(), "", "Please wait...", true);
                    progressDialog.show();
                    Utils.hideKeyboard(getActivity());
                    Authentication authentication = new Authentication();
                    authentication.ChangePasswordRequest(oldPassword, newPassword1, new CallBack<ChangePasswordModel>() {
                        @Override
                        public void onResponse(ChangePasswordModel model) {
                            progressDialog.dismiss();
                            String success = model.getSuccess();
                            if(!success.equals("") && success.length() > 0) {
                                Toast.makeText(getActivity(), "Your Password successfully changed.", Toast.LENGTH_SHORT).show();
                                EmptyView();
                            }
                        }

                        @Override
                        public void onError(ErrorModel errorModel) {
                            progressDialog.dismiss();
                            Toast.makeText(getActivity(), errorModel.getError(), Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });
        return view;
    }

    private void EmptyView(){
        oldPasswordField.setText(null);
        newPassword1Field.setText(null);
        newPassword2Field.setText(null);
    }
}
