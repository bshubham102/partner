package com.renewbuy.partners;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.MenuItem;

public class MultiUseActivity extends AppCompatActivity {
    private static final String TAG = "MultiUseActivity";

    public static final String TITLE = "TITLE" ;

    public static final String SELECT_PROFILE = "SELECT_PROFILE";
    public static final String SELECT_BANK_DETAIL = "SELECT_BANK_DETAIL";
    public static final String SELECT_FEEDBACK = "SELECT_FEEDBACK";
    public static final String SELECT_FRAGEMENT = "SELECT_FRAGMENT";
    public static final String SELECT_WEBVIEW = "SELECT_WEBVIEW" ;
    public static final String SELECT_NOTIFICATION = "SELECT_NOTIFICATION" ;
    public static final String SELECT_OPREATION = "SELECT_OPERATION";
    public static final String SELECT_WALLET = "SELECT_WALLET" ;
    public static final String SELECT_CHANGE_PASSWORD = "SELECT_CHANGE_PASSWORD" ;

    private doWebviewBack webviewBack ;

    public FragmentManager fragmentManager ;
    private FragmentTransaction transaction;

    private int purpose = 0 ;
    private String webViewUrl = null ;

    private boolean profileFlag , bankFlag, NotificationFlag, walletFlag, feedbackFlag, changePasswordFlag ;

    public static MultiUseActivity instance ;
    private ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main2);

        instance = this ;

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            actionBar = getSupportActionBar();
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        Bundle bundle = getIntent().getExtras();


        if(bundle.containsKey(TITLE)) {
            actionBar.setTitle(Html.fromHtml("<small>"+bundle.getString(TITLE)+"</small>"));
            //actionBar.setTitle(bundle.getString(TITLE));
        }

        if (bundle.containsKey(SELECT_FRAGEMENT))
           purpose = bundle.getInt(SELECT_FRAGEMENT);

        if(bundle.containsKey(SELECT_WEBVIEW)) {
            webViewUrl = bundle.getString(SELECT_WEBVIEW);
        }

        if(bundle.containsKey(SELECT_BANK_DETAIL)) {
            bankFlag =true ;
        }

        if(bundle.containsKey(SELECT_NOTIFICATION)) {
            NotificationFlag =true ;
        }

        if(bundle.containsKey(SELECT_FEEDBACK)) {
            feedbackFlag =true ;
        }

        if(bundle.containsKey(SELECT_WALLET)) {
            walletFlag =true ;
        }

        if(bundle.containsKey(SELECT_PROFILE)) {
            profileFlag =true ;
        }

        if(bundle.containsKey(SELECT_CHANGE_PASSWORD)) {
            changePasswordFlag =true ;
        }


        //Intialize Fragment Mangers nd Transaction
          fragmentManager = getSupportFragmentManager() ;
          transaction = fragmentManager.beginTransaction();

        //Select Proper List for requestCode
        // zero is default value
        if(purpose != 0) {
            selectFragment(purpose);
        }

        //View The Webview with a porper url it may be used with more than fragment
        if(webViewUrl != null) {
            IntiaiteWebView(webViewUrl);
        }

        //Intialize the Bank Details Fragment to show theie details
        if(bankFlag) {
            IntialiteBankFragment();
        }

        //Intialize the Notifcation Details Fragment to show theie details
        if(NotificationFlag) {
            IntialiteNotificationFragment();
        }

        //Intialize the feedback Fragment
        if(feedbackFlag) {
            IntialiteFeddbackFragment();
        }

        //Intialize the wallet Fragment
        if(walletFlag) {
            IntialiteWalletFragment();
        }

        //Intialize the Profile Fragment
        if(profileFlag) {
            IntialiteProfileFragment();
        }

        //Intialize the Profile Fragment
        if(changePasswordFlag) {
            IntialiteChangePasswordFragment();
        }


    }

    private void selectFragment(int purpose) {
        switch (purpose) {
            case Utils.MANUFACTURER_REQUEST:
                IntiateList(Utils.MANUFACTURER_REQUEST);
            break;

            case Utils.MODEL_REQUEST:
                IntiateList(Utils.MODEL_REQUEST);
            break;

            case Utils.VARIANT_REQUEST:
                IntiateList(Utils.VARIANT_REQUEST);
            break;

            case Utils.CITY_REQUEST:
                IntiateList(Utils.CITY_REQUEST);
            break;
        }
    }

    private void IntiateList(int requestID) {
        DynamicList fragment = new DynamicList() ;
        Bundle bundle = new Bundle();
        bundle.putInt(SELECT_OPREATION,requestID);
        fragment.setArguments(bundle);
        transaction.replace(R.id.multiFragmnetContainer,fragment);
        transaction.addToBackStack("DynamicList");
        transaction.commit();
    }

    private void IntiaiteWebView(String URL){
        WebViewFragment fragment = new WebViewFragment() ;
        Bundle bundle = new Bundle();
        bundle.putString(WebViewFragment.SELECT_URL,URL);
        fragment.setArguments(bundle);
        webviewBack = fragment ;
        transaction.replace(R.id.multiFragmnetContainer,fragment);
        transaction.addToBackStack("WebViewFragment");
        transaction.commit();
    }

    private void IntialiteBankFragment() {
        transaction.replace(R.id.multiFragmnetContainer,new BankDetail());
        transaction.addToBackStack("BankDetail");
        transaction.commit();
    }

    private void IntialiteNotificationFragment() {
        transaction.replace(R.id.multiFragmnetContainer,new Notification());
        transaction.addToBackStack("Notification");
        transaction.commit();
    }


    private void IntialiteFeddbackFragment() {
        transaction.replace(R.id.multiFragmnetContainer,new FeedbackFragment());
        transaction.addToBackStack("FeedbackFragment");
        transaction.commit();
    }

    private void IntialiteWalletFragment() {
        transaction.replace(R.id.multiFragmnetContainer,new Wallet());
        transaction.addToBackStack("Wallet");
        transaction.commit();
    }

    private void IntialiteProfileFragment() {
        transaction.replace(R.id.multiFragmnetContainer,new Profile());
        transaction.addToBackStack("Profile");
        transaction.commit();
    }

    private void IntialiteChangePasswordFragment() {
        transaction.replace(R.id.multiFragmnetContainer,new ChangePasswordFragment());
        transaction.addToBackStack("ChangePasswordFragment");
        transaction.commit();
    }

    @Override
    public void onBackPressed() {
        BackPress();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                BackPress();
            break;

            case R.id.close_web:
                finish();
            break;
        }
        return true ;
    }

    public interface doWebviewBack{
        void onBackPress(Activity activity);
    }

    private void BackPress(){
        int count = fragmentManager.getBackStackEntryCount() ;
        String fname =  fragmentManager.getBackStackEntryAt(count -1).getName();
        if(fname.equals("WebViewFragment")) {
            if(webviewBack != null) {
                webviewBack.onBackPress(this);
            }
        } else if(fname.equals("ProfileDetail")) {
            fragmentManager.popBackStack();
        }
        else {
            instance = null ;
            finish();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(ProfileDetail.instance != null){
            ProfileDetail.instance.onActivityResult(requestCode, resultCode, data);
        }
    }

}
