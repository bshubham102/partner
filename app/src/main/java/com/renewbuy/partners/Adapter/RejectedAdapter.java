package com.renewbuy.partners.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.renewbuy.partners.CapturePolicy;
import com.renewbuy.partners.R;
import com.renewbuy.partners.Stats;
import com.renewbuy.partners.db.CustomerBean;

import java.util.List;

/**
 * Created by shubham on 19/7/16.
 */
public class RejectedAdapter extends ArrayAdapter<CustomerBean> {

    private Stats.OnStatUploadClickListener mListener ;

    private Context context;
    private int resource;
    private List<CustomerBean> modelsList;
    private LayoutInflater inflater;

    public RejectedAdapter(Context context, int resource, List<CustomerBean> modelsList) {
        super(context, resource, modelsList);
        this.context = context;
        this.resource = resource;
        this.modelsList = modelsList;
        inflater = LayoutInflater.from(context);
        mListener  = (Stats.OnStatUploadClickListener) context;
    }

    @Override
    public int getCount() {
        return modelsList.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = null;
        ViewHolder viewHolder = null;

        if(convertView == null) {

            view = inflater.inflate(resource,parent,false);
            viewHolder = new ViewHolder();
            viewHolder.name = (TextView) view.findViewById(R.id.username) ;
            viewHolder.reason = (TextView) view.findViewById(R.id.reason) ;
            viewHolder.resubmit = (LinearLayout) view.findViewById(R.id.resubmit);
            view.setTag(viewHolder);

        } else {
            view = convertView;
            viewHolder = (ViewHolder) view.getTag();
        }

        CustomerBean model = modelsList.get(position);

        viewHolder.name.setText(model.getCustomer_name());
        viewHolder.reason.setText("Reason: "+model.getRejection_reason());

        viewHolder.resubmit.setTag(position);
        viewHolder.resubmit.setOnClickListener(new Listener());
        return view;
    }

    static class ViewHolder{
        TextView name,reason;
        LinearLayout resubmit;
    }

    private class Listener implements View.OnClickListener{
        @Override
        public void onClick(View v) {
            int pos = (int) v.getTag();
            int lead_id = modelsList.get(pos).getLead_id();
            mListener.onUploadClickListener(true,2);
            CapturePolicy.getInstance().onResubmitClick(lead_id);
            //Toast.makeText(context, ""+pos, Toast.LENGTH_SHORT).show();
        }
    }
}
