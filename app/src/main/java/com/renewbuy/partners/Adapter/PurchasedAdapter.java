package com.renewbuy.partners.Adapter;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.PowerManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.renewbuy.partners.Config;
import com.renewbuy.partners.MainActivity;
import com.renewbuy.partners.MultiUseActivity;
import com.renewbuy.partners.R;
import com.renewbuy.partners.Utils;
import com.renewbuy.partners.db.PaymentBean;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by shubham on 27/7/16.
 */
public class PurchasedAdapter extends ArrayAdapter<PaymentBean> {
    private Context context;
    private int resource;
    private List<PaymentBean> modelsList;
    private LayoutInflater inflater;
    private boolean[] dropedList ;

    public PurchasedAdapter(Context context, int resource, List<PaymentBean> objects) {
        super(context, resource, objects);
        this.context = context;
        this.resource = resource;
        this.modelsList = objects;
        inflater = LayoutInflater.from(context);

        dropedList = new boolean[modelsList.size()];

        for (int i = 0; i <= modelsList.size()-1 ; i++) {
            dropedList[i] = false ;
        }
    }

    @Override
    public int getCount() {
        return modelsList.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = null;
        ViewHolder viewHolder = null;

        if(convertView == null) {

            view = inflater.inflate(resource,parent,false);
            viewHolder = new ViewHolder();

            viewHolder.policy_number = (TextView) view.findViewById(R.id.purchased_policy_number) ;
            viewHolder.policy_date = (TextView) view.findViewById(R.id.purchased_policy_date) ;
            viewHolder.name = (TextView) view.findViewById(R.id.purchased_name) ;
            viewHolder.model = (TextView) view.findViewById(R.id.purchased_car_name) ;
            viewHolder.total_premium = (TextView) view.findViewById(R.id.purchased_total_premium) ;
            viewHolder.insurer_name = (TextView) view.findViewById(R.id.purchased_insurer_name);
            viewHolder.od_premium = (TextView) view.findViewById(R.id.purchased_od_premium) ;
            viewHolder.earn = (TextView) view.findViewById(R.id.purchased_total_earned) ;

            viewHolder.floating_layout = (LinearLayout) view.findViewById(R.id.floating_layout);
            viewHolder.download_policy = (TextView) view.findViewById(R.id.download_policy);
            viewHolder.buy_dengue = (TextView) view.findViewById(R.id.buy_dengue);

            viewHolder.dropdown = (RelativeLayout) view.findViewById(R.id.purchased_drop_btn);

            view.setTag(viewHolder);

        } else {
            view = convertView;
            viewHolder = (ViewHolder) view.getTag();
        }

        PaymentBean model = modelsList.get(position);

        viewHolder.policy_number.setText("Policy #"+model.getPolicyNo());

        SimpleDateFormat  format = new SimpleDateFormat("dd-MMM-yyyy");
        Date d =model.getCustomer_prev_policy_expiry();

        if(d != null)
         viewHolder.policy_date.setText(format.format(d));

        viewHolder.name.setText(model.getCustomer_name());
        viewHolder.model.setText(model.getCustomer_vehical_name());

        int total_pre = (int) model.getCustomer_total_premium();
        String total = context.getResources().getString(R.string.ruppee)+total_pre;
        viewHolder.total_premium.setText(total);

        int od_pre = (int) model.getOdpremium();
        String od = context.getResources().getString(R.string.ruppee)+ od_pre;
        viewHolder.od_premium.setText(od);

        int total_earn = (int) model.getCustomer_earned();
        String earned = context.getResources().getString(R.string.ruppee)+total_earn;
        viewHolder.earn.setText(earned);

        viewHolder.insurer_name.setText(model.getInsurer());

        String doc_url = model.getDocUrl();

        if(doc_url != null && !doc_url.isEmpty()) {
            viewHolder.download_policy.setTag(position);
            viewHolder.download_policy.setOnClickListener(new Listener());
        }else{
            viewHolder.download_policy.setVisibility(View.INVISIBLE);
        }

        viewHolder.buy_dengue.setTag(position);
        viewHolder.buy_dengue.setOnClickListener(new Listener());

        viewHolder.dropdown.setTag(R.string.POSITION_TAG, position);
        viewHolder.dropdown.setTag(R.string.MODEL_TAG, viewHolder.floating_layout);

        viewHolder.dropdown.setTag(position);
        viewHolder.dropdown.setOnClickListener(new Listener());

        return view;
    }

    static class ViewHolder{
        TextView policy_number,policy_date,name,model,total_premium,od_premium,earn,insurer_name;
        TextView download_policy, buy_dengue ;
        LinearLayout  floating_layout;
        RelativeLayout dropdown;
    }

    private class Listener implements View.OnClickListener{
        @SuppressLint("NewApi")
        @Override
        public void onClick(View v) {
            int id  = v.getId();
            switch (id) {
                case R.id.download_policy:

                    int pos = (int) v.getTag();
                    String URL = modelsList.get(pos).getDocUrl();
                    //Toast.makeText(context, "Your Policy will download shortly "+URL, Toast.LENGTH_SHORT).show();
                    final DownloadDocument downloadTask = new DownloadDocument(context);
                    downloadTask.execute(URL);


                break;

                case R.id.buy_dengue:
                    int pay_pos  = (int) v.getTag();
                    int  pay_id = modelsList.get(pay_pos).getPayment_id();
                    //Toast.makeText(context, "Your PaYMENT Id"+ pay_id, Toast.LENGTH_SHORT).show();

                    String mQutoeUrl = Config.DOMAIN + "/products/dengue-shield/?id="+pay_id ;
                    String Title = "Dengue Shield";
                    //Toast.makeText(context, "Your Quotes Url is "+mQutoeUrl, Toast.LENGTH_SHORT).show();
                    Bundle bundle = new Bundle();
                    bundle.putString(MultiUseActivity.SELECT_WEBVIEW,mQutoeUrl);
                    bundle.putString(MultiUseActivity.TITLE,Title);
                    Intent intent = new Intent(MainActivity.activity,MultiUseActivity.class);
                    intent.putExtras(bundle);

                    context.startActivity(intent);
                break;

                case R.id.purchased_drop_btn:

                    int position = (int) v.getTag(R.string.POSITION_TAG);

                    RelativeLayout relativeLayout = (RelativeLayout) v;
                    ImageView image = (ImageView) relativeLayout.getChildAt(0);
                    LinearLayout floating_layout  = (LinearLayout) v.getTag(R.string.MODEL_TAG);

                    boolean isItemExpand = dropedList[position];

                    if( !isItemExpand ) {
                        floating_layout.setVisibility(View.VISIBLE);
                    } else {
                        floating_layout.setVisibility(View.GONE);
                    }

                    image.animate().rotationBy(180).setDuration(300).start();
                    dropedList[position] = !dropedList[position];

                break;
            }

        }
    }

    private class DownloadDocument extends AsyncTask<String,Integer,String>{

        private Context context;

        private ProgressDialog mProgressDialog ;
        private PowerManager.WakeLock mWakeLock;

        private String fileUri ;

        @TargetApi(Build.VERSION_CODES.HONEYCOMB)
        public DownloadDocument(Context context) {
            this.context = context ;
            mProgressDialog = new ProgressDialog(context);
                    /*mPr.show(context,"","Downloading...",true,false,new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    cancel(true);
                }
            });*/
            mProgressDialog.setMessage("Downloading...");
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setMax(100);
            mProgressDialog.setProgressNumberFormat(null);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            mProgressDialog.show();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, getClass().getName());
            mWakeLock.acquire();
        }

        @Override
        protected String doInBackground(String... downloadUrl) {
            InputStream input = null;
            OutputStream output = null;
            HttpURLConnection connection = null;
            try{
                URL url = new URL(downloadUrl[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                if(connection.getResponseCode() != HttpURLConnection.HTTP_OK){
                    return "Server returned HTTP " + connection.getResponseCode()
                            + " " + connection.getResponseMessage();
                }
                // this will be useful to display download percentage
                // might be -1: server did not report the length
                int fileLength = connection.getContentLength();

                // download the file
                input = connection.getInputStream();

                String[] fileSplit = downloadUrl[0].split("/");
                int length = fileSplit.length;
                String filename = fileSplit[length-1];

                String docpath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Renewbuy/Documents";

                File dir = new File(docpath);
                if(!dir.exists())
                    dir.mkdirs();

                File doc = new File(dir,filename);
                fileUri = doc.getPath();

                output = new FileOutputStream(doc);

                byte data[] = new byte[4096];
                long total = 0;
                int count;
                while ((count = input.read(data)) != -1) {
                    // allow canceling with back button
                    if (isCancelled()) {
                        input.close();
                        return null;
                    }
                    total += count;
                    // publishing the progress....
                    if (fileLength > 0) // only if total length is known
                        publishProgress((int) (total * 100 / fileLength));
                    output.write(data, 0, count);
                }
            } catch (IOException e) {
                return e.toString();
            } finally {
                try {
                    if (output != null)
                        output.close();
                    if (input != null)
                        input.close();
                } catch (IOException ignored) {
                }

                if (connection != null)
                    connection.disconnect();
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);
            // if we get here, length is known, now set indeterminate to false
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setProgress(progress[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            mWakeLock.release();
            mProgressDialog.dismiss();
            if (result != null) {
                Toast.makeText(context, "Download error: " + result, Toast.LENGTH_LONG).show();
                Log.i("onPostExecute: ",result);
            } else {
                Toast.makeText(context, "File downloaded", Toast.LENGTH_SHORT).show();

                final Uri uri = Uri.fromFile(new File(fileUri));

                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(uri, Utils.getMimeType(uri));
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                context.startActivity(intent);
            }
        }
    }
}
