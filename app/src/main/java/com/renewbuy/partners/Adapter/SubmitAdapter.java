package com.renewbuy.partners.Adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.renewbuy.partners.Api;
import com.renewbuy.partners.MainActivity;
import com.renewbuy.partners.MultiUseActivity;
import com.renewbuy.partners.R;
import com.renewbuy.partners.Utils;
import com.renewbuy.partners.db.CustomerBean;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by shubham on 26/7/16.
 */
public class SubmitAdapter extends ArrayAdapter<CustomerBean> {

    private static final String SUBMITTED  = "Submitted";
    private static final String ACCEPTED  = "Accepted";
    private static final String ACCEPTED_OFFLINE  = "Accepted Offline";

    private Context context;
    private int resource;
    private List<CustomerBean> modelsList;
    private LayoutInflater inflater;

    public SubmitAdapter(Context context, int resource, List<CustomerBean> modelsList) {
        super(context, resource, modelsList);
        this.context = context;
        this.resource = resource;
        this.modelsList = modelsList;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return modelsList.size();
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        View view = null;
        ViewHolder viewHolder = null;

        if(convertView == null){
            view = inflater.inflate(resource,parent,false);
            viewHolder = new ViewHolder();
            viewHolder.name = (TextView) view.findViewById(R.id.username) ;
            viewHolder.status = (TextView) view.findViewById(R.id.submit_n_accept);
            viewHolder.modelname = (TextView) view.findViewById(R.id.model_n_number) ;
            viewHolder.date = (TextView) view.findViewById(R.id.duedate) ;
            viewHolder.call = (LinearLayout) view.findViewById(R.id.submit_call) ;
            viewHolder.submit_layout = (LinearLayout) view.findViewById(R.id.submitted_view_quote) ;
            viewHolder.view_quotes = (TextView) view.findViewById(R.id.view_quotes) ;
            viewHolder.email_quotes = (TextView) view.findViewById(R.id.email_quote) ;


            view.setTag(viewHolder);
        }else{
            view = convertView;
            viewHolder = (ViewHolder) view.getTag();
        }

        CustomerBean model = modelsList.get(position);

        //Set the Customer Name
        viewHolder.name.setText(model.getCustomer_name());

        /**
         *Check Customer Policy Status
         * @method: model.getCustomer_document_status();
         * @return: status 1(Submitted) or 2(Accepted)
         * @purpose:
         * if status is '1' show Customer Status and Its Mobile Number
         * if status is '2' show customer Status and Model name and Due date of Vehicle and show the view quotes link in WebView
         */
        int status = model.getCustomer_document_status();
        if( status == Utils.SUBMITTED) {
            viewHolder.status.setText(SUBMITTED);
            viewHolder.modelname.setText(model.getCustomer_mobile());
            viewHolder.date.setVisibility(View.GONE);
            viewHolder.call.setVisibility(View.GONE);
            viewHolder.submit_layout.setVisibility(View.GONE);
        }

        else if(status == Utils.ACCEPTED) {
            viewHolder.status.setText(ACCEPTED);
            viewHolder.modelname.setText(model.getCustomer_vehical_name());
            SimpleDateFormat format = new SimpleDateFormat("dd MMMM, yyyy");
            Date d = model.getCustomer_prev_policy_expiry() ;

            if( d != null ) {
                viewHolder.date.setText("Due Date: " + format.format(d));
                viewHolder.date.setVisibility(View.VISIBLE);
            }

            viewHolder.call.setVisibility(View.VISIBLE);
            viewHolder.call.setTag(position);
            viewHolder.call.setOnClickListener(new Listener());

            viewHolder.submit_layout.setVisibility(View.VISIBLE);

            if(model.getQuotesUrl() != null){
                viewHolder.view_quotes.setVisibility(View.VISIBLE);
                viewHolder.view_quotes.setTag(position);
                viewHolder.view_quotes.setOnClickListener(new Listener());
            }

            viewHolder.email_quotes.setTag(position);
            viewHolder.email_quotes.setOnClickListener(new Listener());
        }

        else if(status == Utils.ACCEPTED_OFFLINE) {
            viewHolder.status.setText(ACCEPTED_OFFLINE);
            viewHolder.modelname.setText(model.getCustomer_vehical_name());
            SimpleDateFormat format = new SimpleDateFormat("dd MMMM, yyyy");
            Date d = model.getCustomer_prev_policy_expiry() ;

            if( d != null ) {
                viewHolder.date.setText("Due Date: " + format.format(d));
                viewHolder.date.setVisibility(View.VISIBLE);
            }

            viewHolder.call.setVisibility(View.VISIBLE);
            viewHolder.call.setTag(position);
            viewHolder.call.setOnClickListener(new Listener());

            viewHolder.submit_layout.setVisibility(View.VISIBLE);
            viewHolder.view_quotes.setVisibility(View.GONE);


            viewHolder.email_quotes.setText("CALL TO RENEW");
            viewHolder.email_quotes.setTag(position);
            viewHolder.email_quotes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent callIntent = new Intent(Intent.ACTION_DIAL);
                    try {
                        callIntent.setData(Uri.parse("tel:" + context.getResources().getString(R.string.rb_toll_free)));
                        context.startActivity(callIntent);
                    } catch (Exception ignored) {
                    }
                }
            });
        }

        return view;
    }

    static class ViewHolder{
        TextView name,status,modelname,date,view_quotes,email_quotes;
        LinearLayout call,submit_layout;
    }

    private class Listener implements View.OnClickListener{
        @Override
        public void onClick(View v) {
            int pos = (int) v.getTag();
            int id = v.getId();

            switch (id){
                case R.id.submit_call:

                    String number = modelsList.get(pos).getCustomer_mobile();
                    Intent callIntent = new Intent(Intent.ACTION_DIAL);
                    try {
                        callIntent.setData(Uri.parse("tel:" + number));
                        context.startActivity(callIntent);
                    } catch (Exception ignored) {
                    }

                break;
                case R.id.view_quotes:

                    String mQutoeUrl = modelsList.get(pos).getQuotesUrl()+"&via_cse";
                    String Title = modelsList.get(pos).getCustomer_vehical_name();
                    //Toast.makeText(context, "Your Quotes Url is "+mQutoeUrl, Toast.LENGTH_SHORT).show();
                    Bundle bundle = new Bundle();
                    bundle.putString(MultiUseActivity.SELECT_WEBVIEW,mQutoeUrl);
                    bundle.putString(MultiUseActivity.TITLE,Title);
                    Intent intent = new Intent(MainActivity.activity,MultiUseActivity.class);
                    intent.putExtras(bundle);

                    context.startActivity(intent);

                break;

                case R.id.email_quote:
                     final int vehicle_ID = modelsList.get(pos).getVehicle_id();

                      //Uncomment if required
                     /*Date prevPolicyExpiry = modelsList.get(pos).getCustomer_prev_policy_expiry();

                     SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH);
                     Date curDate = new Date(formatter.format(Calendar.getInstance().getTime()));

                     long no_of_days = 0;
                     if (prevPolicyExpiry != null)
                        no_of_days = ((prevPolicyExpiry.getTime() - curDate.getTime()) / (1000 * 60 * 60 * 24)) + 1;

                     String email_quotes_msg = null;
                     if (no_of_days > 0) {
                        if (no_of_days > 60) {
                            Calendar c = Calendar.getInstance();
                            c.setTime(prevPolicyExpiry);
                            c.add(Calendar.DATE, -59);
                            String next_date = formatter.format(c.getTime());

                            email_quotes_msg = "Next email due on: " + next_date;
                        }
                        if (no_of_days <= 60 && no_of_days > 45) {
                            Calendar c = Calendar.getInstance();
                            c.setTime(prevPolicyExpiry);
                            c.add(Calendar.DATE, -59);
                            String last_date = formatter.format(c.getTime());

                            c = Calendar.getInstance();
                            c.setTime(prevPolicyExpiry);
                            c.add(Calendar.DATE, -44);
                            String next_date = formatter.format(c.getTime());

                            email_quotes_msg = "Last email sent on: " + last_date + "\nNext email due on: " + next_date;
                        }
                        if (no_of_days <= 45 && no_of_days > 30) {
                            Calendar c = Calendar.getInstance();
                            c.setTime(prevPolicyExpiry);
                            c.add(Calendar.DATE, -44);
                            String last_date = formatter.format(c.getTime());

                            c = Calendar.getInstance();
                            c.setTime(prevPolicyExpiry);
                            c.add(Calendar.DATE, -29);
                            String next_date = formatter.format(c.getTime());

                            email_quotes_msg = "Last email sent on: " + last_date + "\nNext email due on: " + next_date;
                        }
                        if (no_of_days <= 30 && no_of_days > 15) {
                            Calendar c = Calendar.getInstance();
                            c.setTime(prevPolicyExpiry);
                            c.add(Calendar.DATE, -29);
                            String last_date = formatter.format(c.getTime());

                            c = Calendar.getInstance();
                            c.setTime(prevPolicyExpiry);
                            c.add(Calendar.DATE, -14);
                            String next_date = formatter.format(c.getTime());

                            email_quotes_msg = "Last email sent on: " + last_date + "\nNext email due on: " + next_date;
                        }
                        if (no_of_days <= 15 && no_of_days > 7) {
                            Calendar c = Calendar.getInstance();
                            c.setTime(prevPolicyExpiry);
                            c.add(Calendar.DATE, -14);
                            String last_date = formatter.format(c.getTime());

                            c = Calendar.getInstance();
                            c.setTime(prevPolicyExpiry);
                            c.add(Calendar.DATE, -6);
                            String next_date = formatter.format(c.getTime());

                            email_quotes_msg = "Last email sent on: " + last_date + "\nNext email due on: " + next_date;
                        }

                        if (no_of_days <= 7) {
                            Calendar c = Calendar.getInstance();
                            c.setTime(prevPolicyExpiry);
                            c.add(Calendar.DATE, -6);
                            String last_date = formatter.format(c.getTime());

                            email_quotes_msg = "Last email due on: " + last_date;
                        }
                    } else {
                        email_quotes_msg = "This policy has already expired. Do you still want to send an email to the contact?";
                    }
                    Api.emailQuotes(MainActivity.activity, String.valueOf(vehicle_ID));
                    new AlertDialog.Builder(MainActivity.activity)
                            .setTitle("Email Quotes?")
                            .setMessage(email_quotes_msg)
                            .setPositiveButton(R.string.btn_send_email, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                 Api.emailQuotes(MainActivity.activity, String.valueOf(vehicle_ID));
                                }
                            })
                            .setNegativeButton(android.R.string.cancel, null).show();*/

                    Api.emailQuotes(MainActivity.activity, String.valueOf(vehicle_ID));
                    break;
            }
        }
    }
}
