package com.renewbuy.partners.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.renewbuy.partners.R;
import com.renewbuy.partners.modelClass.DialogModel;

import java.util.ArrayList;

/**
 * Created by shubham on 13/9/16.
 */
public class DialogAdapter extends ArrayAdapter<DialogModel> {

    private Context context;
    private int resource ;
    private ArrayList<DialogModel> list ;
    private LayoutInflater inflater;

    public DialogAdapter(Context context, int resource, ArrayList<DialogModel> list) {
        super(context, resource, list);
        this.context = context;
        this.resource = resource;
        this.list = list;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = null;
        ViewHolder viewHolder = null;

        if(convertView == null){
            view = inflater.inflate(resource,parent,false);
            viewHolder = new ViewHolder();
            viewHolder.name = (TextView) view.findViewById(R.id.title);
            viewHolder.icon = (ImageView) view.findViewById(R.id.icon);
            view.setTag(viewHolder);
        }else{
            view = convertView;
            viewHolder = (ViewHolder) view.getTag();
        }

        viewHolder.name.setText(list.get(position).getTitle());
        viewHolder.icon.setImageDrawable(context.getResources().getDrawable(list.get(position).getIconResourceID()));

        return view;
    }

    static class ViewHolder{
        TextView name;
        ImageView icon ;
    }
}
