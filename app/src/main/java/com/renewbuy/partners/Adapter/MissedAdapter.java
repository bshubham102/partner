package com.renewbuy.partners.Adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.renewbuy.partners.MainActivity;
import com.renewbuy.partners.MultiUseActivity;
import com.renewbuy.partners.R;
import com.renewbuy.partners.db.CustomerBean;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by shubham on 18/7/16.
 */
public class MissedAdapter extends ArrayAdapter<CustomerBean> {

    private Context context;
    private int resource;
    private List<CustomerBean> modelsList;
    private LayoutInflater inflater;

    public MissedAdapter(Context context, int resource, List<CustomerBean> modelsList) {
        super(context, resource, modelsList);
        this.context = context;
        this.resource = resource;
        this.modelsList = modelsList;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return modelsList.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = null;
        ViewHolder viewHolder = null;

        if(convertView == null){
            view = inflater.inflate(resource,parent,false);
            viewHolder = new ViewHolder();
            viewHolder.name = (TextView) view.findViewById(R.id.username) ;
            viewHolder.modelname = (TextView) view.findViewById(R.id.modelname) ;
            viewHolder.date = (TextView) view.findViewById(R.id.duedate) ;
            viewHolder.call = (LinearLayout) view.findViewById(R.id.missed_call) ;
            viewHolder.view_quotes = (LinearLayout) view.findViewById(R.id.missed_view_quote) ;


            view.setTag(viewHolder);
        }else{
            view = convertView;
            viewHolder = (ViewHolder) view.getTag();
        }

        CustomerBean model = modelsList.get(position);

        viewHolder.name.setText(model.getCustomer_name());
        viewHolder.modelname.setText(model.getCustomer_vehical_name());

        SimpleDateFormat format = new SimpleDateFormat("dd MMMM, yyyy");
        Date d = model.getCustomer_prev_policy_expiry() ;
        if(d!=null)
        {
            viewHolder.date.setText("Due Date: "+format.format(d));
        }


        viewHolder.call.setTag(position);
        viewHolder.call.setOnClickListener(new Listener());

        int vehicleClass =  model.getCustomer_vehical_class();

        if ( vehicleClass == 1) {
            viewHolder.view_quotes.setTag(position);
            viewHolder.view_quotes.setOnClickListener(new Listener());
        } else{
            viewHolder.view_quotes.setVisibility(View.GONE);
        }

        return view;
    }

    static class ViewHolder{
        TextView name,modelname,date;
        LinearLayout call , view_quotes;
    }

    private class Listener implements View.OnClickListener{
        @Override
        public void onClick(View v) {
            int pos = (int) v.getTag();
            int id = v.getId() ;
            switch (id){
                case R.id.missed_call:

                    String number = modelsList.get(pos).getCustomer_mobile();
                    Intent callIntent = new Intent(Intent.ACTION_DIAL);
                    try {
                        callIntent.setData(Uri.parse("tel:" + number));
                        context.startActivity(callIntent);
                    } catch (Exception ignored) {
                    }

                break;

                case R.id.missed_view_quote:

                    String mQutoeUrl = modelsList.get(pos).getQuotesUrl();
                    String Title = modelsList.get(pos).getCustomer_vehical_name();
                    //Toast.makeText(context, "Your Quotes Url is "+mQutoeUrl, Toast.LENGTH_SHORT).show();
                    Bundle bundle = new Bundle();
                    bundle.putString(MultiUseActivity.SELECT_WEBVIEW,mQutoeUrl);
                    bundle.putString(MultiUseActivity.TITLE,Title);
                    Intent intent = new Intent(MainActivity.activity,MultiUseActivity.class);
                    intent.putExtras(bundle);

                    context.startActivity(intent);

                break;
            }

        }
    }
}
