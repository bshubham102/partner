package com.renewbuy.partners.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.renewbuy.partners.MultiUseActivity;
import com.renewbuy.partners.R;
import com.renewbuy.partners.modelClass.DynamicListModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shubham on 16/8/16.
 */
public class DynamicListAdapter extends ArrayAdapter<DynamicListModel> implements Filterable {
    private Context context;
    private int resource;
    private List<DynamicListModel> modelsList;
    private List<DynamicListModel> displaymodelsList;
    private LayoutInflater inflater;
    private int resultcode ;

    public DynamicListAdapter(Context context, int resource,  List<DynamicListModel> modelsList, int resultCode) {
        super(context, resource, modelsList);
        this.context = context;
        this.resource = resource;
        this.displaymodelsList = modelsList;
        inflater = LayoutInflater.from(context);
        this.resultcode = resultCode ;
    }

    static class ViewHolder{
        TextView DisplayText ;
    }

    @Override
    public int getCount() {
        return displaymodelsList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public DynamicListModel getItem(int position) {
        return displaymodelsList.get(position);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder holder ;

        if(view == null) {
            view = inflater.inflate(resource,parent,false);
            holder = new ViewHolder();
            holder.DisplayText = (TextView) view.findViewById(R.id.list_display_text);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        DynamicListModel model = displaymodelsList.get(position);
        holder.DisplayText.setText(model.getName());
        holder.DisplayText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra("Name",displaymodelsList.get(position).getName());
                intent.putExtra("ID",displaymodelsList.get(position).getId());
                MultiUseActivity.instance.setResult(resultcode,intent);
                MultiUseActivity.instance.finish();
            }
        });

        return view;
    }

    @Override
    public Filter getFilter() {
        return new Filter()
        {
            @Override
            protected void publishResults(CharSequence constraint,FilterResults results) {
                displaymodelsList = (ArrayList<DynamicListModel>) results.values; // has the filtered values
                notifyDataSetChanged();  // notifies the data with new filtered values
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();        // Holds the results of a filtering operation in values
                ArrayList<DynamicListModel> filteredArrList = new ArrayList<DynamicListModel>();

                if (modelsList == null) {
                    modelsList = new ArrayList<DynamicListModel>(displaymodelsList); // saves the original data in mOriginalValues
                }

                if (constraint == null || constraint.length() == 0) {
                    // set the Original result to return
                    results.count = modelsList.size();
                    results.values = modelsList;
                } else {
                    constraint = constraint.toString().toLowerCase();
                    for (int i = 0; i < modelsList.size(); i++) {
                        String data = modelsList.get(i).getName();
                        if (data.toLowerCase().startsWith(constraint.toString())) {
                            filteredArrList.add(new DynamicListModel(modelsList.get(i).getId(),modelsList.get(i).getName()));
                        }
                    }
                   // set the Filtered result to return
                  results.count = filteredArrList.size();
                  results.values = filteredArrList;
                }
                return results;
            }
        };
    }
}
