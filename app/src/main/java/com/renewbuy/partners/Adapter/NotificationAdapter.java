package com.renewbuy.partners.Adapter;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.renewbuy.partners.R;
import com.renewbuy.partners.Utils;
import com.renewbuy.partners.modelClass.NotificationModel;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * Created by shubham on 16/5/16.
 */
public class NotificationAdapter extends ArrayAdapter implements View.OnClickListener {

    private Context context;
    private int layout ;
    private List<NotificationModel> modelList = new ArrayList<>();

    private static LayoutInflater inflater ;

    public NotificationAdapter(Context context, int resource, List<NotificationModel> list ) {
        super(context, resource, list);
        this.context= context;
             layout = resource;
             modelList = list ;
        inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return modelList.size();
    }

    @Override
    public Object getItem(int position) {
        return modelList.get(position);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView ;
        if( convertView == null)
            v = inflater.inflate(layout, parent,false);

        NotificationModel model = modelList.get(position) ;

        ImageView icon = (ImageView) v.findViewById(R.id.img_noti_icon);
        TextView title = (TextView) v.findViewById(R.id.tv_noti_title);
        TextView body = (TextView) v.findViewById(R.id.tv_noti_body);
        TextView date = (TextView) v.findViewById(R.id.tv_noti_date);
        RelativeLayout notificationLayout = (RelativeLayout) v.findViewById(R.id.notification_layout);

        if(model.getIcon() != null) {
            if (model.getIcon().startsWith("https:") || model.getIcon().startsWith("http:")) {
                if (model.getBitmap() == null) {
                    new GetImage(icon).execute(model);
                } else {
                    icon.setImageBitmap(model.getBitmap());
                }
            } else {
                icon.setImageDrawable(context.getResources().getDrawable(R.drawable.icon));
            }
        } else {
            icon.setImageDrawable(context.getResources().getDrawable(R.drawable.icon));
        }

        title.setText(model.getTitle());
        body.setText(Html.fromHtml(model.getDescription()));
        body.setMovementMethod(LinkMovementMethod.getInstance());

        SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy");
        Date creationdate = Utils.convertEpochToDate(Integer.valueOf(model.getTimestamp())) ;
        String dateee = format.format(creationdate) ;
        date.setText(dateee);

        if(modelList.get(position).getOnclick_Action() != null && !modelList.get(position).getOnclick_Action().trim().isEmpty()) {
            notificationLayout.setTag(position);
            notificationLayout.setOnClickListener(this);
        }

        return v;
    }


    private String getDate(String timestamp) {
        String date = null;
        if(timestamp.contains("T")) {
            String[] temp = timestamp.split("T");
            String[] spllitDate = temp[0].split("-");
            date = spllitDate[2]+"-"+getMonth(spllitDate[1])+"-"+spllitDate[0];
        }else{
            String[] temp = timestamp.split(" ");    //05/31/2016 6:27:04 PM
            String[] spllitDate = temp[0].split("/");
            date = spllitDate[1]+"-"+getMonth(spllitDate[0])+"-"+spllitDate[2];
        }
        return date ;
    }

    private String getMonth(String s) {
        return  new DateFormatSymbols().getMonths()[Integer.parseInt(s)-1];
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    public void onClick(View v) {
        int id = v.getId();
        String onclick ,purpose = null,action ;
        Intent intent = null;
        Uri uri ;

        switch (id){
            case R.id.notification_layout :
                int index = (int) v.getTag();
                onclick = modelList.get(index).getOnclick_Action();
                intent  = Utils.getClickAction(onclick);

                String[] temp = onclick.split(":");
                purpose = temp[0];
                action  = temp[1];

                if(intent != null) {
                    try {
                        context.startActivity(intent);
                    } catch (android.content.ActivityNotFoundException ex) {
                        Toast.makeText(context,context.getResources().getString(R.string.play_store_error), Toast.LENGTH_SHORT).show();
                        if( Objects.equals(purpose, "OPEN_PLAYSTORE") ) {
                            Intent intent1 = new Intent(Intent.ACTION_VIEW);
                            intent1.setData(Uri.parse("https://play.google.com/store/apps/details?id="+action));
                            context.startActivity(intent1);
                        }
                    }
                }
            break;
        }
    }

    private class GetImage extends  AsyncTask<NotificationModel , Void,NotificationModel>{

        ImageView  icon;
        GetImage(ImageView icon) {
            this.icon= icon ;
        }

        @Override
        protected NotificationModel doInBackground(NotificationModel... params) {
            NotificationModel model = params[0];
            String Url = model.getIcon();
            try {
                InputStream inputStream = new URL(Url).openStream();
                model.setBitmap(BitmapFactory.decodeStream(inputStream));
            } catch (IOException e) {
                e.printStackTrace();
            }
            return model;
        }

        @Override
        protected void onPostExecute(NotificationModel model) {
            super.onPostExecute(model);
            if(model.getBitmap() != null)
                    icon.setImageBitmap(model.getBitmap());

        }
    }
}
