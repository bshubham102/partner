package com.renewbuy.partners.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.renewbuy.partners.R;
import com.renewbuy.partners.modelClass.DrawerModel;

import java.util.List;

/**
 * Created by shubham on 24/8/16.
 */
public class DrawerAdapter extends ArrayAdapter<DrawerModel> {
    private Context context;
    private int resource;
    private List<DrawerModel> titleList;
    private LayoutInflater inflater;

    public DrawerAdapter(Context context, int resource, List<DrawerModel> titleList) {
        super(context, resource, titleList);
        this.context = context;
        this.resource = resource;
        this.titleList = titleList;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return titleList.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = null;
        ViewHolder viewHolder = null;

        if(convertView == null){
            view = inflater.inflate(resource,parent,false);
            viewHolder = new ViewHolder();
            viewHolder.name = (TextView) view.findViewById(R.id.drawer_title);
            viewHolder.icon = (ImageView) view.findViewById(R.id.drawer_icon);
            view.setTag(viewHolder);
        }else{
            view = convertView;
            viewHolder = (ViewHolder) view.getTag();
        }

        viewHolder.name.setText(titleList.get(position).getTitle());
        viewHolder.icon.setImageDrawable(context.getResources().getDrawable(titleList.get(position).getResourceId()));

        return view;
    }

    static class ViewHolder{
        TextView name;
        ImageView icon ;
    }
}
