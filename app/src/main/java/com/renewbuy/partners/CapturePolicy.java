package com.renewbuy.partners;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.graphics.pdf.PdfRenderer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.renewbuy.mercury.Leads;
import com.renewbuy.mercury.Utility;
import com.renewbuy.mercury.callBacks.CallBack;
import com.renewbuy.mercury.models.ErrorModel;
import com.renewbuy.mercury.models.LeadModel;
import com.renewbuy.partners.Adapter.DialogAdapter;
import com.renewbuy.partners.modelClass.DialogModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class CapturePolicy extends Fragment {

    static final int REQUEST_CAMERA = 0;
    static final int REQUEST_GALLERY = 1;
    static final int REQUEST_DOCUMENT = 2;

    private Context context ;
    private LinearLayout leftLayout , rightLayout ;

    private ImageView imageView1 ,imageView2 , imageView3 , pdfIndicator1, pdfIndicator2, pdfIndicator3 , help;
    private ArrayList<String> mPhotoPaths = new ArrayList<String>();
    private Uri mCurrentPhotoUri, selectedImageUri;
    private String selectedImagePath, mCurrentPhotoPath, mCustomerEmail, mCustomerPhone , mCustomerPAN;

    private CardView mPanCardView;
    private TextInputLayout mEmailLayout, mContactLayout, mPanLayout ;
    private EditText mEmail, mContact ,mPan;
    private TextView mHelpText, mPolicyDate , mPolicyMonth , mPolicyYear , mPolicyError;
    private Button mUploadBtn;

    private String resubmitEmail = null, resubmitMobile = null ;
    public  boolean isResubmitFlag = false ;
    private int lead_id ;

    private ProgressDialog dialog ;

    private ScrollView mscrollView;

    // Flag for panCard Field is visible or not
    private boolean isPanShow ;

    private String reqURL = "";

    ArrayList<String> datelist , monthlist , yearlist ;

    public static CapturePolicy instance;

    public static CapturePolicy getInstance(){
        if ( instance == null) {
            instance = (CapturePolicy) instantiate(MainActivity.activity,"Capture Policy");
        }
        return instance;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_capture_policy, container, false);

        //get the context of Parent Activity
        context = getActivity();

        //Intailize the Views
        init(v);

        //Url for upload files
        reqURL = Api.getUrl(MainActivity.activity, R.string.vehicles_url);

        //Check It is a 'VAHAN' user or NOT
        checkVAHAN();

        //set Click Listener
        setClickListener();

        instance = this ;
        return v;
    }

    public void checkVAHAN() {
        String partnerCode  = Utils.getStringFromPreferences(context,Utils.executivePartnerCode) ;
        if(partnerCode != null){
            partnerCode = partnerCode.toLowerCase() ;
            if(partnerCode.startsWith("vhn") || partnerCode.startsWith("vahan")) {
                mPanCardView.setVisibility(View.VISIBLE);
                mPan.setFilters(new InputFilter[]{new InputFilter.AllCaps() });
                isPanShow = true ;
            }
        }
    }

    private void init(View v) {

        //Intialize ScrollView
        mscrollView = (ScrollView) v.findViewById(R.id.scrollCapture);

        //Intialize the layout in upload Section
        leftLayout     = (LinearLayout) v.findViewById(R.id.leftLayout);
        rightLayout    = (LinearLayout) v.findViewById(R.id.rightLayout);

        //Intialize the upload image
        imageView1     = (ImageView) v.findViewById(R.id.imageView1);
        imageView2     = (ImageView) v.findViewById(R.id.imageView2);
        imageView3     = (ImageView) v.findViewById(R.id.imageView3);

        //Set Default Drawable
        imageView1.setImageDrawable(getDefaultCamera());
        imageView2.setImageDrawable(getDefaultCamera());
        imageView3.setImageDrawable(getDefaultCamera());

        //Intialize the pdf indicator in upload photo type
        pdfIndicator1  = (ImageView) v.findViewById(R.id.pdf_indicator1);
        pdfIndicator2  = (ImageView) v.findViewById(R.id.pdf_indicator2);
        pdfIndicator3  = (ImageView) v.findViewById(R.id.pdf_indicator3);

        //intialize the view in upper section
        help           = (ImageView) v.findViewById(R.id.image_info);
        mHelpText      = (TextView) v.findViewById(R.id.list_header);
        mHelpText.setText(getResources().getString(R.string.upload_details));

        /**
         * Intialize the Pan Card Section
         * @purpose: It willl appear only for VAHAN User
         */
        mPanCardView = (CardView) v.findViewById(R.id.card_view4);

        //Intialize the textInputLayout for display errors
        mEmailLayout   = (TextInputLayout) v.findViewById(R.id.input_layout_email);
        mContactLayout = (TextInputLayout) v.findViewById(R.id.input_layout_contact);
        mPanLayout     = (TextInputLayout) v.findViewById(R.id.input_layout_pan);

        //Intialize the EditTexts for Input Values
        mEmail   = (EditText) v.findViewById(R.id.input_email);
        mContact = (EditText) v.findViewById(R.id.input_contact);
        mPan     = (EditText) v.findViewById(R.id.input_pan);

        //Intialize the Policy Date , Month ,Year TextViews
        mPolicyDate   = (TextView) v.findViewById(R.id.txt_policy_date);
        mPolicyMonth  = (TextView) v.findViewById(R.id.txt_policy_month);
        mPolicyYear   = (TextView) v.findViewById(R.id.txt_policy_year);
        mPolicyError  = (TextView) v.findViewById(R.id.txt_policy_date_error);

        //Intialize the Submit Button
        mUploadBtn = (Button) v.findViewById(R.id.btn_upload_policy);

    }

    private Drawable getDefaultCamera(){
        return context.getResources().getDrawable(R.drawable.ic_click_camera);
    }

    private void setClickListener() {
        imageView1.setOnClickListener(new MyCLickListener());
        imageView2.setOnClickListener(new MyCLickListener());
        imageView3.setOnClickListener(new MyCLickListener());

        help.setOnClickListener(new MyCLickListener());

        mPolicyDate.setOnClickListener(new MyCLickListener());
        mPolicyMonth.setOnClickListener(new MyCLickListener());
        mPolicyYear.setOnClickListener(new MyCLickListener());

        mUploadBtn.setOnClickListener(new MyCLickListener());
    }

    private class MyCLickListener implements View.OnClickListener{
        @Override
        public void onClick(View v) {
            int id = v.getId();
            switch(id) {
                case R.id.image_info:
                    String[] texts = {
                      "Take photo of the policy keeping on a flat surface (max 3 photos)",
                      "You can also upload from your phone gallery (image or pdf)",
                      "Add customers email, mobile and policy expiry date and submit",
                      "Check status in “My Referral” section",
                      "Get paid on successful transactions"
                    };

                    int[] resources = {
                      R.drawable.ic_click_tut_first ,
                      R.drawable.ic_click_tut_two,
                      R.drawable.ic_click_tut_three,
                      R.drawable.ic_click_tut_four,
                      R.drawable.ic_click_tut_five
                    };

                    Utils.ShowDialog(context,context.getResources().getString(R.string.click_n_upload),texts,resources);
                break;

                case R.id.imageView1:
                    if(imageView1.getDrawable().getConstantState() == getDefaultCamera().getConstantState())
                        intentForCapturingImage();
                    else
                        optionOnImageViewClick(0);
                break;

                case R.id.imageView2:
                    if(imageView2.getDrawable().getConstantState() == getDefaultCamera().getConstantState())
                        intentForCapturingImage();
                    else
                        optionOnImageViewClick(1);
                break;

                case R.id.imageView3:
                    if(imageView3.getDrawable().getConstantState() == getDefaultCamera().getConstantState())
                        intentForCapturingImage();
                    else
                        optionOnImageViewClick(2);
                break;

                case R.id.txt_policy_date:
                    Utils.hideKeyboard(context);
                    datelist = Utils.getDateList();
                    Utils.selectDropDown(context,"Select Date",mPolicyDate,datelist);
                break;

                case R.id.txt_policy_month:
                     Utils.hideKeyboard(context);
                     monthlist = Utils.getMonthList();
                     Utils.selectDropDown(context,"Select Month",mPolicyMonth,monthlist);
                break;

                case R.id.txt_policy_year:
                    Utils.hideKeyboard(context);
                    yearlist = Utils.getYearList();
                    Utils.selectDropDown(context,"Select Year",mPolicyYear,yearlist);
                break;

                case R.id.btn_upload_policy:

                    String dateDefaultText  = getResources().getString(R.string.dd);
                    String monthDefaultText = getResources().getString(R.string.mm);
                    String yearDefaultText  = getResources().getString(R.string.yyyy);

                    String mUserEmail   = mEmail.getText().toString();
                    String mUserContact = mContact.getText().toString();
                    String mUserPolicyDate  = mPolicyDate.getText().toString();
                    String mUserPolicyMonth = mPolicyMonth.getText().toString();
                    String mUserPolicyYear  = mPolicyYear.getText().toString();
                    String mUserPAN =  mPan.getText().toString();

                    if (!isUploadedPolicy()) {
                        Utils.displayAlert(context,"Error", "Please select atleast 1 photo");
                        return ;
                    }

                    if (!Utils.isValidEmailId(mUserEmail)) {
                        setError(mEmailLayout,"Please enter a valid Email Id");
                        return;
                    } else {
                        hideError(mEmailLayout);
                    }

                    if (!Utils.isValidMobileNo(mUserContact)) {
                        setError(mContactLayout,"Please enter a valid mobile number");
                        return;
                    } else {
                        hideError(mContactLayout);
                    }

                    if(mUserPolicyDate.equals(dateDefaultText) ){
                        mPolicyError.setVisibility(View.VISIBLE);
                        mPolicyError.setText("Please Select Date");
                        return;
                    } else {
                        mPolicyError.setVisibility(View.GONE);
                    }

                    if(mUserPolicyMonth.equals(monthDefaultText) ){
                        mPolicyError.setVisibility(View.VISIBLE);
                        mPolicyError.setText("Please Select Month");
                        mPolicyMonth.requestFocus();
                        return ;
                    } else {
                        mPolicyError.setVisibility(View.GONE);
                    }

                    if(mUserPolicyYear.equals(yearDefaultText) ){
                        mPolicyError.setVisibility(View.VISIBLE);
                        mPolicyError.setText("Please Select Year");
                        mPolicyYear.requestFocus();
                        return ;
                    } else {
                        mPolicyError.setVisibility(View.GONE);
                    }

                    if(resubmitEmail != null){
                        if(!resubmitEmail.equals(mUserEmail)){
                            setError(mEmailLayout,"You cannot change Email");
                            return;
                        }else{
                            hideError(mEmailLayout);
                        }
                    }
                    if(resubmitMobile != null){
                        if(!resubmitMobile.equals(mUserContact)){
                            setError(mContactLayout,"You cannot change mobile number");
                            return;
                        }else{
                            hideError(mContactLayout);
                        }
                    }

                    if( isPanShow && mUserPAN.length() > 0 ) {
                        if( mUserPAN.length() < 10 ) {
                           setError(mPanLayout,"Please enter the valid PAN Card Number");
                           return;
                        }
                        else if( mUserPAN.length() > 10 ) {
                           setError(mPanLayout,"Please enter the valid PAN Card Number");
                           return;
                        }
                        else {
                           hideError(mPanLayout);
                           // Upload the policy
                           if(Utils.isInternetAvailable(MainActivity.activity)) {
                               dialog = ProgressDialog.show(getActivity(), "", "Uploading policy...", true);
                               try {
                                   multipartRequestV3(mPhotoPaths);
                               } catch (Exception e) {
                                   e.printStackTrace();
                                   if (dialog != null)
                                       dialog.dismiss();
                                   Utils.displayAlert(context, "Error", e.getMessage());
                               }
                           } else {
                               Toast.makeText(context, "No Internet Available", Toast.LENGTH_SHORT).show();
                           }
                        }
                    } else {
                        if (!Utils.checkDateIsInRightFormat(mUserPolicyDate, mUserPolicyMonth, mUserPolicyYear)) {
                            mPolicyError.setVisibility(View.VISIBLE);
                            mPolicyError.setText("Date is not Correct");
                            return;
                        } else {
                            mPolicyError.setVisibility(View.GONE);
                            hideError(mPanLayout);
                            // Upload the policy
                            if(Utils.isInternetAvailable(MainActivity.activity)) {
                                dialog = ProgressDialog.show(getActivity(), "", "Uploading policy...", true);
                                try {
                                    multipartRequestV3(mPhotoPaths);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    if (dialog != null)
                                        dialog.dismiss();
                                    Utils.displayAlert(context, "Error", e.getMessage());
                                }
                            } else {
                                Toast.makeText(context, "No Internet Available", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                break;

            }
        }
    }

    private void setError(TextInputLayout layout , String msg){
        layout.setErrorEnabled(true);
        layout.setError(msg);
        layout.requestFocus();
    }
    private void hideError(TextInputLayout layout){
        layout.setError(null);
        layout.clearFocus();
        layout.setErrorEnabled(false);
    }

    public void intentForCapturingImage() {
        Activity activity = MainActivity.activity;
        final AlertDialog.Builder dlgAlert = new AlertDialog.Builder(activity);

        ListView lv = new ListView(activity);
        lv.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        ArrayList<DialogModel>  models = new ArrayList<>();
        models.add(new DialogModel("Camera",R.drawable.ic_click_overlay_camera));
        models.add(new DialogModel("Gallery",R.drawable.ic_click_overlay_gallery));
        models.add(new DialogModel("Pdf Document",R.drawable.ic_click_overlay_pdf));

        DialogAdapter adapter = new DialogAdapter(activity, R.layout.dialog_row_layout, models);
        lv.setAdapter(adapter);

        dlgAlert.setView(lv);
        dlgAlert.setCancelable(true);
        final AlertDialog dialog = dlgAlert.show();

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                dialog.dismiss();

                switch (position) {
                    case 0:
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (context.checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ) {
                                requestPermissions(new String[]{Manifest.permission.CAMERA},REQUEST_CAMERA);
                            } else {
                                startCameraOperation();
                            }
                        } else {
                            startCameraOperation();
                        }
                    break;

                    case 1:
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (context.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ) {
                                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},REQUEST_GALLERY);
                            } else {
                                startGalleryOperation();
                            }
                        } else {
                            startGalleryOperation();
                        }
                    break;

                    case 2:
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (context.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ) {
                                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},REQUEST_DOCUMENT);
                            } else {
                                PdfOperation();
                            }
                        } else {
                            PdfOperation();
                        }
                    break;
                }

            }
        });
    }

    private void startCameraOperation(){
        try {
            File photoFile = Utils.createImageFile(getActivity());
            // Save a file: path for use with ACTION_VIEW intents
            mCurrentPhotoPath = photoFile.getAbsolutePath();
            mCurrentPhotoUri = Uri.fromFile(photoFile);

            Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            captureIntent.putExtra(MediaStore.EXTRA_OUTPUT, mCurrentPhotoUri);
            getActivity().startActivityForResult(captureIntent, Utils.REQUEST_IMAGE_CAPTURE);
        }catch (IOException ex) {
            // Error occurred while creating the File
            ex.printStackTrace();
            Log.e(Config.TAG, "Exception: " + ex.getMessage());
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void startGalleryOperation(){
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        //photoPickerIntent.putExtra(Intent.EXTRA_LOCAL_ONLY,true);
        getActivity().startActivityForResult(photoPickerIntent, Utils.REQUEST_IMAGE_GALLERY);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void PdfOperation(){
        // special intent for Samsung file manager
        Intent sIntent = new Intent("com.sec.android.app.myfiles.PICK_DATA");
        // if you want any file type, you can skip next line
        sIntent.putExtra("CONTENT_TYPE", "application/pdf");
       // sIntent.putExtra(Intent.EXTRA_LOCAL_ONLY,true);
        sIntent.addCategory(Intent.CATEGORY_DEFAULT);

        Intent docIntent = new Intent(Intent.ACTION_GET_CONTENT);
        //docIntent.putExtra(Intent.EXTRA_LOCAL_ONLY,true);
        docIntent.setType("application/pdf");

        if (context.getPackageManager().resolveActivity(sIntent, 0) != null){
            // it is device with samsung file manager
            getActivity().startActivityForResult(sIntent, Utils.REQUEST_IMAGE_DOCUMENT);
        }
        else if (context.getPackageManager().resolveActivity(docIntent, 0) != null){
            getActivity().startActivityForResult(docIntent, Utils.REQUEST_IMAGE_DOCUMENT);
        }else{
            Utils.displayAlert(context,"Error!!!", "");
        }
    }

    /**
     * Atleast One Policy Document must be Uploaded
     * @return : true if doucment is uploaded otherwise false
     */
    boolean isUploadedPolicy(){
        if(mPhotoPaths.size()<=0){
            return false;
        }

        for(int i=0; i<mPhotoPaths.size();i++){
            if(!mPhotoPaths.get(i).isEmpty())
                return true;
        }

        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            switch (requestCode) {
                case REQUEST_CAMERA:
                     startCameraOperation();
                break;

                case REQUEST_GALLERY:
                    startGalleryOperation();
                break;

                case REQUEST_DOCUMENT:
                    PdfOperation();
                break;
            }
        } else {
            Toast.makeText(context,"Permission Denied",Toast.LENGTH_LONG).show();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case Utils.REQUEST_IMAGE_CAPTURE:
                    selectedImageUri = mCurrentPhotoUri;
                    selectedImagePath = Utils.getRealPathFromURI(getActivity(), selectedImageUri);
                break;

                case Utils.REQUEST_IMAGE_GALLERY:
                    selectedImageUri = data == null ? null : data.getData();
                    selectedImagePath = Utils.getImagePathFromUri(getActivity(), selectedImageUri);
                break;

                case Utils.REQUEST_IMAGE_DOCUMENT:
                    selectedImageUri = data == null ? null : data.getData();
                    selectedImagePath = Utils.getPath(MainActivity.activity, selectedImageUri);
                    break;
            }
            if (selectedImagePath != null)
                new ImageCompressionAsyncTask(false).execute(selectedImagePath);
            else {
                Utils.displayAlert(context,"Error", "This is not a valid photo.\nPlease select another file.");
            }
        }
    }

    public void optionOnImageViewClick(final int i){
        Activity activity = MainActivity.activity;
        final AlertDialog.Builder dlgAlert = new AlertDialog.Builder(activity);

        ListView lv = new ListView(activity);
        lv.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        ArrayList<DialogModel>  models = new ArrayList<>();
        models.add(new DialogModel("View",R.drawable.ic_upload_detail_image_view));
        models.add(new DialogModel("Remove",R.drawable.ic_upload_detail_image_delete));

        DialogAdapter adapter = new DialogAdapter(activity, R.layout.dialog_row_layout, models);
        lv.setAdapter(adapter);

        dlgAlert.setView(lv);
        dlgAlert.setCancelable(true);
        final AlertDialog dialog = dlgAlert.show();

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                dialog.dismiss();

                switch (position) {
                    case 0:
                        viewImage(mPhotoPaths.get(i));
                        break;

                    case 1:
                        if (i == 0) {
                            imageView1.setImageDrawable(getDefaultCamera());
                            pdfIndicator1.setVisibility(View.GONE);
                        }
                        else if (i == 1) {
                            imageView2.setImageDrawable(getDefaultCamera());
                            pdfIndicator2.setVisibility(View.GONE);
                        }
                        else {
                            imageView3.setImageDrawable(getDefaultCamera());
                            pdfIndicator3.setVisibility(View.GONE);
                        }

                        mPhotoPaths.remove(i);
                        mPhotoPaths.add(i, "");

                        break;
                }
                if( rightLayout.getVisibility() == View.VISIBLE) {
                    if( (imageView1.getDrawable().getConstantState() == getDefaultCamera().getConstantState()) &&
                        (imageView2.getDrawable().getConstantState() == getDefaultCamera().getConstantState()) &&
                        (imageView3.getDrawable().getConstantState() == getDefaultCamera().getConstantState()) ) {
                        rightLayout.setVisibility(View.GONE);
                    }
                }
            }
        });
    }

    void viewImage(String img) {
        File file=new File(img);
        Uri fileUri = Uri.fromFile(file);

        if(Utils.isImageFile(fileUri)) {
            Activity activity = MainActivity.activity;
            final AlertDialog.Builder dlgAlert = new AlertDialog.Builder(activity);

            ImageView iv = new ImageView(activity);
            iv.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            iv.setImageBitmap(BitmapFactory.decodeFile(img));

            dlgAlert.setView(iv);
            dlgAlert.setCancelable(true);
            dlgAlert.show();
        } else if (Utils.isDocumentFile(fileUri)){
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(fileUri, Utils.getMimeType(fileUri));
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);
        }
    }

    private class ImageCompressionAsyncTask extends AsyncTask<String, Void, String> {
        private boolean fromGallery;
        private String TAG = "ImageCompressionAsyncTask";
        private ProgressDialog pd;

        public ImageCompressionAsyncTask(boolean fromGallery) {
            this.fromGallery = fromGallery;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = ProgressDialog.show(getActivity(), "", "Please wait", true);
        }

        @Override
        protected String doInBackground(String... params) {
            String filePath;

            File file = new File(selectedImagePath);

            if (Utils.isImageFile(Uri.fromFile(file))) {
                filePath = compressImage(params[0]);
            } else if (Utils.isDocumentFile(Uri.fromFile(file))) {
                filePath = params[0];
            } else {
                filePath = null;
            }

            return filePath;
        }

        public String compressImage(String filePath) {
            Bitmap scaledBitmap = null;

            if (filePath == null) {
                return null;
            }
            if (filePath.startsWith("http")) {
                return null;
            }
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = false;
            Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

            int actualHeight = options.outHeight;
            int actualWidth = options.outWidth;

            if (actualHeight == 0 && actualWidth == 0) {
                return null;
            }

            float maxHeight = 1200.0f;
            float maxWidth = 1200.0f;
            float imgRatio = actualWidth / actualHeight;
            float maxRatio = maxWidth / maxHeight;

            if (actualHeight > maxHeight || actualWidth > maxWidth) {
                if (imgRatio < maxRatio) {
                    imgRatio = maxHeight / actualHeight;
                    actualWidth = (int) (imgRatio * actualWidth);
                    actualHeight = (int) maxHeight;
                } else if (imgRatio > maxRatio) {
                    imgRatio = maxWidth / actualWidth;
                    actualHeight = (int) (imgRatio * actualHeight);
                    actualWidth = (int) maxWidth;
                } else {
                    actualHeight = (int) maxHeight;
                    actualWidth = (int) maxWidth;
                }
            }

            options.inSampleSize = Utils.calculateInSampleSize(options, actualWidth, actualHeight);
            options.inJustDecodeBounds = false;
            options.inDither = false;
            options.inPurgeable = true;
            options.inInputShareable = true;
            options.inTempStorage = new byte[16 * 1024];

            try {
                scaledBitmap = BitmapFactory.decodeFile(filePath, options);
            } catch (OutOfMemoryError exception) {
                exception.printStackTrace();
            }

            FileOutputStream out = null;
            String filename = getFilename();
            try {
                out = new FileOutputStream(filename);
                scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            return filename;

        }

        public String getFilename() {
            File file = new File(Environment.getExternalStorageDirectory().getPath(), "RenewBuy/Images");
            if (!file.exists()) {
                file.mkdirs();
            }
            String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
            return uriSting;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (result == null) {
                Utils.displayAlert(context,"Error", "This is not a valid photo.\nPlease select another file.");
                pd.dismiss();
                return;
            }
            File file=new File(selectedImagePath);
            Uri fileUri = Uri.fromFile(file);

            //File Size in MegaBytes
            long fileSize = (file.length() / 1024) / 1024 ;
            Log.i("File Size",""+fileSize);

            if(fileSize > 2.0){
                Utils.ShowSuccessDialog(context,"File size should be less than 2MB",Utils.ERROR_DIALOG);
                pd.dismiss();
                return;
            }

            if (imageView1.getDrawable().getConstantState() == getDefaultCamera().getConstantState()) {

                if (Utils.isImageFile(fileUri)) {

                    Utils.setPic(imageView1, selectedImagePath);
                    pdfIndicator1.setVisibility(View.GONE);

                } else if (Utils.isDocumentFile(fileUri)) {

                    render(imageView1, file);
                    pdfIndicator1.setVisibility(View.VISIBLE);

                }

                if (mPhotoPaths.size() >= 1)
                    mPhotoPaths.remove(0);

                mPhotoPaths.add(0, result);
                rightLayout.setVisibility(View.VISIBLE);

            } else if (imageView2.getDrawable().getConstantState() == getDefaultCamera().getConstantState()){

                if (Utils.isImageFile(fileUri)) {

                    Utils.setPic(imageView2, selectedImagePath);
                    pdfIndicator2.setVisibility(View.GONE);

                } else if (Utils.isDocumentFile(fileUri)) {

                    render(imageView2, file);
                    pdfIndicator2.setVisibility(View.VISIBLE);

                }

                if (mPhotoPaths.size() >= 2)
                    mPhotoPaths.remove(1);
                mPhotoPaths.add(1, result);

            } else if (imageView3.getDrawable().getConstantState() == getDefaultCamera().getConstantState()) {

                if (Utils.isImageFile(fileUri)) {

                    Utils.setPic(imageView3, selectedImagePath);
                    pdfIndicator3.setVisibility(View.GONE);

                } else if (Utils.isDocumentFile(fileUri)) {

                    render(imageView3, file);
                    pdfIndicator3.setVisibility(View.VISIBLE);

                }

                if (mPhotoPaths.size() >= 3)
                    mPhotoPaths.remove(2);

                mPhotoPaths.add(2, result);
            }

            pd.dismiss();
        }
    }

    private PdfRenderer renderer;
    int count ;
    int REQ_WIDTH;
    int REQ_HEIGHT;
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void render(ImageView view , File f) {
        try {

             REQ_WIDTH  = view.getWidth();
             REQ_HEIGHT = view.getHeight() ;
             renderer   = new PdfRenderer(ParcelFileDescriptor.open(f, ParcelFileDescriptor.MODE_READ_ONLY));
             //count      = renderer.getPageCount();
             display(view);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void display(ImageView view){

        Bitmap bitmap = Bitmap.createBitmap(REQ_WIDTH, REQ_HEIGHT, Bitmap.Config.ARGB_8888);
        Matrix m = view.getImageMatrix();
        renderer.openPage(0).render(bitmap, null, null, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY);
        view.setImageMatrix(m);
        view.setImageBitmap(bitmap);
        renderer.close();
    }

    /**
     *  Some Data is deleted and keep in temp.txt file
     */

    /**
     * Method to Upload Multipart Data with plain-text in v2 Endpoint
     * @param filepath  list of file you want to upload
     * @ParseException  throw exception if dont have proper month
     */
    public void multipartRequestV3(final ArrayList<String> filepath) throws ParseException {
        String urlUpload ;
        int requestMethod ;

        if(isResubmitFlag){
            urlUpload = Config.DOMAIN+"/api/v2/leads/"+lead_id+"/?embed=documents";
            requestMethod = Request.Method.PATCH;
        }else{
            urlUpload = Config.DOMAIN+"/api/v2/leads/";
            requestMethod = Request.Method.POST;
        }

        Map<String, File>   mFilePartData = new HashMap<>();
        Map<String, String> mStringPart   = new HashMap<>();
        Map<String, String> headers       = new HashMap<>();

        mCustomerEmail = mEmail.getText().toString();
        mCustomerPhone = mContact.getText().toString() ;
        mCustomerPAN   = mPan.getText().toString();
        int month      =  Utils.getMonthNumber(mPolicyMonth.getText().toString());
        String policy_expiry = mPolicyYear.getText()+"-"+month+"-"+mPolicyDate.getText();

        String boundary = "*****" + Long.toString(System.currentTimeMillis()) + "*****";
        String auth = "Token " + Utils.getStringFromPreferences(context,Utils.authTokenKey);
        String executive_code = Utils.getStringFromPreferences(context,Utils.executiveCodeKey);

        // Header Details
        headers.put("Content-Type", "multipart/form-data; boundary=" + boundary);
        headers.put("Authorization", auth);
        headers.put("API-SECRET-KEY",SplashScreen.API_KEY);
        headers.put("App-ID",SplashScreen.APP_ID);

        // plain-text Body
        if(!isResubmitFlag){
            mStringPart.put("email", mCustomerEmail);
            mStringPart.put("mobile", mCustomerPhone);
            mStringPart.put("executive", executive_code);
        }
        mStringPart.put("product_type", "1");
        mStringPart.put("lead_status", "1");
        // Mulitpart data
        for (int i = 0; i <= filepath.size()-1 ; i++) {
            mStringPart.put("documents["+i+"]type","3");
            mFilePartData.put("documents["+i+"]file",new File(filepath.get(i)));
        }

        mStringPart.put("prev_policy_expiry", policy_expiry);

        MultipartUpload upload = new MultipartUpload(requestMethod, context, urlUpload, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.i("onResponse: ",response.toString());

                if(isResubmitFlag)
                    setEnableTextField(true);

                isResubmitFlag = false ;

                Api.refreshExecutiveData(getActivity());

                mEmail.setText(null);
                mContact.setText(null);

                if(isPanShow)
                 mPan.setText(null);

                setDefaultValues();

                dialog.dismiss();
                Utils.ShowSuccessDialog(context,"Thanks for upload. Please review the status in Refferals.",Utils.SUCCESS_DIALOG);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("onErrorResponse: ",resolveNetWorkError(error));

                dialog.dismiss();
                Utils.ShowSuccessDialog(context,resolveNetWorkError(error),Utils.ERROR_DIALOG);
            }
        },mFilePartData,mStringPart,headers);

        upload.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.MINUTES.toMillis(Utility.REQUEST_TIMEOUT), DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ApplicationController.getInstance().addToRequestQueue(upload);
    }

    // Decode VolleyError in String
    public static String resolveNetWorkError(VolleyError error){
        String ErrorMessage = "";

        NetworkResponse response = error.networkResponse;
        if (response == null) {
            Log.i("TAG","Unable to connect Server");
            return "Unable to connect Server";
        }

        JSONObject jsonResponse;
        try {
            String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            jsonResponse = new JSONObject(jsonString);
            String errorMessage = jsonResponse.optString("detail");
            if (!errorMessage.isEmpty()) {
                ErrorMessage  = errorMessage ;
            } else {
                Iterator iterator = jsonResponse.keys();
                String iteratorString = "";
                while (iterator.hasNext()) {
                    String key = (String) iterator.next();
                    Log.i("Error", jsonResponse.get(key).toString());
                    iteratorString+= jsonResponse.get(key).toString() ;
                    break;
                }
                ErrorMessage = iteratorString ;
            }
        } catch (UnsupportedEncodingException | JSONException e) {
            VolleyLog.e("Response:%n %s", error.getMessage());
            ErrorMessage = "Error from server";
        }
        Log.i("TAG", ErrorMessage);
        return ErrorMessage ;
    }



    public void onResubmitClick(int lead_id) {
        this.lead_id = lead_id ;
        mPhotoPaths = new ArrayList<>();

        dialog = ProgressDialog.show(getActivity(), "", "Getting policy details...", true);
        Leads leads = new Leads();
        leads.getLeadByID(lead_id, new CallBack<LeadModel>() {
            @Override
            public void onResponse(LeadModel model) {
                if(model != null){
                    isResubmitFlag = true ;
                    resubmitEmail = model.getEmail() ;
                    resubmitMobile = model.getMobile() ;

                    mEmail.setText(resubmitEmail);
                    mContact.setText(resubmitMobile);

                    setEnableTextField(false);

                    if(model.getPrev_policy_expiry() != null){
                        String date[] = model.getPrev_policy_expiry().split("-");
                        String Date  = date[2];
                        int Month = Integer.parseInt(date[1])-1;
                        String Year  = date[0];

                        mPolicyDate.setText(Date);
                        mPolicyMonth.setText(Utils.getMonthList().get(Month));
                        mPolicyYear.setText(Year);
                    }
                }
                dialog.dismiss();
            }

            @Override
            public void onError(ErrorModel errorModel) {
                dialog.dismiss();
                Toast.makeText(context, errorModel.getError(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void resetResubmitLead(){
        isResubmitFlag = false ;
        resubmitEmail = null;
        resubmitMobile = null ;

        setEnableTextField(true);


        mEmail.setText(null);
        mContact.setText(null);

        if(isPanShow)
            mPan.setText(null);

        setDefaultValues();
    }

    //set enable email and mobile number fields
    private void setEnableTextField(boolean isEnable){
        mEmail.setEnabled(isEnable);
        mContact.setEnabled(isEnable);
    }

    //reset all fields
    private void setDefaultValues(){

        resubmitEmail = null;
        resubmitMobile = null ;

        mPhotoPaths.clear();

        mPolicyDate.setText(context.getResources().getString(R.string.dd));
        mPolicyMonth.setText(context.getResources().getString(R.string.mm));
        mPolicyYear.setText(context.getResources().getString(R.string.yyyy));

        imageView1.setImageDrawable(getDefaultCamera());
        imageView2.setImageDrawable(getDefaultCamera());
        imageView3.setImageDrawable(getDefaultCamera());

        pdfIndicator1.setVisibility(View.GONE);
        pdfIndicator2.setVisibility(View.GONE);
        pdfIndicator3.setVisibility(View.GONE);

        rightLayout.setVisibility(View.GONE);
    }
}
