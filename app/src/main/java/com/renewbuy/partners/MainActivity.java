package com.renewbuy.partners;

import android.*;
import android.Manifest;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.VectorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.PersistableBundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.renewbuy.mercury.Authentication;
import com.renewbuy.partners.Adapter.DrawerAdapter;
import com.renewbuy.partners.db.AppDb;
import com.renewbuy.partners.db.ExportDb;
import com.renewbuy.partners.gcm.DeviceDetails;
import com.renewbuy.partners.modelClass.DrawerModel;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements Stats.OnStatUploadClickListener {

    private static final String TAG = "MainActivity";
    private static final int REQUEST_FILE_OPERATION = 1;
    public static int RATING;

    // Wallet Counter Variable
    private int WALLET_COUNTER = 0;

    // Notification Counter variable
    private int COUNTER = 0;

    private int[] tabIcon = {
            R.drawable.stat_selector,
            R.drawable.upload_selector,
            R.drawable.camera_selector,
            R.drawable.user_selector,
            R.drawable.health_selector
    };

    private int[] tabTitle = {
            R.string.stats,
            R.string.upload,
            R.string.capture,
            R.string.refferals,
            R.string.health
    };

    private String[] ActionBarTitle = {
            "My Earning",
            "Issue Policy Now",
            "Click And Upload",
            "Customers List",
            "Dengue Shield"
    };

    private String[] titleArray = {
            "How to Use",
            /*"Training Video",*/
            "Insurer Contacts",
            "FAQs",
            "Check for Updates",
            "Rate Us",
            "Call Us",
            "About Us",
            "Privacy Policy",
            "Terms of Use",
            "Logout"
    };

    private int[] drawerIcon = {
            R.drawable.left_howto_use,
            /*R.drawable.video_png,*/
            R.drawable.garage_filter_insurer_blue,
            R.drawable.faq_icon,
            R.drawable.refresh,
            R.drawable.rate_us,
            R.drawable.call_us,
            R.drawable.about,
            R.drawable.privacy_icon,
            R.drawable.terms,
            R.drawable.logout
    };

    private boolean doublepress;

    private Fragment[] fragList = {
            new Stats(),
            new PolicyQuotesWebView(),
            new CapturePolicy(),
            new TransactionFragment(),
            WebViewFragment.newInstance(true)
    };

    private List<DrawerModel> drawerItem = new ArrayList<>();

    public FragmentManager fragmentManager;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    public  CoordinatorLayout coordinatorLayout;

    private ImageView mProfile;

    // This view is static Because it's using
    // in TransactionFragment.java to hide the Reminder dot from tab layout
    public static ImageView counter;

    public static MainActivity activity;
    private ActionBar actionBar;

    private onBackListener mListener ;

    public static Bitmap bitmap ;


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activity = this;

        if(!Utils.isLoggedIn(this)) {
            showLoginScreen();
            return ;
        }

        Log.i(TAG, "onCreate: ");
        setContentView(R.layout.activity_main);

        //RefreshData
        Api.refreshExecutiveData(MainActivity.activity);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            actionBar = getSupportActionBar();
            setActionTitle(0);
        }

        fragmentManager = getSupportFragmentManager();
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);

        //Set Drawer Item
        setTitleInList();
        DrawerAdapter adapter = new DrawerAdapter(this, R.layout.drawer_row, drawerItem);

        //Header View for ListView
        View header = LayoutInflater.from(this).inflate(R.layout.drawer_header_layout, mDrawerList, false);
        mProfile = (ImageView) header.findViewById(R.id.profile);
        TextView username = (TextView) header.findViewById(R.id.username);
        TextView email = (TextView) header.findViewById(R.id.email);

        username.setText(Utils.getStringFromPreferences(this, Utils.executiveNameKey));
        email.setText(Utils.getStringFromPreferences(this, Utils.emailKey));
        mDrawerList.addHeaderView(header);

        mDrawerList.setAdapter(adapter);

        //setListener
        mDrawerList.setOnItemClickListener(new MyItemCLickListener());

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.open, R.string.close);
        toggle.syncState();

        viewPager = (ViewPager) findViewById(R.id.viewPager);
        if (viewPager != null) {
            viewPager.setOffscreenPageLimit(fragList.length);
        }
        setUpPager();

        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        if (tabLayout != null) {
            tabLayout.setupWithViewPager(viewPager);
            setUpTab();
        }

        // Intialize PlutoSDK Service
        //new DataCollection(true).collect(this);

        // Intialize or Create Database
        AppDb.getInstance(this);

        // Send Device Details
        new DeviceDetails(activity);

        //Get Persistance Notifcations
        Api.getApiNotification(this);

        new ExportDb().exportDatabse(this,AppDb.DATABASE_NAME , "rb_redesign.db");

        //get Notification Intent and show notifcation screen
        savedInstanceState = getIntent().getExtras();
        if(savedInstanceState != null){
           String tab = savedInstanceState.getString(Utils.notificationTab);
           //Log.i("onCreate: ",tab);
           if(tab != null)
               showNotification();
        }

        //request for write/read file for Marshmallow+ android devices
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ) {
                requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},REQUEST_FILE_OPERATION);
            } else {
                setProfileImage();
            }
        }else{
            setProfileImage();
        }
    }

    // set Title for Drawer list items
    private void setTitleInList() {
        for (int i = 0; i <= titleArray.length - 1; i++) {
            drawerItem.add(new DrawerModel(drawerIcon[i], titleArray[i]));
        }
    }

    private void setActionTitle(int pos) {
        if (actionBar != null)
            actionBar.setTitle(Html.fromHtml("<small>" + ActionBarTitle[pos] + "</small>"));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            switch (requestCode) {
                case REQUEST_FILE_OPERATION:
                    setProfileImage();
                break;
            }
        } else {
            Toast.makeText(this,"Permission Denied",Toast.LENGTH_LONG).show();
            String profilePath = Utils.getStringFromPreferences(this, Utils.profileKey);
            ImageLoader imageLoader = ApplicationController.getInstance().getImageLoader();
            imageLoader.get(profilePath, new ImageLoader.ImageListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e(TAG, "Image Load Error: " + error.getMessage());
                }

                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                    if (response.getBitmap() != null) {
                        mProfile.setImageBitmap(Utils.getRoundedCornerBitmap(response.getBitmap(), 175));
                    }
                }
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_menu, menu);

        if (COUNTER > 0) {
            MenuItem menuItem = menu.findItem(R.id.notificationMenu);
            menuItem.setIcon(Utils.buildCounterDrawable(this, COUNTER, R.drawable.notification));
        }

        if (WALLET_COUNTER > 0) {
            MenuItem wallet = menu.findItem(R.id.rb_wallet);
            wallet.setIcon(Utils.buildCounterDrawable(this, WALLET_COUNTER, R.drawable.wallet));
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {

            case R.id.notificationMenu:
                    showNotification();
            return true;

            case R.id.refreshDataMenu:
                    Toast.makeText(MainActivity.this, "Refreshing....", Toast.LENGTH_SHORT).show();
                    Api.refreshExecutiveData(activity);
            return true;

            case R.id.rb_wallet:
                Intent wintent = new Intent(MainActivity.activity, MultiUseActivity.class);
                Bundle wbundle = new Bundle();
                wbundle.putString(MultiUseActivity.SELECT_WALLET, MultiUseActivity.SELECT_WALLET);
                wbundle.putString(MultiUseActivity.TITLE, "My Wallet");
                wintent.putExtras(wbundle);
                startActivity(wintent);

            return true;

            default:
                return false;
        }
    }

    private void setUpTab() {
        invalidateTabMenu();
        tabLayout.getTabAt(0).getCustomView().setSelected(true);

        int screenWidth = getWindowManager().getDefaultDisplay().getWidth() ;
        int count = tabLayout.getTabCount() ;
        for (int i = 0; i <= count-1 ; i++) {
            tabLayout.getTabAt(i).getCustomView().setMinimumWidth(screenWidth / fragList.length-3);
        }
    }

    private void setUpPager() {
        PagerAdapter adapter = new PagerAdapter(fragmentManager);
        for (int i = 0; i <= fragList.length - 1; i++) {
            adapter.addFrag(fragList[i]);
        }
        viewPager.setAdapter(adapter);
        viewPager.setOnPageChangeListener(new PageListener());
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Utils.MANUFACTURER_REQUEST || resultCode == Utils.MODEL_REQUEST || resultCode == Utils.VARIANT_REQUEST
                || resultCode == Utils.CITY_REQUEST) {
            PolicyQuotesWebView.intance.onActivityResult(requestCode, resultCode, data);
        } else if (requestCode == Utils.REQUEST_IMAGE_CAPTURE || requestCode == Utils.REQUEST_IMAGE_DOCUMENT || requestCode == Utils.REQUEST_IMAGE_GALLERY) {
            CapturePolicy.getInstance().onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onBackPressed() {

        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawers();

        }
        else if (viewPager.getCurrentItem() > 0) {
            int pos = viewPager.getCurrentItem() ;
            if(pos == fragList.length-1){
                mListener = (onBackListener) fragList[pos];
                mListener.donBackPress(viewPager);
            }else{

                if(fragList[pos] instanceof CapturePolicy && CapturePolicy.getInstance().isResubmitFlag){
                    CapturePolicy.getInstance().resetResubmitLead();
                    viewPager.setCurrentItem(0);
                } else{
                    // int pos = viewPager.getCurrentItem()-1;
                    // change with pos OR  0 is first page of pager
                    viewPager.setCurrentItem(0);
                }
            }
        }
        else {
            int count = fragmentManager.getBackStackEntryCount();
            if (count > 1) {
                fragmentManager.popBackStack();
            } else {
                closeapp();
            }
        }
    }

    private void closeapp() {
        if (doublepress) {
            finish();
        }
        doublepress = true;
        Toast.makeText(this, "Please press back again to exit", Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doublepress = false;
            }
        }, 2000);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public void invalidateTabMenu() {
        for (int i = 0; i <= tabIcon.length - 1; i++) {
            LinearLayout layout = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);

            ImageView icon = (ImageView) layout.findViewById(R.id.tab_icon);
            TextView title = (TextView) layout.findViewById(R.id.tab_title);
            ImageView counter = (ImageView) layout.findViewById(R.id.tab_counter);

            Resources res = getResources() ;
            Drawable drawable = null;

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                drawable = getDrawable(tabIcon[i]);
            else
                drawable = res.getDrawable(tabIcon[i]);

            icon.setImageDrawable(drawable);

            title.setText(tabTitle[i]);

            if (Utils.hasRefferalsNotfication && getString(tabTitle[i]).equals(getString(R.string.refferals)))
                counter.setVisibility(View.VISIBLE);

            tabLayout.getTabAt(i).setCustomView(layout);
        }
    }

    @Override
    public void onUploadClickListener(boolean isClick,int pos) {
        if (isClick)
            viewPager.setCurrentItem(pos, true);
    }

    //ViewPager Adapter class
    private class PagerAdapter extends FragmentStatePagerAdapter {

        private final List<Fragment> mFragmentList = new ArrayList<>();

        public PagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment) {
            mFragmentList.add(fragment);
        }
    }

    // ViewPager Page Change Listener
    private class PageListener implements ViewPager.OnPageChangeListener {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            Utils.hideKeyboard(MainActivity.this);
            if(CapturePolicy.getInstance().isResubmitFlag)
                CapturePolicy.getInstance().resetResubmitLead();
        }

        @Override
        public void onPageSelected(int position) {

            //Change ActionBar Title
            setActionTitle(position);

            //If Something is new in Refferals add
            if (getString(tabTitle[position]).equals(getString(R.string.refferals))) {
                if (Utils.hasRefferalsNotfication) {
                    LinearLayout linearLayout = (LinearLayout) tabLayout.getTabAt(position).getCustomView();
                    LinearLayout linearLayout2 = (LinearLayout) linearLayout.getChildAt(1);
                    counter = (ImageView) linearLayout2.getChildAt(1);
                }
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {
        }
    }

    //ListView Click Listener
    private class MyItemCLickListener implements AdapterView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            // postion zero is for profile section in Drawer
            //Toast.makeText(MainActivity.this, ""+position, Toast.LENGTH_SHORT).show();
            switch (position) {
                case 0:
                    openProfile();
                    break;

                /*case 1:
                    break;*/

                case 1:
                    openIntroScreen();
                    break;

                case 2:
                    openStaticInWebView(Config.INSURE_CONTACT, "Insurer Contacts");
                    break;

                case 3:
                    openStaticInWebView(Config.FAQ, "FAQs");
                    break;

                case 4:
                    try {
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse("market://details?id=" + getPackageName()));
                        startActivity(intent);
                    } catch (Exception e) {
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=" + getPackageName()));
                        startActivity(intent);
                    }
                    break;

                case 5:
                    RateUs();
                    break;

                case 6:
                    String number = getString(R.string.rb_toll_free);
                    Intent callIntent = new Intent(Intent.ACTION_DIAL);
                    try {
                        callIntent.setData(Uri.parse("tel:" + number));
                        startActivity(callIntent);
                    } catch (Exception e) {
                        Toast.makeText(MainActivity.this, "Please Install Dialer", Toast.LENGTH_SHORT).show();
                    }

                    break;

                case 7:
                    openStaticInWebView(Config.ABOUT_US, "About Us");
                    break;

                case 8:
                    openStaticInWebView(Config.PRIVACY_POLICY, "Privacy Policy");
                    break;

                case 9:
                    openStaticInWebView(Config.TERMS_CONDITION, "Terms of Use");
                    break;

                case 10:
                    //Logout method for clear SDK prefs
                    Authentication authentication = new Authentication();
                    authentication.logout(MainActivity.this);

                    Utils.logoutUser(MainActivity.this);

                    if (Utils.getStringFromPreferences(getApplicationContext(), Utils.executivePartnerCode) != null) {
                        Utils.removeDataFromPreferences(getApplicationContext(), Utils.executivePartnerCode);
                    }

                    showLoginScreen();

                    break;
            }
            mDrawerLayout.closeDrawers();
        }
    }

    private void showLoginScreen() {
        finish();
        Intent intent = new Intent(MainActivity.this, IntroActivity.class);
        startActivity(intent);
    }

    private void showNotification(){
        Intent intent = new Intent(MainActivity.activity, MultiUseActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(MultiUseActivity.SELECT_NOTIFICATION, MultiUseActivity.SELECT_NOTIFICATION);
        bundle.putString(MultiUseActivity.TITLE, "Notifications");
        intent.putExtras(bundle);
        startActivity(intent);
    }

    private void RateUs() {
        LinearLayout ll = new LinearLayout(this);
        final RatingBar rb = new RatingBar(this);
        Button btn = new Button(this);

        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        rb.setLayoutParams(lp);

        rb.setStepSize(1F);
        //rb.setPadding(0, 5, 0, 5);
        rb.setRating((float) 5.0);
        LayerDrawable stars = (LayerDrawable) rb.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(1).setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(0).setColorFilter(getResources().getColor(R.color.colorPrimaryDark), PorterDuff.Mode.SRC_ATOP);

        btn.setLayoutParams(lp);
        btn.setText("Rate Us");
        btn.setTextColor(getResources().getColor(android.R.color.white));
        btn.setBackgroundDrawable(getResources().getDrawable(R.drawable.button_style));
        btn.setGravity(Gravity.CENTER);
        btn.setPadding(0, 5, 0, 5);

        ll.setLayoutParams(lp);
        ll.setOrientation(LinearLayout.VERTICAL);
        ll.setGravity(Gravity.CENTER);
        ll.setPadding(25, 25, 25, 25);

        ll.addView(rb);
        ll.addView(btn);

        final Dialog d_rate_us = new Dialog(this);
        d_rate_us.requestWindowFeature(Window.FEATURE_NO_TITLE);
        d_rate_us.setContentView(ll);
        d_rate_us.show();

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int rating = (int) rb.getRating();
                d_rate_us.dismiss();
                if (rating < 4) {
                    if (rating < 4) {
                        RATING = rating;
                        openFeedBack();
                    }
                } else if (rating >= 4) {
                    try {
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse("market://details?id=" + getPackageName()));
                        startActivity(intent);
                    } catch (Exception e) { //google play app is not installed
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=" + getPackageName()));
                        startActivity(intent);
                    }
                }
            }
        });
    }

    private void openStaticInWebView(String Url, String PageTitle) {
        Bundle bundle = new Bundle();
        bundle.putString(MultiUseActivity.SELECT_WEBVIEW, Url);
        bundle.putString(MultiUseActivity.TITLE, PageTitle);
        Intent intent = new Intent(MainActivity.activity, MultiUseActivity.class);
        intent.putExtras(bundle);

        startActivity(intent);
    }

    private void openFeedBack() {
        Bundle bundle = new Bundle();
        bundle.putString(MultiUseActivity.SELECT_FEEDBACK, MultiUseActivity.SELECT_FEEDBACK);
        bundle.putString(MultiUseActivity.TITLE, "Feedback");
        Intent intent = new Intent(MainActivity.activity, MultiUseActivity.class);
        intent.putExtras(bundle);

        startActivity(intent);
    }

    private void openProfile() {
        Bundle bundle = new Bundle();
        bundle.putString(MultiUseActivity.SELECT_PROFILE, MultiUseActivity.SELECT_PROFILE);
        bundle.putString(MultiUseActivity.TITLE, "My Profile");
        Intent intent = new Intent(MainActivity.activity, MultiUseActivity.class);
        intent.putExtras(bundle);

        startActivity(intent);
    }

    private void openIntroScreen() {
        Intent intent = new Intent(this,IntroSlider.class);
        intent.putExtra(IntroSlider.HOW_TO_USE_FLAG,true);
        startActivity(intent);
    }

    public void setProfileImage() {
        String profilePath = Utils.getStringFromPreferences(this, Utils.profileKey);

        if (profilePath.startsWith("http")) {
            ImageLoader imageLoader = ApplicationController.getInstance().getImageLoader();
            imageLoader.get(profilePath, new ImageLoader.ImageListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e(TAG, "Image Load Error: " + error.getMessage());
                }

                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                    if (response.getBitmap() != null) {
                        new SaveBitmapInFile().execute(response.getBitmap());
                    }
                }
            });
        } else {
            File file = new File(profilePath);
            if(file.exists()) {
                bitmap = Utils.getCircularBitmapFromFile(mProfile, file.getAbsolutePath());
                if(bitmap != null) {
                    mProfile.setImageBitmap(bitmap);
                    Log.i(TAG, "setProfileImage: profile Set");
                }
            }
        }
    }

    public interface onBackListener{
        void donBackPress(ViewPager viewPager);
    }

    /*******************************************************
     *        TODO: NOTIFICATION COUNTER METHOD
     ******************************************************/

    /**
     * Increment the Counter showing on the notification icon in actionBar in main activity
     *
     * @param by : is increment the existing counter with 'by'
     */
    public void incrementNotificationCount(int by) {
        COUNTER = COUNTER + by;
        invalidateOptionsMenu();
    }

    /**
     * Increment the Counter showing by '1' on the notification icon in actionBar in main activity
     */
    public void incrementNotificationCount() {
        COUNTER = COUNTER + 1;
        invalidateOptionsMenu();
    }

    /**
     * Change the Notification Counter
     *
     * @param value : is assign to the Notification counter value
     */
    public void changeNotificationCount(int value) {
        COUNTER = value;
        invalidateOptionsMenu();
    }

    /**
     * reset the notification counter
     */
    public void resetNotficationCounter() {
        COUNTER = 0;
        invalidateOptionsMenu();
    }

    /*******************************************************
     * TODO: NOTIFICATION COUNTER METHOD OVER
     ******************************************************/

    private class SaveBitmapInFile extends AsyncTask<Bitmap, Void, Integer> {

        private Bitmap bitmap ;
        @Override
        protected Integer doInBackground(Bitmap... params) {
            Bitmap bitmap = params[0];
            if (bitmap != null) {
                String profilepath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Renewbuy/Profiles";
                File dir = new File(profilepath);
                if(!dir.exists())
                    dir.mkdirs();

                File image = new File(dir,"profile.png");
                FileOutputStream stream ;
                try {
                    stream = new FileOutputStream(image);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    stream = null ;
                }

                if(stream != null) {
                    bitmap.compress(Bitmap.CompressFormat.PNG,70,stream);
                    String newprofilepath = image.getAbsolutePath();
                    Utils.saveDataToPreferences(MainActivity.this, Utils.profileKey,newprofilepath);
                    MainActivity.activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            setProfileImage();
                        }
                    });
                }
            }
            return 0;
        }

    }
}
