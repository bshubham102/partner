package com.renewbuy.partners;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.inputmethodservice.InputMethodService;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.renewbuy.mercury.models.UserModel;

public class LoginFragment extends Fragment {

    private EditText passwordField;
   // private TextView login_show_pwd;

    private IntroActivity activity ;

    private TextInputLayout mEmailLayout, mPasswordLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_login, container, false);

        activity = IntroActivity.activity ;

        final EditText email = (EditText) view.findViewById(R.id.email);
        final Button submitButton = (Button) view.findViewById(R.id.login_submit);
        passwordField = (EditText) view.findViewById(R.id.password);

        mEmailLayout = (TextInputLayout) view.findViewById(R.id.input_layout_email);
        mPasswordLayout = (TextInputLayout) view.findViewById(R.id.input_layout_password);
        //login_show_pwd = (TextView) view.findViewById(R.id.tv_login_show_pwd);

        //Toast.makeText(getActivity(), InstallReferrerReceiver.khem, Toast.LENGTH_LONG).show();

       /* login_show_pwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Editable pwd = passwordField.getText();
                if(pwd!=null && !pwd.toString().isEmpty()) {
                    if (login_show_pwd.getText().equals("Show")) {
                        login_show_pwd.setText("Hide");

                        passwordField.setInputType(InputType.TYPE_CLASS_TEXT);
                        passwordField.setTransformationMethod(null);
                        passwordField.setSelection(pwd.length());
                    } else {
                        login_show_pwd.setText("Show");

                        passwordField.setTransformationMethod(PasswordTransformationMethod.getInstance());
                        passwordField.setSelection(pwd.length());
                    }
                }
            }
        });*/

        /*passwordField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s!=null && !s.toString().isEmpty()){
                    login_show_pwd.setVisibility(View.VISIBLE);
                }else{
                    login_show_pwd.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });*/

        String primaryEmail=Utils.getStringFromPreferences(getActivity(), "LOGIN_EMAIL");
        if(!primaryEmail.isEmpty()){
            email.setText(primaryEmail);
        }
        else {
            primaryEmail = Utils.getPrimaryEmail(getActivity());
            if (!primaryEmail.isEmpty()) {
                email.setText(primaryEmail);
            }
        }
        clearPassword();

        // Set onClick handler for new user text
       /* view.findViewById(R.id.registerText).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             IntroActivity.activity.goToSignUp();
            }
        });*/

        view.findViewById(R.id.tv_forgot_password).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Activity activity = IntroActivity.activity;

                AlertDialog.Builder dlgAlert = new AlertDialog.Builder(activity);

                final EditText myMsg = new EditText(activity);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT,1.0f);
                params.setMargins(10,10,10,10);
                myMsg.setLayoutParams(params);
                myMsg.setHint("E-mail ID");
                myMsg.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                myMsg.setText(email.getText().toString());

                ImageView  imageView = new ImageView(activity);
                imageView.setImageDrawable(getResources().getDrawable(R.drawable.login_email));
                LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                params1.setMargins(25,5,5,5);
                imageView.setLayoutParams(params1);

                LinearLayout ll=new LinearLayout(activity);
                ll.setOrientation(LinearLayout.HORIZONTAL);

                ll.setPadding(10,10,10,10);
                ll.setGravity(Gravity.CENTER);
                ll.addView(imageView);
                ll.addView(myMsg);

                dlgAlert.setView(ll);

                dlgAlert.setTitle("Forgot Password ?");
                dlgAlert.setPositiveButton("Submit", null);
                dlgAlert.setNegativeButton("Cancel", null);

                final AlertDialog dialog = dlgAlert.create();
                dialog.show();
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorAccent));
                dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorAccent));

                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String email=myMsg.getText().toString();
                        if(email.isEmpty() || !Utils.isValidEmailId(email)){
                            myMsg.setError("Please enter a valid Email Id");
                        }
                        else {
                            dialog.dismiss();
                            Api.forgotPassword(activity, email);
                        }
                    }
                });
            }
        });

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isRepeatedClicks("loginSubmitButton")) {
                    return;
                }

                if (getActivity().getCurrentFocus() != null) {
                    InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(InputMethodService.INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                }

                String loginEmail = email.getText().toString();
                if(loginEmail.equals(""))
                {
                    mEmailLayout.setError("Email can not be empty");
                    email.requestFocus();
                    return;
                }
               else  if (!Utils.isValidEmailId(loginEmail)) {
                    mEmailLayout.setError("Please enter valid Email Id");
                    email.requestFocus();
                    return;
                } else {
                    mEmailLayout.setErrorEnabled(false);
                }

                if (passwordField.getText().toString().trim().isEmpty()) {
                     mPasswordLayout.setError("Password cannot be empty");
                     passwordField.requestFocus();
                    return;
                } else {
                    mPasswordLayout.setErrorEnabled(false);
                    mEmailLayout.setErrorEnabled(false);
                    // Save the email address to pick up later as well
                    Utils.saveDataToPreferences(getActivity(), "LOGIN_EMAIL", loginEmail);

                    Utils.hideKeyboard(getActivity());

                    UserModel userModel = new UserModel(loginEmail,passwordField.getText().toString());
                    Api.loginUser(IntroActivity.activity, userModel);
                }
            }
        });

        passwordField.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    submitButton.performClick();
                    try {
                        InputMethodManager imm = (InputMethodManager) getActivity().getBaseContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    return true;
                }
                return false;
            }
        });
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        clearPassword();
    }

    public void clearPassword() {
        passwordField.setText("");
    }

}
