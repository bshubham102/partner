package com.renewbuy.partners;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PolicyQuotesWebView extends Fragment {

    private QuoteUrlListener  mListener ;
    String locationName="";

    public interface QuoteUrlListener {
        void onUrlFound(String Url);
    }

    private View view ;
    private ImageView help;
    private TextView mHelpText, manufacturer, model, variant, city, policy_month, policy_year, ncb;
    private TextView mVariantError, mCityError, mPurchasedDateError, mNcbError,txt_model_error,txt_vehcile_manufacturer,txt_manufacture_error ;
    private RadioGroup mVehicleTypeRaadioGroup, mClaimRadioGroup , mPrevPolicyRadioGroup;
    private ProgressDialog dialog ;
    private Context context;

    private RelativeLayout container ;

    private Button mSubmitButton ;

    private LinearLayout rolloverLayout ;
    private CardView ncbCard ;
    public int ManufacturerID = 0, ModelID = 0, VariantID = 0, CityID = 0 ;

    private boolean isNewVehicle = false  , isNcbMade = false ,has_prev_policy = true ;

    public static PolicyQuotesWebView intance;
    public static Double currentlat=0.0 , currentlag=0.0, newlat = null , newlag = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_policy_quotes_web_view, container, false);

        //Intitate instance;
        intance = this ;

        context = getActivity() ;

        //Intialize the Views
        init(view);

        setClickListener();

        return view ;
    }

    private void init(View view) {

        //Intialize help text
        help           = (ImageView) view.findViewById(R.id.image_info);
        mHelpText      = (TextView) view.findViewById(R.id.list_header);
        mHelpText.setText(getResources().getString(R.string.fill_form));
        txt_model_error = (TextView) view.findViewById(R.id.txt_model_error);
        txt_manufacture_error=(TextView)view.findViewById(R.id.txt_manufacture_error);


        //Intialize the TextView
        manufacturer = (TextView) view.findViewById(R.id.txt_vehcile_manufacturer);
        model   = (TextView) view.findViewById(R.id.txt_model);
        variant = (TextView) view.findViewById(R.id.txt_variant);
        city    = (TextView) view.findViewById(R.id.txt_registration_city);
        policy_month = (TextView) view.findViewById(R.id.txt_policy_month);
        policy_year = (TextView) view.findViewById(R.id.txt_policy_year);

        //Intialize the Error TextView
        mVariantError    = (TextView) view.findViewById(R.id.txt_variant_error);
        mCityError    = (TextView) view.findViewById(R.id.txt_city_error);
        mPurchasedDateError    = (TextView) view.findViewById(R.id.txt_policy_date_error);
        mNcbError    = (TextView) view.findViewById(R.id.txt_ncb_error);

        //Intialize radion for vehcile type
        mVehicleTypeRaadioGroup = (RadioGroup) view.findViewById(R.id.vehicle_type_radio_group);

        //Intialize the toll over layout
        rolloverLayout = (LinearLayout) view.findViewById(R.id.roll_over_type_layout);

        mClaimRadioGroup = (RadioGroup) view .findViewById(R.id.claim_radio_group);

        mPrevPolicyRadioGroup = (RadioGroup) view .findViewById(R.id.prev_policy_radio_group);

        ncbCard = (CardView) view.findViewById(R.id.ncb_card);

        ncb  = (TextView) view.findViewById(R.id.txt_ncb);

        mSubmitButton = (Button) view.findViewById(R.id.btn_view_quotes);
    }

    private void setClickListener() {
        //help text at top area
        help.setOnClickListener(new MyCLickListener());

        //All textviews
        manufacturer.setOnClickListener(new MyCLickListener());
        model.setOnClickListener(new MyCLickListener());
        variant.setOnClickListener(new MyCLickListener());
        city.setOnClickListener(new MyCLickListener());
        policy_month.setOnClickListener(new MyCLickListener());
        policy_year.setOnClickListener(new MyCLickListener());

        ncb.setOnClickListener(new MyCLickListener());

        mSubmitButton.setOnClickListener(new MyCLickListener());

        mVehicleTypeRaadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.new_vehicle:
                        isNewVehicle = true;
                        rolloverLayout.setVisibility(View.GONE);
                    break;

                    case R.id.rollover_vehicle:
                        isNewVehicle = false ;
                        rolloverLayout.setVisibility(View.VISIBLE);
                    break;
                }
            }
        });

        mClaimRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.claim_yes:
                        isNcbMade = true;
                        ncbCard.setVisibility(View.GONE);
                    break;

                    case R.id.claim_no:
                        isNcbMade = false ;
                        ncbCard.setVisibility(View.VISIBLE);
                    break;
                }
            }
        });

        mPrevPolicyRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.prev_yes:
                        has_prev_policy = true;
                    break;

                    case R.id.prev_no:
                        has_prev_policy = false ;
                    break;
                }
            }
        });
    }

    private class MyCLickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            int id = v.getId();
            switch (id) {
                case R.id.image_info:
                    String[] texts = {
                            "For instant renewal, fill the details in the below form along with customer details",
                            " You can send an auto generated link to customers after that",
                            "Customers can make a purchase from the link and policy shall be issued immediately",
                            "Your earnings shall be reflected in “My Earnings” tab"
                    };

                    int[] resources = {
                            R.drawable.ic_issue_tut_first,
                            R.drawable.ic_issue_tut_two,
                            R.drawable.ic_issue_tut_three,
                            R.drawable.ic_issue_tut_four,
                    };

                    Utils.ShowDialog(context, context.getResources().getString(R.string.instant_policy_issue), texts, resources);
                break;

                case R.id.txt_vehcile_manufacturer:
                    if(! model.getText().toString().equals("")) {
                        model.setText(null);
                        ModelID = 0 ;
                    }

                    if(! variant.getText().toString().equals("")) {
                        variant.setText(null);
                        VariantID = 0 ;
                    }

                    startMultiUseActivityForResult(Utils.MANUFACTURER_REQUEST,"Select Vehicle Manufacturer");
                break;

                case R.id.txt_model:
                    if(ManufacturerID != 0)
                       startMultiUseActivityForResult(Utils.MODEL_REQUEST,"Select Vehicle Model");
                    else
                        Toast.makeText(context, "Please Select Manufacturer First", Toast.LENGTH_SHORT).show();
                break;

                case R.id.txt_variant:
                    if(ModelID != 0)
                        startMultiUseActivityForResult(Utils.VARIANT_REQUEST,"Select Variant");
                    else
                        Toast.makeText(context, "Please Select Model First", Toast.LENGTH_SHORT).show();
                break;

                case R.id.txt_registration_city:
                    startMultiUseActivityForResult(Utils.CITY_REQUEST,"Select Registration City");
                break;

                case R.id.txt_policy_month:
                    Utils.selectDropDown(context,"Select Month",policy_month,Utils.getMonthList());
                break;

                case R.id.txt_policy_year:
                    Utils.selectDropDown(context,"Select Year",policy_year,Utils.getTenYearList());
                break;

                case R.id.txt_ncb:
                    String monthDefaultText = getResources().getString(R.string.mm);
                    String yearDefaultText  = getResources().getString(R.string.yyyy);

                    if( policy_month.getText().toString().equals(monthDefaultText) || policy_year.getText().toString().contains(yearDefaultText)) {
                        Toast.makeText(context, "Please Select Valid Purchase Date First:", Toast.LENGTH_SHORT).show();
                        return ;
                    }

                    ArrayList<String> ncbList = new ArrayList<>();
                    ncbList.add("0%") ;
                    try
                    {
                        String regDateString = Utils.getMonthNumber(policy_month.getText().toString())+"/01/"+policy_year.getText().toString();
                        String currentDateString = new SimpleDateFormat("M/dd/yyyy").format(new Date());

                        Date reg = new Date(regDateString);
                        Date current = new Date(currentDateString);

                        float diffYears = (float) ((current.getTime() - reg.getTime()) / (1000 * 60 * 60 * 24)) / 365 ;

                        if(diffYears > 1)
                            ncbList.add("20%");

                        if(diffYears > 2)
                            ncbList.add("25%");

                        if(diffYears > 3)
                            ncbList.add("35%");

                        if(diffYears > 4)
                            ncbList.add("45%");

                        if(diffYears > 5)
                            ncbList.add("50%");

                        Utils.selectDropDown(context,"Select Ncb",ncb,ncbList);

                    } catch (ParseException e) {
                        e.printStackTrace();
                        Utils.selectDropDown(context,"Select Ncb",ncb,ncbList);
                    }

                break;

                case R.id.btn_view_quotes:
                     submitForm();
                break;
            }
        }
    }

    private void startMultiUseActivityForResult(int requestCode,String title) {
        Bundle bundle = new Bundle();
        bundle.putInt(MultiUseActivity.SELECT_FRAGEMENT,requestCode);
        bundle.putString(MultiUseActivity.TITLE,title);
        Intent intent = new Intent(MainActivity.activity,MultiUseActivity.class);
        intent.putExtras(bundle);
        startActivityForResult(intent,requestCode);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        String name = data.getExtras().getString("Name");
        int    id   = data.getExtras().getInt("ID");

        switch(resultCode) {
            case Utils.MANUFACTURER_REQUEST:
                 manufacturer.setText(name);
                 ManufacturerID = id ;
            break;

            case Utils.MODEL_REQUEST:
                 model.setText(name);
                 ModelID = id;
            break;

            case Utils.VARIANT_REQUEST:
                variant.setText(name);
                VariantID = id;
            break;

            case Utils.CITY_REQUEST:
                city.setText(name);
                CityID = id;
            break;
        }
    }

    private void submitForm() {
        if(manufacturer.getText().toString().equals(""))
        {
            setError(txt_manufacture_error,"Please Select Manufacturer");
            return ;
        } else {
            hideError(txt_manufacture_error);
        }
        if(model.getText().toString().equals(""))
        {
            setError(txt_model_error,"Please Select Model");
            return ;
        } else {
            hideError(txt_model_error);
        }

        if(variant.getText().toString().equals("")) {
            setError(mVariantError,"Please Select Variant");
            return ;
        } else {
           hideError(mVariantError);
        }

        if(city.getText().toString().equals("")) {
            setError(mCityError,"Please Select City");
            return ;
        } else {
            hideError(mCityError);
        }

        String monthDefaultText = getResources().getString(R.string.mm);
        String yearDefaultText  = getResources().getString(R.string.yyyy);


        if(!isNewVehicle) {
            if (policy_month.getText().toString().equals(monthDefaultText) || policy_year.getText().toString().contains(yearDefaultText)) {
               setError(mPurchasedDateError, "Please Select a Valid Date");
               return;
            } else {
                hideError(mPurchasedDateError);
            }
            if(!isNcbMade) {
                if (ncb.getText().toString().equals("")) {
                    setError(mNcbError, "Please Select Previous NCB");
                    return;
                } else {
                    hideError(mNcbError);
                }
            }
        }

        JSONObject object = new JSONObject();
        JSONObject customer = new JSONObject();
        try {
            customer.put("registration_city",CityID);
            object.put("customer",customer);
            object.put("variant",VariantID);
            object.put("is_new",isNewVehicle);
            object.put("executive",Utils.getStringFromPreferences(context,Utils.executiveIdKey));

            String date ;
            if(!isNewVehicle) {
                int month = Utils.getMonthNumber(policy_month.getText().toString()) ;
                if(month<=9)
                 date = policy_year.getText().toString()+"-0"+month+"-01";
                else
                 date = policy_year.getText().toString()+"-"+month+"-01";

            } else {
                @SuppressLint("SimpleDateFormat")
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                Date date1 = new Date();
                date = format.format(date1);
                has_prev_policy = false ;
            }

            object.put("purchase_date", date);
            object.put("has_prev_policy",has_prev_policy);

            if(!isNewVehicle) {
                object.put("claim_previousyear", isNcbMade);
                if (!isNcbMade)
                    object.put("previous_yearncb", ncb.getText().toString().contains("%") ? ncb.getText().toString().replace("%", "") : ncb.getText().toString());
            }

            Log.i("submitForm: ",object.toString());
            new CreateProposal(object,MainActivity.activity);

        } catch (JSONException | ParseException e) {
            e.printStackTrace();
            Log.i("submitForm: ",e.toString());
        }
    }

    private void setError(TextView layout , String msg){
        layout.setText(msg);
        layout.setVisibility(View.VISIBLE);
    }
    private void hideError(TextView layout){
        layout.setVisibility(View.GONE);
    }

    // public class to create a new proposal for showing policy quotes
    public class CreateProposal {
        private MainActivity activity ;
        private JSONObject params;
        private String mQutoeUrl;

        public CreateProposal(JSONObject params, MainActivity activity) {
            this.params = params;
            this.activity = activity;
            dialog = new ProgressDialog(context);
            dialog.setMessage("Please Wait");
            dialog.setCancelable(false);
            dialog.show();
            getpolicy();
        }

        private void getpolicy() {
            String URL = Config.DOMAIN+"/api/v1/motor/proposal/?embed=customer";
            final String token = Utils.getAuthToken(activity);

            AuthorizedJsonObjectRequest req = new AuthorizedJsonObjectRequest(Request.Method.POST,URL,params,token,
                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            VolleyLog.d("fetchDataForQuotes Response: %s", response);
                            String title = null;
                            try {
                               mQutoeUrl = !response.isNull("quotes_url") ? response.getString("quotes_url") : null;
                                title = !response.isNull("vehicle_name") ? response.getString("vehicle_name") : null ;
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            showPolicyQuotes(mQutoeUrl,title);
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            VolleyLog.d("fetchDataForQuotes Error: %s", error);
                            showPolicyQuotes(null , null);
                        }
                    }
            );
            RetryPolicy policy = new DefaultRetryPolicy(Api.TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            req.setRetryPolicy(policy);

            // add the request object to the queue to be executed.
            ApplicationController.getInstance().addToRequestQueue(req);
        }

    }

    private void showPolicyQuotes(String mQutoeUrl,String title) {
        if(dialog != null)
            dialog.hide();

        if(mQutoeUrl != null) {

            // Reset Previous Data
            resetView();

            Bundle bundle = new Bundle();
            bundle.putString(MultiUseActivity.SELECT_WEBVIEW,mQutoeUrl+"&via_cse");

            if(title != null)
                bundle.putString(MultiUseActivity.TITLE,title);
            Intent intent = new Intent(MainActivity.activity,MultiUseActivity.class);
            intent.putExtras(bundle);

            startActivity(intent);

        } else {
            Toast.makeText(context, "Unable to connect server.", Toast.LENGTH_SHORT).show();
        }
    }

    private void resetView(){
        manufacturer.setText(null);
        model.setText(null);
        variant.setText(null);
        city.setText(null);
        mVehicleTypeRaadioGroup.check(R.id.rollover_vehicle);
        mClaimRadioGroup.check(R.id.claim_no);
        policy_month.setText(context.getResources().getString(R.string.mm));
        policy_year.setText(context.getResources().getString(R.string.yyyy));
        ncb.setText(null);
        mPrevPolicyRadioGroup.check(R.id.prev_yes);
    }


    public void getLocationDetails(Context context1) {
        GPSTracker gpsTracker = new GPSTracker(context1);
        if(gpsTracker.canGetLocation())
        {
            currentlat  = gpsTracker.getLatitude()  ;
            currentlag  = gpsTracker.getLongitude() ;
            showLocationName(currentlat,currentlag , context1);
        } else {

            //Common.getInstance().showAlert(context,"please enable Gps setting");

           // locationName.setTextSize(10);
            //locationName.setText("An Error Occured");
        }
    }

    private void showLocationName(double lat, double lag , Context context) {

        Geocoder geocoder = new Geocoder(context);
        try {
            List<Address> list = geocoder.getFromLocation(lat, lag,1);
            String area = list.get(0).getAddressLine(0);
            String subarea = list.get(0).getAddressLine(1);
           locationName = area + "," + subarea;

        }catch (IOException e) {
            //changeUI(null);
            //locationName.setTextSize(10);
            //locationName.setText("An Error Occured");
        }
    }




}
