package com.renewbuy.partners.Sales;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.renewbuy.partners.Adapter.RejectedAdapter;
import com.renewbuy.partners.R;
import com.renewbuy.partners.Utils;
import com.renewbuy.partners.db.AppDb;
import com.renewbuy.partners.db.CustomerBean;
import com.renewbuy.partners.db.CustomerTable;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class RejectedTransaction extends Fragment {

    private ListView listView;
    private View view;
    private RejectedAdapter adapter;
    private Context context;
    private List<CustomerBean> models = new ArrayList<>();
    private View header;

    private RelativeLayout empty_layout;

    public static RejectedTransaction instance ;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_rejected_transaction, container, false) ;
        context = getActivity();
        listView = (ListView) view.findViewById(R.id.rejected_list);

        //Intialize the empty layout
        empty_layout = (RelativeLayout) view.findViewById(R.id.empty_rejected_layout);

        header = LayoutInflater.from(context).inflate(R.layout.list_header,null);
        TextView title = (TextView) header.findViewById(R.id.list_header);
        title.setText(getResources().getString(R.string.rejected_transaction));

        ImageView info  = (ImageView) header.findViewById(R.id.image_info);
        info.setVisibility(View.GONE);

        listView.setDividerHeight(0);
        listView.addHeaderView(header);
        init();
        setAdapter();

        instance = this ;

        return view;
    }

    private void init() {
        AppDb db = AppDb.getInstance(context);
        CustomerTable customerTable = (CustomerTable) db.getTableObject(CustomerTable.TABLE_NAME);
        Vector<CustomerBean> categoryBeans =customerTable.getDataByStatus(context, Utils.REJECTED);
        if( categoryBeans.size() > 0 ) {
            for (int i = 0; i <= categoryBeans.size()-1 ; i++) {
                models.add(categoryBeans.get(i));
            }
        }
    }

    public void refreshList(){
        models.clear();
        init();
        setAdapter();
    }

    private void setAdapter(){
        if(models.size() > 0 ){
            setEmpty_layout(false);
            adapter = new RejectedAdapter(context,R.layout.rejected_row,models);
            listView.setAdapter(adapter);
        } else {
            setEmpty_layout(true);
        }
    }

    private void setEmpty_layout(boolean b){
        if(b){
            listView.setVisibility(View.GONE);
            empty_layout.setVisibility(View.VISIBLE);
        } else {
            empty_layout.setVisibility(View.GONE);
            listView.setVisibility(View.VISIBLE);
        }
    }
}
