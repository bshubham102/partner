package com.renewbuy.partners.Sales;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.renewbuy.partners.Adapter.SubmitAdapter;
import com.renewbuy.partners.MainActivity;
import com.renewbuy.partners.R;
import com.renewbuy.partners.Stats;
import com.renewbuy.partners.Utils;
import com.renewbuy.partners.db.AppDb;
import com.renewbuy.partners.db.CustomerBean;
import com.renewbuy.partners.db.CustomerTable;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class SubmitTransaction extends Fragment {

    private Stats.OnStatUploadClickListener mListener;

    private ListView listView;
    private Context context;
    private View header ;
    private List<CustomerBean> models = new ArrayList<>();
    private SubmitAdapter adapter ;
    private RelativeLayout empty_layout;
    private LinearLayout empty_upload_layout ;

    public static SubmitTransaction instance ;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof MainActivity) {
            mListener = (Stats.OnStatUploadClickListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_submit_transaction, container, false);

        //Get hte context of Main Parent Activity
        context = getActivity();

        //Intialize the empty layout
        empty_layout = (RelativeLayout) view.findViewById(R.id.empty_submit_layout);
        empty_upload_layout = (LinearLayout) view.findViewById(R.id.subit_upload_layout);
        empty_upload_layout.setOnClickListener(new MyListener());

        //Intialize the views
        listView = (ListView) view.findViewById(R.id.submit_list);

        header = LayoutInflater.from(context).inflate(R.layout.list_header,null);
        TextView title = (TextView) header.findViewById(R.id.list_header);
        title.setText(getResources().getString(R.string.submit_transaction));


        ImageView info  = (ImageView) header.findViewById(R.id.image_info);
        info.setOnClickListener(new MyListener());

        listView.setDividerHeight(0);
        listView.addHeaderView(header);

        init();
        setAdapter();

        instance = this;
        return view;
    }

    private void init() {
        AppDb db = AppDb.getInstance(context);
        CustomerTable customerTable = (CustomerTable) db.getTableObject(CustomerTable.TABLE_NAME);
        Vector<CustomerBean> categoryBeans = customerTable.getDataByLessThan(context, Utils.ACCEPTED);
        if( categoryBeans.size() > 0 ) {
            for (int i = 0; i <= categoryBeans.size()-1 ; i++) {
                models.add(categoryBeans.get(i));
            }
        }
    }

    public void refreshList(){
        models.clear();
        init();
        setAdapter();
    }

    private void setAdapter(){
        if(models.size() > 0 ){
            setEmpty_layout(false);
            adapter = new SubmitAdapter(context, R.layout.submitted_row, models);
            listView.setAdapter(adapter);
        } else {
            setEmpty_layout(true);
        }
    }
    private class MyListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            String[] texts = {
                    "Only readable documents will be accepted",
                    "You should remind the customer periodically before policy expiry",
                    "Rejected images shall reflect in “Rejected” tab , you can resubmit rejected cases again"
            };

            int[] rrsources = {
                    R.drawable.ic_submitted_tut_first ,
                    R.drawable.ic_submitted_tut_two,
                    R.drawable.ic_submitted_tut_three
            };

            switch (v.getId()) {
                case R.id.image_info:
                    Utils.ShowDialog(context,context.getResources().getString(R.string.submitted),texts,rrsources);
                break;

                case R.id.subit_upload_layout:
                    mListener.onUploadClickListener(true,2);
                break;
            }
        }
    }
    private void setEmpty_layout(boolean b){
        if(b){
            listView.setVisibility(View.GONE);
            empty_layout.setVisibility(View.VISIBLE);
        } else {
            empty_layout.setVisibility(View.GONE);
            listView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null ;
    }
}
