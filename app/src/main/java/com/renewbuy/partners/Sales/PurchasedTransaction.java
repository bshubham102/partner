package com.renewbuy.partners.Sales;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.renewbuy.partners.Adapter.PurchasedAdapter;
import com.renewbuy.partners.MainActivity;
import com.renewbuy.partners.MultiUseActivity;
import com.renewbuy.partners.R;
import com.renewbuy.partners.Utils;
import com.renewbuy.partners.db.AppDb;
import com.renewbuy.partners.db.PaymentBean;
import com.renewbuy.partners.db.PaymentTable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Vector;

/**
 * A simple {@link Fragment} subclass.
 */
public class PurchasedTransaction extends Fragment {

    private  String[] filterList;
    private  String[] filterHeading ;
    private int selectedFilter = 0 ;

    private ListView listView;
    private TextView title;
    private Button bank_detail;
    private Context context;
    private View header ;
    private List<PaymentBean> models = new ArrayList<>();

    private RelativeLayout empty_layout;
    public static PurchasedTransaction instance ;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_purchased_transaction, container, false);
        context = getActivity();
        listView = (ListView) view.findViewById(R.id.purchased_list);

        //Intialize the empty layout
        empty_layout = (RelativeLayout) view.findViewById(R.id.empty_purchased_layout);

        bank_detail = (Button) view.findViewById(R.id.add_bank_details);
        setButtonText();
        bank_detail.setOnClickListener(new MyListener());

        header = LayoutInflater.from(context).inflate(R.layout.list_header,null);
        title = (TextView) header.findViewById(R.id.list_header);

        filterList = getResources().getStringArray(R.array.year_monrh_filter);
        filterHeading = getResources().getStringArray(R.array.purchased_filter);
        initTextView(filterHeading[0]);

        ImageView info  = (ImageView) header.findViewById(R.id.image_info);
        info.setOnClickListener(new MyListener());

        listView.setDividerHeight(0);
       // listView.addHeaderView(header);
        init();
        setAdapter();

        instance = this ;

        return view;
    }

    private void init() {
        AppDb db = AppDb.getInstance(context);
        PaymentTable paymentTable = (PaymentTable) db.getTableObject(PaymentTable.TABLE_NAME);
        Vector<PaymentBean> paymentBean = paymentTable.getAllData(context);
        if( paymentBean.size() > 0 ) {
            for (int i = 0; i <= paymentBean.size()-1 ; i++) {
                models.add(paymentBean.get(i));
            }

        }
    }

    public void refreshList(){
        models.clear();
        init();
        setAdapter();
    }

    private void setAdapter(){
        if(models.size() > 0 ){
            setEmpty_layout(false);
            PurchasedAdapter adapter = new PurchasedAdapter(context, R.layout.purchased_row, models);
            listView.setAdapter(adapter);
        } else{
            setEmpty_layout(true);
        }
    }

    private class MyListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            int id = v.getId();
            switch (id){

                case R.id.image_info:
                    String[] texts = {
                            "Whenever a referred customer purchases policy,the details shall reflect here",
                            "You can view and download policy details from here",
                            "Your earnings shall reflect against each purchase",
                            "Visit our FAQ section for more details"
                    };
                    int[] rrsources = {
                            R.drawable.ic_purchase_tut_first ,
                            R.drawable.ic_purchase_tut_two ,
                            R.drawable.ic_purchase_tut_three ,
                            R.drawable.ic_purchase_tut_four
                    };
                    Utils.ShowDialog(context,context.getResources().getString(R.string.purchased),texts,rrsources);

                break;


                case R.id.add_bank_details:
                   // Toast.makeText(context, "Add Bank Details", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(MainActivity.activity, MultiUseActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(MultiUseActivity.SELECT_BANK_DETAIL,MultiUseActivity.SELECT_BANK_DETAIL);
                    bundle.putString(MultiUseActivity.TITLE,"Your Bank Details");
                    intent.putExtras(bundle);
                    startActivity(intent);
                break;
            }
        }
    }

    private SpannableString getSpanableString(String line , int startIndex , int lastIndex) {
        SpannableString spanstring = new SpannableString(line);
        ClickableSpan span = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                Utils.showFilterDialog(context, getOptionList(), selectedFilter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if( selectedFilter != which) {
                          initTextView(filterHeading[which]);
                          //Set selected item id
                          selectedFilter = which;
                        }
                        dialog.dismiss();
                    }
                });
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                ds.setColor(getResources().getColor(R.color.colorAccent));
                ds.setUnderlineText(false);
            }
        };
        spanstring.setSpan(span,startIndex,lastIndex, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
       // spanstring.setSpan(new StyleSpan(Typeface.BOLD),startIndex,lastIndex, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return spanstring ;

    }

    private List<String> getOptionList() {
        List<String> option = new ArrayList<>();
        Collections.addAll(option, filterList);
        return  option ;
    }

    private void initTextView(String titlestring){
        SpannableString text = getSpanableString(titlestring,titlestring.indexOf("t"),titlestring.length());
        title.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.arrow_drop_down,0);
        title.setText(text);
        title.setMovementMethod(LinkMovementMethod.getInstance());
    }

    private void setEmpty_layout(boolean b){
        if(b){
            listView.setVisibility(View.GONE);
            empty_layout.setVisibility(View.VISIBLE);
        } else {
            empty_layout.setVisibility(View.GONE);
            listView.setVisibility(View.VISIBLE);
        }
    }

    public void setButtonText(){
       boolean b = Utils.getBooleanFromPreferences(context,Utils.hasAllAccountDetailsFlag) ;
        if(!b)
            bank_detail.setText(context.getResources().getString(R.string.add_bank_account_details));
        else
            bank_detail.setText(context.getResources().getString(R.string.view_bank_account_details));
    }

}
